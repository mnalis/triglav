defmodule Triglav.MixProject do
  use Mix.Project

  def project do
    [
      app: :triglav,
      version: "0.1.0",
      elixir: "~> 1.14",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      dialyzer: [plt_add_apps: ~w/credo ex_unit mix/a]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Triglav.Application, []},
      extra_applications: [:logger, :runtime_tools, :inets, :os_mon]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:atomex, "~> 0.5.1"},
      {:benchee, "~> 1.1", only: [:dev], runtime: false},
      {:castore, "~> 1.0"},
      {:credo, "~> 1.6", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      # Required for dashboard stats
      {:ecto_psql_extras, "~> 0.6"},
      {:ecto_sql, "~> 3.4"},
      {:esbuild, "~> 0.4", runtime: Mix.env() == :dev},
      {:floki, "~> 0.36.2"},
      {:geo, "~> 3.0"},
      {:geo_postgis, "~> 3.1"},
      {:hackney, "~> 1.18"},
      {:jason, "~> 1.0"},
      {:mint, "~> 1.4"},
      {:nimble_csv, "~> 1.2"},
      {:parent, "~> 0.12.1"},
      {:phoenix, "~> 1.6.0"},
      {:phoenix_ecto, "~> 4.1"},
      {:phoenix_html, "~> 3.0"},
      {:phoenix_live_dashboard, "~> 0.5"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:plug_cowboy, "~> 2.0"},
      {:postgrex, ">= 0.0.0"},
      {:protobuf, "~> 0.11"},
      {:sentry, "~> 8.0"},
      {:sweet_xml, "~> 0.7.1"},
      {:swoosh, "~> 1.6"},
      {:tailwind, "~> 0.1", runtime: Mix.env() == :dev},
      {:telemetry_metrics, "~> 0.6"},
      {:telemetry_poller, "~> 1.0"},
      {:tesla, "~> 1.4"},
      {:tzdata, "~> 1.1"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["deps.get", "ecto.setup", "cmd npm install --prefix assets"],
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate --quiet", "test"],
      "assets.deploy": [
        "esbuild default --minify",
        "esbuild pt_routes --minify",
        "esbuild pt_stops --minify",
        "esbuild track --minify",
        "tailwind default",
        "phx.digest"
      ]
    ]
  end
end
