.PHONY: release deploy clean gtfs_realtime

release: clean
	mix deps.get --only prod
	MIX_ENV=prod mix compile
	MIX_ENV=prod mix assets.deploy
	MIX_ENV=prod mix release --overwrite
	mix phx.digest.clean --all

deploy:
	fab -H bezdomni --prompt-for-sudo-password deploy

clean:
	mix phx.digest.clean --all
	rm -rf priv/static/assets

gtfs_realtime:
	protoc --elixir_out=. --elixir_opt=package_prefix=gtfs priv/gtfs/gtfs-realtime.proto
	echo "# Generated from priv/gtfs/gtfs-realtime.proto" > lib/gtfs/transit_realtime.ex
	echo "# See 'gtfs_realtime' command in Makefile" >> lib/gtfs/transit_realtime.ex
	echo "# Do not edit manually\n\n" >> lib/gtfs/transit_realtime.ex
	cat priv/gtfs/gtfs-realtime.pb.ex >> lib/gtfs/transit_realtime.ex
	rm priv/gtfs/gtfs-realtime.pb.ex
	mix format lib/gtfs/transit_realtime.ex

build:
	hut builds submit .builds/deploy.yaml --follow
