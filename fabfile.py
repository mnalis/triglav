from fabric import task
from invoke import run
from os.path import exists


@task()
def deploy(c):
    # Fail early if sudo password is not valid
    c.sudo("echo")

    if exists("triglav.tar.gz"):
        print("Deploying release: triglav.tar.gz")
    else:
        print("triglav.tar.gz not found, aborting")
        return

    print("Copying release...")
    run("scp triglav.tar.gz bezdomni:projects/triglav/")

    print("\nStopping triglav service...")
    c.sudo("service triglav stop")

    with c.cd("projects/triglav"):
        print("\nExtracting release...")
        c.run("rm -rf prod")
        c.run("tar xzf triglav.tar.gz")

        print("\nRunning migrations...")
        c.run('source .envrc && ./triglav eval "Triglav.Release.migrate()"')

    print("\nStarting triglav service...")
    c.sudo("service triglav start")
