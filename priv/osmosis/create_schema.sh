#!/bin/sh
psql -c "DROP SCHEMA IF EXISTS osm CASCADE;"
psql -c "CREATE SCHEMA osm;"

PGOPTIONS="--search_path=osm,public" psql -f pgsnapshot_schema_0.6.sql
PGOPTIONS="--search_path=osm,public" psql -f pgsnapshot_schema_0.6_action.sql
PGOPTIONS="--search_path=osm,public" psql -f pgsnapshot_schema_0.6_changes.sql
PGOPTIONS="--search_path=osm,public" psql -f pgsnapshot_schema_0.6_linestring.sql

# initial
# osmosis --read-pbf /home/ihabunek/projects/ihabunek/triglav_import/croatia-latest.osm.pbf --write-pgsql host=localhost:5434 database=test postgresSchema=osm user=ihabunek password=starseed
