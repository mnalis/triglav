defmodule Triglav.Repo.Migrations.AlterErrorAddPlatform do
  use Ecto.Migration

  def up do
    alter table("errors") do
      add :platform_id, :integer
    end
  end

  def down do
    alter table("errors") do
      remove :platform_id, :integer
    end
  end
end
