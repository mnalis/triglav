defmodule Triglav.Repo.Migrations.CreateOsmDerivedDataTables do
  use Ecto.Migration

  def change do
    create table(:pt_route_relations) do
      add :operator_id, references(:pt_operators), null: false
      add :relation_id, :bigint, null: false
      add :stop_ids, {:array, :string}, null: false
      add :geometry, :geometry, null: false
      add :contiguous, :boolean, null: false
    end

    create table(:pt_route_relation_platforms) do
      add :route_relation_id, references(:pt_route_relations), null: false
      add :node_id, :bigint
      add :way_id, :bigint
      add :stop_id, :text
      add :sequence_id, :integer, null: false
      add :geometry, :geometry, null: false
      add :distance_from_route, :float
    end
  end
end
