defmodule Triglav.Repo.Migrations.CreateHpsTracks do
  use Ecto.Migration

  def up do
    create table("hps_tracks") do
      add :ref, :text
      add :route_no, :text
      add :name, :text
      add :region, :text
      add :url, :text
      add :length, :decimal
      add :height_diff, :integer
      add :walk_time, :integer
      add :gpx1_url, :text
      add :gpx2_url, :text
    end

    create index("hps_tracks", [:ref], unique: true)
  end

  def down do
    drop table("hps_tracks")
  end
end
