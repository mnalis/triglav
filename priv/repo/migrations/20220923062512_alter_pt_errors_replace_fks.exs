defmodule Triglav.Repo.Migrations.AlterPtErrorsReplaceFks do
  @moduledoc """
  This migration removes foreign keys from errors to route and platform.

  Replace route_id with route_ref which can be used to match the error to the
  route in latest feed, instead of it potentially being linked to a route in a
  previous feed.

  Platform is never used, an osm platform is identified by the relation_id and
  sequence_id.
  """
  use Ecto.Migration

  def change do
    alter table(:pt_errors) do
      add :route_ref, :text
    end

    execute """
      UPDATE pt_errors AS error
      SET route_ref = route.ref
      FROM pt_routes AS route
      WHERE route.id = error.route_id
    """

    alter table(:pt_errors) do
      modify :route_ref, :text, null: false
      remove :route_id
      remove :platform_id
    end
  end
end
