defmodule Triglav.Repo.Migrations.CreatePublicTransportErrors do
  use Ecto.Migration

  def change do
    create table(:pt_errors) do
      add :operator_id, references(:pt_operators), null: false

      # These are not references because a feed can be deleted and we want errors to remain.
      add :feed_id, :bigint, null: false
      add :route_id, :bigint, null: false
      add :platform_id, :bigint

      add :relation_id, :bigint
      add :sequence_id, :integer
      add :key, :text, null: false
      add :params, :map, null: false, default: %{}
      add :created_at, :utc_datetime, null: false, default: fragment("now()")
      add :resolved_at, :utc_datetime
    end

    create table(:pt_error_history) do
      add :operator_id, references(:pt_operators), null: false
      add :count, :integer, null: false
      add :previous_count, :integer, null: false
      add :created_count, :integer, null: false
      add :resolved_count, :integer, null: false
      add :feed_ref, :text, null: false
      add :osm_sequence_number, :integer, null: false
      add :timestamp, :utc_datetime, null: false, default: fragment("now()")
      add :created_error_ids, {:array, :integer}, null: false, default: []
      add :resolved_error_ids, {:array, :integer}, null: false, default: []
    end
  end
end
