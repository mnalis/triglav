defmodule Triglav.Repo.Migrations.CreateOsmState do
  use Ecto.Migration

  def up do
    create table("osm_state") do
      add :sequence_number, :integer, null: false
      add :timestamp, :utc_datetime, null: false
      timestamps()
    end
  end

  def down do
    drop table("osm_state")
  end
end
