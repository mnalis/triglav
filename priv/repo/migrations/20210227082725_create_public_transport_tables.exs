defmodule Triglav.Repo.Migrations.CreatePublicTransportTables do
  use Ecto.Migration

  def up do
    create table("public_transport_trips") do
      add :relation_id, :bigint, null: false
      add :route_id, :string
      add :geometry, :geometry, null: false
      add :exact, :boolean, null: false
      add :stop_ids, {:array, :string}
      add :sample_trip_id, :string
    end

    create table("public_transport_platforms") do
      add :trip_id, references("public_transport_trips"), null: false
      add :relation_id, :bigint, null: false
      add :route_id, :string
      add :stop_id, :string
      add :node_id, :bigint
      add :way_id, :bigint
      add :sequence_id, :integer, null: false
      add :geometry, :geometry, null: false
      add :distance_from_route, :integer
    end

    create index("public_transport_trips", [:sample_trip_id], unique: true)
    create index("public_transport_trips", [:geometry], using: :gist)

    create index("public_transport_platforms", [:relation_id, :sequence_id], unique: true)
    create index("public_transport_platforms", [:geometry], using: :gist)
  end

  def down do
    drop table("public_transport_platforms")
    drop table("public_transport_trips")
  end
end
