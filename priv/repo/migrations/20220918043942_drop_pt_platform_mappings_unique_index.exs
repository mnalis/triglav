defmodule Triglav.Repo.Migrations.DropPtPlatformMappingsUniqueIndex do
  use Ecto.Migration

  # This should not be unique since there can be multiple mappings for a single
  # platform.
  def change do
    drop index(:pt_platform_mappings, [:feed_id, :platform_id])
  end
end
