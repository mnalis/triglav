defmodule Triglav.Repo.Migrations.CreateHpsTraces do
  use Ecto.Migration

  def up do
    create table("hps_traces") do
      add :track_id, references("hps_tracks"), null: false
      add :name, :text, null: false
      add :linestring, :geometry, null: false
      add :times, {:array, :utc_datetime}, null: false
      add :elevations, {:array, :float}
    end

    create table("hps_waypoints") do
      add :trace_id, references("hps_traces"), null: false
      add :name, :text, null: false
      add :description, :text, null: false
      add :point, :geometry, null: false
      add :elevation, :float
      add :time, :utc_datetime
    end

    create index("hps_traces", [:track_id], unique: true)
    create index("hps_waypoints", [:trace_id, :point], unique: true)
  end

  def down do
    drop table("hps_waypoints")
    drop table("hps_traces")
  end
end
