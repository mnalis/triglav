defmodule Triglav.Repo.Migrations.CreateNewPublicTransportTables do
  use Ecto.Migration

  def change do
    create table("pt_operators") do
      add :name, :text, null: false
      add :ref, :text, null: false
      add :slug, :text, null: false
      add :wikidata_id, :text
    end

    create unique_index(:pt_operators, [:slug])

    create table("pt_feeds") do
      add :operator_id, references("pt_operators"), null: false
      add :ref, :text, null: false
      add :imported_at, :utc_datetime, null: false
      add :start_date, :date
      add :end_date, :date
      add :file_stats, :map, null: false
      add :active, :boolean, null: false
    end

    create unique_index(:pt_feeds, [:operator_id, :ref])

    create table("pt_routes") do
      add :feed_id, references("pt_feeds"), null: false
      add :name, :text, null: false
      add :network, :text, null: false
      add :order_no, :integer, null: false
      add :ref, :text, null: false
      add :route_type, :text, null: false
    end

    create unique_index(:pt_routes, [:feed_id, :ref])

    create table("pt_route_variants") do
      add :feed_id, references("pt_feeds"), null: false
      add :route_id, references("pt_routes"), null: false
      add :departure_count, :integer, null: false
      add :ref, :text, null: false
      add :direction, :text, null: false
      add :stop_ids, {:array, :text}, null: false
      add :geometry, :geometry
    end

    create unique_index(:pt_route_variants, [:feed_id, :ref])

    create table("pt_platforms") do
      add :feed_id, references("pt_feeds"), null: false
      add :ref, :text, null: false
      add :name, :text, null: false
      add :geometry, :geometry, null: false
      add :use_count, :integer
    end

    create unique_index(:pt_platforms, [:feed_id, :ref])
    create index(:pt_platforms, [:geometry], using: :gist)

    create table("pt_route_variant_platforms") do
      add :feed_id, references("pt_feeds"), null: false
      add :route_variant_id, references("pt_route_variants"), null: false
      add :platform_id, references("pt_platforms"), null: false
      add :sequence_no, :integer, null: false
    end

    create unique_index(:pt_route_variant_platforms, [:route_variant_id, :sequence_no])
  end
end
