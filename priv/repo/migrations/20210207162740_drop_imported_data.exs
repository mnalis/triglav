defmodule Triglav.Repo.Migrations.DropImportedData do
  use Ecto.Migration

  def change do
    drop table("imported_data")
  end
end
