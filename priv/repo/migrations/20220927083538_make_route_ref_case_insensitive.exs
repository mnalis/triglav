defmodule Triglav.Repo.Migrations.MakeRouteRefCaseInsensitive do
  use Ecto.Migration

  def change do
    execute "CREATE EXTENSION IF NOT EXISTS citext", ""

    alter table(:pt_routes) do
      modify :ref, :citext, null: false
    end

    alter table(:pt_errors) do
      modify :route_ref, :citext, null: false
    end
  end

  def down do
    alter table(:pt_routes) do
      modify :ref, :text, null: false
    end

    alter table(:pt_errors) do
      modify :route_ref, :text, null: false
    end
  end
end
