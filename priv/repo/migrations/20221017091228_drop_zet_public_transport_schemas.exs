defmodule Triglav.Repo.Migrations.DropZetPublicTransportSchemas do
  use Ecto.Migration

  def change do
    drop table("errors")
    drop table("error_history")
    drop table("public_transport_platforms")
    drop table("public_transport_trips")
    execute "DROP SCHEMA IF EXISTS zet CASCADE"
  end
end
