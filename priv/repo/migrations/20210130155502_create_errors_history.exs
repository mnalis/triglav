defmodule Triglav.Repo.Migrations.CreateErrorHistory do
  use Ecto.Migration

  def change do
    create table("error_history") do
      add :count, :integer, null: false
      add :previous_count, :integer, null: false
      add :created_count, :integer, null: false
      add :resolved_count, :integer, null: false
      add :zet_feed_version, :string, null: false
      add :osm_sequence_number, :integer, null: false
      add :timestamp, :utc_datetime, null: false, default: fragment("now()")
    end
  end
end
