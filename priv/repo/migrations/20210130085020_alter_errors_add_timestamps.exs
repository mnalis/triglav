defmodule Triglav.Repo.Migrations.ModifyErrorsAddTimestamps do
  use Ecto.Migration

  def change do
    alter table("errors") do
      add :created_at, :utc_datetime_usec, null: false, default: fragment("now()")
      add :resolved_at, :utc_datetime
    end
  end
end
