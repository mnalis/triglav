defmodule Triglav.Repo.Migrations.AddTraceDistances do
  use Ecto.Migration

  def change do
    alter table("hps_traces") do
      add :distances, {:array, :float}, null: false, default: []
    end
  end
end
