defmodule Triglav.Repo.Migrations.CreatePublicTransportMappingTables do
  use Ecto.Migration

  def change do
    create table("pt_platform_mappings") do
      add :feed_id, references("pt_feeds"), null: false
      add :platform_id, references("pt_platforms"), null: false
      add :node_id, :bigint
      add :way_id, :bigint
      add :distance, :float
    end

    create unique_index(:pt_platform_mappings, [:feed_id, :platform_id])

    create table("pt_route_mapped_route_relations", primary_key: false) do
      add :feed_id, references("pt_feeds"), null: false
      add :route_id, references("pt_routes"), null: false, primary_key: true
      add :relation_id, :bigint, null: false, primary_key: true
    end

    create table("pt_route_mapped_route_master_relations", primary_key: false) do
      add :feed_id, references("pt_feeds"), null: false
      add :route_id, references("pt_routes"), null: false, primary_key: true
      add :relation_id, :bigint, null: false, primary_key: true
    end

    alter table("pt_route_variants") do
      add :mapped_relation_id, :bigint, null: true
    end
  end
end
