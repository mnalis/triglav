defmodule Triglav.Repo.Migrations.CreatePoi do
  use Ecto.Migration

  def change do
    create table(:poi_sources) do
      add :name, :text, null: false
      add :slug, :text, null: false
      add :node_count, :integer
      add :mapping_count, :integer
      timestamps()
    end

    create table(:poi_nodes) do
      add :source_id, references(:poi_sources), null: false
      add :name, :text, null: false
      add :ref, :text
      add :geometry, :geometry, null: false
      add :tags, :map, null: false, default: %{}
    end

    create table(:poi_mappings) do
      add :source_id, references(:poi_sources), null: false
      add :node_id, references(:poi_nodes), null: false
      add :osm_way_id, :bigint
      add :osm_node_id, :bigint
      add :distance, :float, null: false
    end

    create unique_index(:poi_sources, [:slug])
    create unique_index(:poi_nodes, [:source_id, :ref])
  end
end
