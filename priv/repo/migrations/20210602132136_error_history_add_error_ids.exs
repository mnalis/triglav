defmodule Triglav.Repo.Migrations.ErrorHistoryAddErrorIds do
  use Ecto.Migration

  def change do
    alter table("error_history") do
      add :created_error_ids, {:array, :integer}, null: false, default: []
      add :resolved_error_ids, {:array, :integer}, null: false, default: []
    end
  end
end
