defmodule Triglav.Repo.Migrations.CreateError do
  use Ecto.Migration

  def up do
    create table("errors") do
      add :key, :text, null: false
      add :params, :map, null: false, default: %{}
      add :route_id, :text
      add :relation_id, :integer
    end
  end

  def down do
    drop table("errors")
  end
end
