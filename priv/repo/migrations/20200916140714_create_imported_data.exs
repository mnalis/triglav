defmodule Triglav.Repo.Migrations.CreateImportedData do
  use Ecto.Migration

  def change do
    create table("imported_data") do
      add :osm_sequence_number, :integer
      add :osm_timestamp, :utc_datetime
    end
  end
end
