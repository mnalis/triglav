defmodule Triglav.Repo.Migrations.DropHpPostOfficesPhoneFax do
  use Ecto.Migration

  def change do
    alter table(:hp_post_offices) do
      remove :phone
      remove :fax
    end
  end
end
