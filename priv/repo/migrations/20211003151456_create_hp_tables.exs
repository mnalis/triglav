defmodule Triglav.Repo.Migrations.CreateHpTables do
  use Ecto.Migration

  def change do
    create table("hp_post_offices") do
      add :post_code, :text, null: false
      add :street, :text, null: false
      add :place, :text, null: false
      add :phone, {:array, :text}, null: false
      add :fax, {:array, :text}, null: false
      add :geometry, :geometry, null: false
    end

    create unique_index("hp_post_offices", [:post_code])

    create table("hp_post_office_mappings") do
      add :post_office_id, references("hp_post_offices"), null: false
      add :node_id, :bigint
      add :way_id, :bigint
      add :distance, :float
    end
  end
end
