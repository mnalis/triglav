defmodule Triglav.Repo.Migrations.Hstore do
  use Ecto.Migration

  def up do
    execute "CREATE EXTENSION IF NOT EXISTS hstore"
  end

  def down do
    execute "DROP EXTENSION IF EXISTS hstore"
  end
end
