TODO
====

* Investigate incremental data updates with osm2pgsql
* Investigate writing a custom parser for [osm pbf](https://wiki.openstreetmap.org/wiki/PBF_Format) to speed up import
* Include leaflet.css globally
* Extract leaflet.js into a vendor.js (maybe not yet supported by esbuild?)

## Public transport

* Process GTFS files (especially stop_times.txt which is large) using [Flow](https://hexdocs.pm/flow/Flow.html) to get some concurrency
* Some autotrolej routes seem to have only one stop, e.g. Autotrolej 1A. Check that this is not a parsing error.
* Instead of hashing the file contents to check for changes, we should hash the data extracted from those files, e.g. by using [phash2](https://www.erlang.org/doc/man/erlang.html#phash2-1). This way we won't create a new feed if the feed data changed but distinct routes, stops, and other data we import did not.

New errors:

* Ways with a role other than "platform"
* Platforms in bus relations should be nodes, not ways
* Compare with https://ptna.openstreetmap.de/results/HR/21/HR-21-ZET-Analysis.diff.html

## Add indexes to osmosis creation script

This has been applied on prod, but will go away if not added to a migration.

```sql
CREATE INDEX idx_nodes_tag_public_transport ON osmosis.nodes ((tags->'public_transport'));
CREATE INDEX idx_ways_tag_public_transport ON osmosis.ways ((tags->'public_transport'));
CREATE INDEX idx_nodes_tag_public_transport ON osmosis.nodes ((tags->'amenity'));
CREATE INDEX idx_ways_tag_public_transport ON osmosis.ways ((tags->'amenity'));
```
