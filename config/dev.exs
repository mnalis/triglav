import Config

database_url =
  System.get_env("DATABASE_URL") ||
    raise """
    environment variable DATABASE_URL is missing.
    For example: ecto://USER:PASS@HOST/DATABASE
    """

# Configure your database
config :triglav, Triglav.Repo,
  url: database_url,
  show_sensitive_data_on_connection_error: true,
  pool_size: 10

config :triglav, Triglav.PublicTransport.Npt,
  username: System.get_env("NTP_USERNAME"),
  password: System.get_env("NTP_PASSWORD")

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with esbuild to bundle .js and .css sources.
config :triglav, TriglavWeb.Endpoint,
  # Binding to loopback ipv4 address prevents access from other machines.
  # Change to `ip: {0, 0, 0, 0}` to allow access from other machines.
  http: [ip: {127, 0, 0, 1}, port: 4000],
  check_origin: false,
  code_reloader: true,
  debug_errors: true,
  secret_key_base: "2d9SiI7iT49HsnCODVDzaVRUtE4ZiVtwebbdbWl1o7lac2NWrqJZ5y2vISmigRR+",
  watchers: [
    app_css: {Tailwind, :install_and_run, [:default, ~w(--watch)]},
    app_js: {Esbuild, :install_and_run, [:default, ~w(--sourcemap=inline --watch)]},
    pt_routes_js: {Esbuild, :install_and_run, [:pt_routes, ~w(--sourcemap=inline --watch)]},
    pt_stops_js: {Esbuild, :install_and_run, [:pt_stops, ~w(--sourcemap=inline --watch)]},
    track_js: {Esbuild, :install_and_run, [:track, ~w(--sourcemap=inline --watch)]}
  ]

# Watch static and templates for browser reloading.
config :triglav, TriglavWeb.Endpoint,
  live_reload: [
    patterns: [
      ~r"priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$",
      ~r"lib/triglav_web/(live|views)/.*(ex)$",
      ~r"lib/triglav_web/templates/.*(eex)$"
    ]
  ]

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Initialize plugs at runtime for faster development compilation
config :phoenix, :plug_init_mode, :runtime

config :triglav, Triglav.Mailer,
  adapter: Swoosh.Adapters.Logger,
  level: :debug,
  log_full_email: true

# config :triglav, Triglav.Mailer,
#   adapter: Swoosh.Adapters.Mailgun,
#   base_url: System.get_env("MAILGUN_BASE_URL"),
#   api_key: System.get_env("MAILGUN_API_KEY"),
#   domain: System.get_env("MAILGUN_DOMAIN")
