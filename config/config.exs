# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :triglav,
  ecto_repos: [Triglav.Repo]

config :triglav, Triglav.Repo, types: Triglav.PostgrexTypes

# Configures the endpoint
config :triglav, TriglavWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: TriglavWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Triglav.PubSub,
  live_view: [signing_salt: "PUQDt1tz"]

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.14.23",
  default: [
    args:
      ~w(js/app.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ],
  pt_routes: [
    args:
      ~w(js/pt_routes.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ],
  pt_stops: [
    args:
      ~w(js/pt_stops.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ],
  track: [
    args:
      ~w(js/track.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

config :tailwind,
  version: "3.0.23",
  default: [
    args: ~w(
      --config=tailwind.config.js
      --input=css/app.css
      --output=../priv/static/assets/app.css
    ),
    cd: Path.expand("../assets", __DIR__)
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# TODO: would be nice to write with a Mint client to remove hackney dep
config :swoosh, :api_client, Swoosh.ApiClient.Hackney

config :tesla, adapter: {Tesla.Adapter.Mint, timeout: :timer.seconds(30)}

config :elixir, :time_zone_database, Tzdata.TimeZoneDatabase

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

# Import developer specific local configuration per environment.
local_config = "#{Mix.env()}.local.exs"
local_path = Path.join(__DIR__, local_config)

if File.exists?(local_path) do
  import_config local_config
end
