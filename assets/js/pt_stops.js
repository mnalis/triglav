import { GeoJSON, Icon, Map, Marker, TileLayer } from "leaflet"

const map = new Map("stops-map")
const tilesUrl = "https://tile.openstreetmap.org/{z}/{x}/{y}.png"
const attribution = '&copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
const blueIcon = new Icon({ iconUrl: "/images/stop.svg", iconSize: [14, 14] })
const redIcon = new Icon({ iconUrl: "/images/stop_red.svg", iconSize: [14, 14] })


const platformsData = JSON.parse(document.getElementById("platforms-geojson").innerHTML)
const platforms = new GeoJSON(platformsData, {
  pointToLayer: (feature, latlng) => {
    const popup = `
      <b>${feature.properties.name}</b><br />
      GTFS ID: ${feature.properties.ref}<br />
      Use count: ${feature.properties.use_count}
    `
    const icon = feature.properties.matched ? blueIcon : redIcon

    return new Marker(latlng, { icon }).bindPopup(popup).bindTooltip(feature.properties.name)
  },
}).addTo(map)

new TileLayer(tilesUrl, { attribution }).addTo(map)
map.fitBounds(platforms.getBounds())
