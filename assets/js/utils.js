/**
 * Load JSON data stored in a <script> element with the given ID.
 */
export const loadData = (id) => JSON.parse(document.getElementById(id).innerHTML)

/**
 * Open JSOM remote links via xhr to avoid opening new tab.
 */
export const handleJosmLinks = () => {
  const els = document.getElementsByClassName("josm-remote")
  for (const el of els) {
    el.onclick = event => {
      const oReq = new XMLHttpRequest()
      oReq.addEventListener("load", result => {
        el.classList.add("success")
        setTimeout(() => el.classList.remove("success"), 500)
      })
      oReq.addEventListener("error", result => {
        el.classList.add("error")
        setTimeout(() => el.classList.remove("error"), 500)
      })
      oReq.open("GET", event.target.href)
      oReq.send()

      return false
    }
  }
}
