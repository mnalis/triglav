import { Circle, GeoJSON, Map, TileLayer } from "leaflet"
import { loadData } from "./utils"

const PADDING_LEFT = 20
const PADDING_RIGHT = 20
const PADDING_TOP = 50
const PADDING_BOTTOM = 40

const drawElevationGuides = (ctx, elevations) => {
  const canvasHeight = ctx.canvas.clientHeight

  const min = Math.floor(Math.min(...elevations) / 100)
  const max = Math.ceil(Math.max(...elevations) / 100)
  const count = max - min + 1
  const offset = (canvasHeight - PADDING_TOP - PADDING_BOTTOM) / count

  for (let i = 0; i <= count; i++) {
    const height = (count - i + min) * 100
    const y = PADDING_TOP + i * offset

    drawHorizontalLine(ctx, y, "#999")
    drawHeightMarker(ctx, y, height)
  }
}

const drawHorizontalLine = (ctx, y, strokeStyle = "black") => {
  const width = ctx.canvas.clientWidth

  ctx.strokeStyle = strokeStyle
  ctx.lineWidth = 0.5

  ctx.beginPath()
  ctx.moveTo(0, y + 0.5)
  ctx.lineTo(width, y + 0.5)
  ctx.stroke()
}

const drawHeightMarker = (ctx, y, height) => {
  ctx.font = "12px sans-serif"
  ctx.fillStyle = "#aaa"
  ctx.fillText(`${height}m`, 10, y - 5)
}

const resizeCanvas = (canvas) => {
  canvas.width = canvas.clientWidth
  canvas.height = canvas.clientHeight
}

// Smallest elevation representable on the chart
const getMinElevation = (elevations) => Math.floor(Math.min(...elevations) / 100) * 100

// Largest elevation representable on the chart
const getMaxElevation = (elevations) => Math.ceil(Math.max(...elevations) / 100) * 100

const crunchChartData = (target, elevations, times, distances, linestring) => {
  const grades = calculateGrades(distances, elevations)
  const minElevation = getMinElevation(elevations)
  const maxElevation = getMaxElevation(elevations)

  const heightDiff = maxElevation - minElevation
  const distance = distances[distances.length - 1]

  const dataWidth = target.clientWidth - PADDING_LEFT - PADDING_RIGHT
  const dataHeight = target.clientHeight - PADDING_TOP - PADDING_BOTTOM

  let points = []

  for (const k in elevations) {
    const p = {
      time: times[k],
      elevation: elevations[k],
      distance: distances[k],
      grade: grades[k],
      coordinates: linestring.coordinates[k],
    }

    if (p.distance == null || p.elevation == null) {
      console.warn("skipping point. distance:", p.distance, "elevation:", p.elevation)
      continue
    }

    const distancePerc = p.distance / distance
    const x = Math.round(PADDING_LEFT + distancePerc * dataWidth)

    const elevationPerc = (p.elevation - minElevation) / heightDiff
    const y = Math.round(PADDING_TOP + (1 - elevationPerc) * dataHeight)

    points.push({ x, y, ...p })
  }

  return {
    points,
    elevations,
    times,
    minElevation,
    maxElevation,
    heightDiff,
    dataWidth,
    dataHeight,
    distance,
  }
}

const calculateGrades = (distances, elevations) => {
  const grades = []

  let prevX = null
  let prevY = null

  const count = distances.length
  for (let i = 0; i < count; i++) {
    // TODO: make a rolling average for grade to make color transitions less frequent?

    const x = distances[i] * 1000 // x in km
    const y = elevations[i]

    if (prevX !== null && prevY !== null) {
      const dx = Math.abs(x - prevX)
      const dy = Math.abs(y - prevY)
      const grade = dx ? 100 * dy / dx : 0

      grades.push(grade)
    }

    prevX = x
    prevY = y
  }

  // The loop creates one less element than there are distances/elevations so
  // repeat the last element to keep them the same length.
  grades.push(grades[grades.length - 1])

  return grades
}

const drawElevations = (ctx, points) => {
  ctx.strokeStyle = "indigo"
  ctx.lineWidth = 1

  let prevColor = null
  let first = true

  for (const { x, y, grade } of points) {
    const color = getGradeColor(ctx, grade)

    if (first) {
      ctx.beginPath()
      ctx.strokeStyle = color
      ctx.moveTo(x, y)
    } else {
      ctx.lineTo(x, y)

      if (color !== prevColor) {
        prevColor = color
        ctx.stroke()
        ctx.beginPath()
        ctx.strokeStyle = color
        ctx.moveTo(x, y)
      }
    }

    first = false
  }

  ctx.stroke()
}

const getGradeColor = (ctx, grade) => {
  if (grade < 10) {
    return "green"
  }

  if (grade < 15) {
    return "orange"
  }

  return "red"
}

const findClosestPoint = (chart, distance) => {
  let min = Number.MAX_SAFE_INTEGER
  let minIndex = null

  // TODO: slowish, binary search?
  for (const idx in chart.points) {
    const point = chart.points[idx]
    const diff = Math.abs(point.distance - distance)

    if (diff < min) {
      min = diff
      minIndex = idx
    }

    if (point.distance > distance) {
      break
    }
  }

  return chart.points[minIndex]
}

const makeChartCanvas = (chart) => {
  const canvas = document.getElementById("elevation-chart")
  const ctx = canvas.getContext("2d")

  resizeCanvas(canvas)
  drawElevationGuides(ctx, chart.elevations)
  drawElevations(ctx, chart.points)
}

let marker
const MARKER_STYLES = {
  color: "red",
  radius: 40,
}

const makeOverlayCanvas = (chart, map) => {
  const canvas = document.getElementById("elevation-overlay")
  const ctx = canvas.getContext("2d")

  resizeCanvas(canvas)

  canvas.addEventListener("mouseout", () => {
    if (marker) {
      marker.remove()
      marker = null
    }

    clear(ctx)
  })

  canvas.addEventListener("mousemove", e => {
    const x = Math.min(Math.max(e.layerX - PADDING_LEFT, 0), chart.dataWidth)
    const distance = chart.distance * (x / chart.dataWidth)
    const point = findClosestPoint(chart, distance)

    const leafletCoordinates = [point.coordinates[1], point.coordinates[0]]

    if (!marker) {
      marker = new Circle(leafletCoordinates, MARKER_STYLES).addTo(map)
    } else {
      marker.setLatLng(leafletCoordinates)
    }

    clear(ctx)

    ctx.strokeStyle = "indigo"
    ctx.fillStyle = "indigo"
    ctx.lineWidth = 1

    ctx.beginPath()
    ctx.arc(point.x, point.y, 5, 0, 2 * Math.PI)
    ctx.stroke()

    const h = `Height: ${Math.round(point.elevation)}m`
    const d = `Distance: ${point.distance.toFixed(2)}km`
    const g = `Grade: ${point.grade.toFixed(1).padStart(4)}%`
    const t = `${new Date(point.time * 1000).toLocaleString("hr")}`

    ctx.font = "12px monospace"
    ctx.fillStyle = "#666"
    ctx.fillText(`${h}  ${d}  ${g}  ${t}`, 10, canvas.height - 10)
  })
}

const makeMap = linestring => {
  const map = new Map("map", { scrollWheelZoom: false })
  const tilesUrl = "https://tile.openstreetmap.org/{z}/{x}/{y}.png"
  const attribution = '&copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  new TileLayer(tilesUrl, { attribution }).addTo(map)
  const geoJSON = new GeoJSON(linestring).addTo(map)
  map.fitBounds(geoJSON.getBounds())
  return map
}

const clear = ctx => ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)

const target = document.getElementById("elevation")
const distances = loadData("distances")
const elevations = loadData("elevations")
const linestring = loadData("linestring")
const times = loadData("times")
const chart = crunchChartData(target, elevations, times, distances, linestring)

const map = makeMap(linestring)
makeChartCanvas(chart)
makeOverlayCanvas(chart, map)
