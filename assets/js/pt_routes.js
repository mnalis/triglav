import { CircleMarker, Control, GeoJSON, Icon, Map, Marker, TileLayer } from "leaflet"

const map = new Map("routes-map")
const tilesUrl = "https://tile.openstreetmap.org/{z}/{x}/{y}.png"
const attribution = '&copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'

const gtfsStopsData = JSON.parse(document.getElementById("gtfs-stops").innerHTML)
const osmRelationsData = JSON.parse(document.getElementById("osm-relations").innerHTML)

const icon = new Icon({ iconUrl: "/images/stop.svg", iconSize: [14, 14] })
const colorFromProps = feature => ({ color: feature.properties?.color })

const gtfsStops = new GeoJSON(gtfsStopsData, {
  pointToLayer: (feature, latlng) => {
    const tooltip = `
      <b>${feature.properties.name}</b><br />
      GTFS ID: ${feature.properties.ref}<br />
      Use count: ${feature.properties.use_count}
    `
    return new Marker(latlng, { icon }).bindTooltip(tooltip)
  },
}).addTo(map)

const layers = { "GTFS Stops": gtfsStops }

for (const name of Object.keys(osmRelationsData)) {
  const featureCollection = new GeoJSON(osmRelationsData[name], {
    style: colorFromProps,
    pointToLayer: (feature, latlng) => {
      const tooltip = `
        <b>${feature.properties.name}</b><br/>
        OSM Relation: ${feature.properties.relation_id}<br/>
        Sequence: ${feature.properties.sequence_id}<br/>
        GTFS ID: ${feature.properties.gtfs_stop_id || "MISSING"}
      `
      return new CircleMarker(latlng, { radius: 8 }).bindTooltip(tooltip)
    },
  })
  featureCollection.addTo(map)
  layers[name] = featureCollection
}

new TileLayer(tilesUrl, { attribution }).addTo(map)
new Control.Layers({}, layers).addTo(map)
map.fitBounds(gtfsStops.getBounds())
