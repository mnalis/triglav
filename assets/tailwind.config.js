// See the Tailwind configuration guide for advanced usage
// https://tailwindcss.com/docs/configuration

const colors = require('tailwindcss/colors')

module.exports = {
  content: [
    './js/**/*.js',
    '../lib/*_web.ex',
    '../lib/*_web/**/*.*ex'
  ],
  theme: {
    fontFamily: {
      mono: ["Fira Mono", "ui-monospace", "SFMono-Regular", "Menlo", "Monaco", "Consolas", "Liberation Mono", "Courier New", "monospace"]
    },
    extend: {
      colors: {
        "purple": "#54006E",
        "dark-purple": "#2E0063",
        "light-purple": "#7D4696",
      },
    },
  },
  plugins: [
    require('@tailwindcss/forms')
  ]
}
