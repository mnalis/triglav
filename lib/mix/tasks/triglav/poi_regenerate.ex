defmodule Mix.Tasks.Triglav.PoiRegenerate do
  use Mix.Task

  require Logger

  @shortdoc "Regenerate POI mappings"

  @impl Mix.Task
  def run(_args) do
    Application.put_env(:triglav, :repo_only, true)
    Mix.Task.run("app.start")
    Logger.info("Regenerating POI mappings...")
    {time, _} = :timer.tc(fn -> Triglav.Poi.regenerate_all() end)
    time_seconds = :erlang.float_to_binary(time / 1_000_000, decimals: 2)
    Logger.info("Done. Took #{time_seconds} seconds.")
  end
end
