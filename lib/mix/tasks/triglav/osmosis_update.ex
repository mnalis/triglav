defmodule Mix.Tasks.Triglav.OsmosisUpdate do
  use Mix.Task

  alias Triglav.PublicTransport

  require Logger

  @shortdoc "Updates the osmosis data to latest available."

  @impl Mix.Task
  def run(args) do
    Application.put_env(:triglav, :repo_only, true)
    {:ok, _} = Application.ensure_all_started(:triglav)
    {opts, _} = OptionParser.parse!(args, strict: [max_count: :integer])
    max_count = Keyword.get(opts, :max_count)

    case Triglav.Import.Osmosis.update(max_count) do
      {:ok, :no_update_available} ->
        Logger.info("Done. No update available")

      {:ok, {:updated_to, state}} ->
        Logger.info("Updated to #{state.sequence_number} #{state.timestamp}")

        Logger.info("Regenerating derived data")
        PublicTransport.regenerate_derived_data()

      {:error, error} ->
        Logger.error("Update failed. Error:\n#{inspect(error)}")
    end
  end
end
