defmodule Mix.Tasks.Triglav.PtRegenerate do
  use Mix.Task

  alias Triglav.PublicTransport
  alias Triglav.Tooling

  require Logger

  @shortdoc "Regenerate public transport mappings and errors"

  @impl Mix.Task
  def run(_args) do
    Application.put_env(:triglav, :repo_only, true)
    {:ok, _} = Application.ensure_all_started(:triglav)

    Tooling.time(fn ->
      PublicTransport.regenerate_derived_data()
    end)
  end
end
