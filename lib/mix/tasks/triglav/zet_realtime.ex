defmodule Mix.Tasks.Triglav.ZetRealtime do
  use Mix.Task

  alias Triglav.Zet.Realtime

  @shortdoc "Dump zet realtime data as JSON"

  @impl Mix.Task
  def run(_) do
    Realtime.fetch!()
    |> Protobuf.JSON.encode!()
    |> IO.puts()
  end
end
