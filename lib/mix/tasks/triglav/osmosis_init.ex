defmodule Mix.Tasks.Triglav.OsmosisInit do
  use Mix.Task

  alias Triglav.Import.Osmosis

  @shortdoc "(re)Creates the Osmosis schema and loads the initial data set"

  @impl Mix.Task
  def run(_args) do
    Application.put_env(:triglav, :repo_only, true)
    {:ok, _} = Application.ensure_all_started(:triglav)

    if not Osmosis.schema_exists?() or confirm() do
      Osmosis.create_schema()
      Osmosis.load_initial()
    else
      IO.puts("Aborted")
    end
  end

  defp confirm() do
    response =
      IO.gets("\nOsmosis schema already exists. This will overwrite it. Are you sure? [y/N]: ")
      |> String.trim()

    String.downcase(response) == "y"
  end
end
