defmodule Mix.Tasks.Triglav.PtUpdate do
  use Mix.Task

  alias Triglav.PublicTransport

  require Logger

  @shortdoc "Update public transport feeds"

  @impl Mix.Task
  def run(args) do
    Application.put_env(:triglav, :repo_only, true)
    {:ok, _} = Application.ensure_all_started(:triglav)
    {opts, operators} = OptionParser.parse!(args, strict: [force: :boolean])
    opts = if operators == [], do: opts, else: Keyword.put(opts, :operators, operators)

    {time, _} = :timer.tc(fn -> PublicTransport.update_all(opts) end)
    time_seconds = :erlang.float_to_binary(time / 1_000_000, decimals: 2)
    Logger.info("Done. Took #{time_seconds} seconds.")
  end
end
