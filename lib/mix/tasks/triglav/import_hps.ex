defmodule Mix.Tasks.Triglav.ImportHps do
  use Mix.Task
  alias Triglav.Hps

  @shortdoc "Imports track data from HPS"

  @impl Mix.Task
  def run(_args) do
    Application.put_env(:triglav, :repo_only, true)
    {:ok, _} = Application.ensure_all_started(:triglav)
    Hps.import_tracks()
  end
end
