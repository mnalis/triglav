defmodule Triglav do
  @moduledoc """
  Triglav keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  def tmp_dir() do
    with dir when is_binary(dir) <- System.tmp_dir(),
         path = Path.join([dir, "triglav_tmp"]),
         :ok <- File.mkdir_p(path) do
      {:ok, path}
    else
      _ -> :error
    end
  end

  def tmp_dir!() do
    path = System.tmp_dir() |> Path.join("triglav_tmp")
    File.mkdir_p!(path)
    path
  end
end
