defmodule TriglavWeb.ViewHelpers do
  alias Phoenix.Controller
  alias TriglavWeb.Router.Helpers

  def static_path(path) do
    Helpers.static_path(TriglavWeb.Endpoint, path)
  end

  def title(conn, assigns) do
    module = Controller.view_module(conn)
    template = Controller.view_template(conn)

    try do
      title = module.title(template, assigns)
      "#{title} – Triglav"
    rescue
      # Not title function matched
      FunctionClauseError -> "Triglav"
    end
  end

  def format_float(float, decimals \\ 2) do
    to_string(:io_lib.format("~.#{decimals}f", [float]))
  end

  def active?(conn, path) do
    String.starts_with?(conn.request_path, path)
  end

  def classes({class, condition}), do: if(condition, do: class, else: "")
  def classes(input) when is_atom(input), do: to_string(input)
  def classes(input) when is_binary(input), do: input
  def classes(input) when is_list(input), do: Enum.map_join(input, " ", &classes/1)

  def classes(input) when is_map(input) do
    input
    |> Enum.filter(fn {_, v} -> v end)
    |> Enum.map(fn {k, _} -> k end)
    |> classes()
  end
end
