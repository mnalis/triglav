defmodule TriglavWeb.OsmHelpers do
  @moduledoc """
  Helpers for displaying OSM data.
  """
  use Phoenix.Component

  alias Triglav.PublicTransport.Schemas.{Operator, Platform}
  alias Triglav.Schemas.Osmosis.Node
  alias Triglav.Schemas.Osmosis.Relation
  alias Triglav.Schemas.Osmosis.Way
  alias TriglavWeb.Router.Helpers

  @doc """
  Renders a link to the relation on OpenStreetMap website.
  """
  @spec osm_link(Node.t() | Way.t() | Relation.t(),
          name: boolean(),
          josm: boolean(),
          tags: [String.t() | atom()]
        ) :: Phoenix.LiveView.Rendered.t()
  def osm_link(element, opts \\ []) do
    assigns = %{
      id: element.id,
      name: element.tags["name"],
      image_url: osm_image(element),
      image_alt: osm_image_alt(element),
      target_url: osm_url(element),
      josm_link: Triglav.Josm.load_object(element),
      tags: get_tags(element, opts)
    }

    ~H"""
    <span class="whitespace-nowrap">
      <img class="h-4 inline-block" src={@image_url} alt={@image_alt} />
      <a href={@target_url}><%= @id %></a>
      <%= if opts[:name] do %><%= @name %><% end %>

      <%= if !Enum.empty?(@tags) do %>
        <span class="tag text-xs">
          <%= for {k, v} <- @tags do %>[<%= k %>=<%= v %>]<% end %>
        </span>
      <% end %>

      <%= if opts[:josm] do %>
        <%= josm_load_objects([element]) %>
      <% end %>
    </span>
    """
  end

  def josm_load_objects(objects, opts \\ []) do
    {title, opts} = Keyword.pop(opts, :title, "Load")

    render_josm_link(%{
      href: Triglav.Josm.load_objects(objects, opts),
      title: title,
      text: title
    })
  end

  def josm_load_platforms(route_variant, opts \\ []) do
    for platform_mapping <- route_variant.platforms do
      for mapping <- platform_mapping.platform.mappings do
        [mapping.way, mapping.node]
      end
    end
    |> List.flatten()
    |> Enum.reject(&is_nil/1)
    |> josm_load_objects(opts)
  end

  def josm_zoom(lat, lon, opts \\ []) do
    title = Keyword.get(opts, :title, "Zoom")

    render_josm_link(%{
      href: Triglav.Josm.zoom(lat, lon),
      title: title,
      text: title
    })
  end

  def josm_add_tags(object, tags) do
    str_tags = Enum.map_join(tags, ",", fn {k, v} -> "#{k}=#{v}" end)
    title_base = if map_size(tags) > 1, do: "Add tags", else: "Add tag"

    render_josm_link(%{
      href: Triglav.Josm.add_tags(object, tags),
      title: "Add tags",
      text: "#{title_base} [#{str_tags}]"
    })
  end

  @spec josm_add_platform_node(Operator.t(), Platform.t()) :: Phoenix.LiveView.Rendered.t()
  def josm_add_platform_node(operator, platform) do
    {lon, lat} = platform.geometry.coordinates

    # TODO: ADAPT FOR TRAMS
    josm_add_node(lat, lon, %{
      "gtfs:stop_id": platform.ref,
      bus: "yes",
      highway: "bus_stop",
      name: platform.name,
      official_name: platform.name,
      public_transport: "platform",
      source: operator.ref
    })
  end

  def josm_add_node(lat, lon, tags) do
    render_josm_link(%{
      href: Triglav.Josm.add_node(lat, lon, tags),
      title: "Create a platform node at coordinates given by GTFS",
      text: "Add node"
    })
  end

  @doc """
  Creates a (relatively) rectangular closed way centered on the given coordinates.
  """
  def josm_add_square_way(lat, lon, tags) do
    # 0.0001 degrees ~= 10m
    lat1 = Float.round(lat - 0.00005, 5)
    lat2 = Float.round(lat + 0.00005, 5)
    lon1 = Float.round(lon - 0.0001, 5)
    lon2 = Float.round(lon + 0.0001, 5)

    points = [{lat1, lon1}, {lat1, lon2}, {lat2, lon2}, {lat2, lon1}, {lat1, lon1}]

    render_josm_link(%{
      href: Triglav.Josm.add_way(points, tags),
      title: "Add way",
      text: "Add way"
    })
  end

  defp render_josm_link(assigns) do
    ~H"""
    <a class="josm-remote" href={@href} title={@title} target="josm">
      <%= @text %>
    </a>
    """
  end

  @spec route_hierarchy([Relation.t()], [Relation.t()]) :: Phoenix.LiveView.Rendered.t()
  def route_hierarchy(masters, routes) do
    routes_map = Map.new(routes, &{&1.id, &1})

    annotated_masters =
      for master <- masters do
        members = Enum.map(master.members, &{&1.member_id, routes_map[&1.member_id]})
        {master, members}
      end

    matched_route_ids =
      annotated_masters
      # Handle the possiblity that member is not included in `rotues`
      |> Enum.map(fn {_master, members} ->
        Enum.map(members, fn {id, _rel} -> id end)
      end)
      |> List.flatten()

    unmatched_routes = Enum.reject(routes, &(&1.id in matched_route_ids))

    assigns = %{
      relations: Enum.concat(masters, routes),
      annotated_masters: annotated_masters,
      unmatched_routes: unmatched_routes
    }

    ~H"""
    <ul class="list-none ml-0">
      <%= for {master, routes} <- @annotated_masters do %>
        <li><%= osm_link(master, name: true, tags: ["type", "ref"]) %></li>
        <%= for {_id, route} <- routes do %>
          <%= if route do %>
            <li>↳ <%= osm_link(route, name: true, tags: ["type", "ref"]) %></li>
          <% end %>
        <% end %>
      <% end %>
      <%= for route <- @unmatched_routes do %>
        <li><%= osm_link(route, name: true, tags: ["type", "ref"]) %></li>
      <% end %>
    </ul>
    """
  end

  defp osm_url(%Node{} = node), do: osm_path("/node/#{node.id}")
  defp osm_url(%Way{} = way), do: osm_path("/way/#{way.id}")
  defp osm_url(%Relation{} = relation), do: osm_path("/relation/#{relation.id}")
  defp osm_path(path), do: "https://www.openstreetmap.org#{path}"

  defp osm_image(%Node{}), do: static_path("/images/osm/node.svg")
  defp osm_image(%Way{}), do: static_path("/images/osm/way.svg")
  defp osm_image(%Relation{}), do: static_path("/images/osm/relation.svg")

  defp osm_image_alt(%Node{}), do: "Node"
  defp osm_image_alt(%Way{}), do: "Way"
  defp osm_image_alt(%Relation{}), do: "Relation"

  defp static_path(path), do: Helpers.static_path(TriglavWeb.Endpoint, path)

  defp get_tags(object, opts) do
    opts
    |> Keyword.get(:tags, [])
    |> Enum.map(&to_string/1)
    |> Enum.filter(&Map.has_key?(object.tags, &1))
    |> Enum.map(&{&1, Map.get(object.tags, &1)})
  end
end
