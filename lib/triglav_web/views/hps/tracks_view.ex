defmodule TriglavWeb.Hps.TracksView do
  use TriglavWeb, :view

  def title("index.html", _), do: "HPS Tracks"
  def title("detail.html", %{track: track}), do: "HPS Track #{track.route_no}: #{track.name}"
end
