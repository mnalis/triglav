defmodule TriglavWeb.PublicTransportView do
  use TriglavWeb, :view

  import TriglavWeb.Components.PublicTransport

  def title("data.html", assigns), do: "#{operator(assigns)} data"
  def title("errors_list.html", assigns), do: "#{operator(assigns)} errors"
  def title("errors_detail.html", assigns), do: "#{operator(assigns)} errors"
  def title("index.html", _), do: "Public Transport"
  def title("route.html", assigns), do: "#{operator(assigns)} #{route(assigns)}"
  def title("match.html", assigns), do: "#{operator(assigns)} #{route(assigns)} find match"
  def title("routes.html", assigns), do: "#{operator(assigns)} routes"
  def title("stops.html", assigns), do: "#{operator(assigns)} stops"

  defp operator(assigns), do: assigns.feed.operator.ref
  defp route(assigns), do: "#{String.capitalize(to_string(assigns.route.route_type))} #{assigns.route.ref}"
end
