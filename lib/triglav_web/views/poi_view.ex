defmodule TriglavWeb.PoiView do
  use TriglavWeb, :view

  import TriglavWeb.Components.Poi

  def title("index.html", _), do: "POI"
  def title("source.html", %{"source" => source}), do: "POI #{source}"
end
