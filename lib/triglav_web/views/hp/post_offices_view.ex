defmodule TriglavWeb.HP.PostOfficesView do
  use TriglavWeb, :view

  def title("index.html", _), do: "HP Post Offices"
end
