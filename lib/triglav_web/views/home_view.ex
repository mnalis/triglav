defmodule TriglavWeb.HomeView do
  use TriglavWeb, :view

  def title("index.html", _), do: "Home"
  def title("data.html", _), do: "Data overview"
end
