defmodule TriglavWeb.PoiController do
  use TriglavWeb, :controller

  action_fallback TriglavWeb.FallbackController

  alias Triglav.Repo
  alias Triglav.Poi
  alias Triglav.Poi.Source

  def index(conn, _params) do
    sources = Triglav.Poi.Sources.all()
    render(conn, "index.html", sources: sources)
  end

  def nodes(conn, %{"source" => source}) do
    with {:ok, source} <- Repo.fetch_by(Source, slug: source) do
      nodes = Poi.nodes(source) |> Repo.preload(mapping: [:osm_node, :osm_way])
      render(conn, "nodes.html", source: source, nodes: nodes)
    end
  end
end
