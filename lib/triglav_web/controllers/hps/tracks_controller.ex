defmodule TriglavWeb.Hps.TracksController do
  use TriglavWeb, :controller

  alias Triglav.Hps
  alias Triglav.Repo

  def index(conn, params) do
    region = Map.get(params, "region")
    regions = Hps.track_count_by_region()
    tracks = Hps.list_tracks(region: region)

    render(conn, "index.html", tracks: tracks, region: region, regions: regions)
  end

  def detail(conn, %{"id" => id}) do
    track = Hps.get_track(id) |> Repo.preload(trace: :waypoints)
    times = Enum.map(track.trace.times, &serialize_datetime/1)

    render(conn, "detail.html",
      track: track,
      elevations: Jason.encode!(track.trace.elevations),
      times: Jason.encode!(times),
      distances: Jason.encode!(track.trace.distances),
      linestring: Jason.encode!(track.trace.linestring)
    )
  end

  defp serialize_datetime(nil), do: nil
  defp serialize_datetime(dt), do: DateTime.to_unix(dt)
end
