defmodule TriglavWeb.HP.PostOfficesController do
  use TriglavWeb, :controller

  alias Triglav.HP

  def index(conn, _params) do
    mappings = HP.list_post_office_mappings()

    render(conn, "index.html", mappings: mappings)
  end
end
