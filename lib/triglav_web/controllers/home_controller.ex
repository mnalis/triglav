defmodule TriglavWeb.HomeController do
  use TriglavWeb, :controller

  alias Triglav.Zet.Osmosis

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def data(conn, _params) do
    replication_changes = Osmosis.list_replication_changes()

    render(conn, "data.html", replication_changes: replication_changes)
  end
end
