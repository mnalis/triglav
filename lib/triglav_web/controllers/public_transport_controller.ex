defmodule TriglavWeb.PublicTransportController do
  use TriglavWeb, :controller

  action_fallback TriglavWeb.FallbackController

  alias Triglav.GeoJSON
  alias Triglav.Import.Geofabrik
  alias Triglav.Osm
  alias Triglav.PublicTransport
  alias Triglav.PublicTransport.Schemas.{Error, ErrorHistory, Feed, Platform, Route, RouteRelation}
  alias Triglav.Repo
  alias Triglav.Schemas.Osmosis.Relation
  alias Triglav.Zet.Osmosis
  alias TriglavWeb.Charts

  import Ecto.Query

  def index(conn, _params) do
    feeds =
      Repo.all(
        from feed in Feed,
          join: operator in assoc(feed, :operator),
          where: [active: true],
          preload: [operator: operator],
          order_by: [asc: operator.name]
      )

    render(conn, "index.html", feeds: feeds)
  end

  def routes(conn, %{"operator" => operator_slug}) do
    with {:ok, feed} <- fetch_active_feed(operator_slug) do
      osm_state = Geofabrik.get_local_state()
      counts = PublicTransport.matched_counts(feed.id)

      routes =
        Repo.all(
          from Route,
            where: [feed_id: ^feed.id],
            order_by: :order_no,
            preload: [
              :mapped_route_relations,
              [mapped_route_master_relations: :members]
            ]
        )

      mapped_relation_ids =
        routes
        |> Enum.flat_map(& &1.mapped_route_relations)
        |> Enum.map(& &1.id)

      unmatched_relations =
        Repo.all(
          from route_relation in RouteRelation,
            join: relation in assoc(route_relation, :relation),
            where: route_relation.operator_id == ^feed.operator_id,
            where: route_relation.id not in ^mapped_relation_ids,
            select: relation
        )
        |> Enum.sort_by(&Integer.parse(Map.get(&1.tags, "ref", "")))

      errors =
        from(error in Error, where: error.operator_id == ^feed.operator_id and is_nil(error.resolved_at))
        |> Repo.all()
        |> Enum.group_by(& &1.route_ref)

      render(conn, "routes.html",
        counts: counts,
        errors: errors,
        feed: feed,
        osm_state: osm_state,
        routes: routes,
        unmatched_relations: unmatched_relations
      )
    end
  end

  def stops(conn, %{"operator" => operator_slug} = params) do
    show_unused = Map.get(params, "show_unused") == "1"

    with {:ok, feed} <- fetch_active_feed(operator_slug) do
      count = PublicTransport.count_platforms(feed.id)

      platforms =
        PublicTransport.list_platforms(feed.id, hide_unused: !show_unused)
        |> Repo.preload(mappings: [:way, :node])

      platforms_geojson =
        platforms
        |> Enum.map(&Platform.to_feature/1)
        |> GeoJSON.feature_collection()
        |> Jason.encode!()

      render(conn, "stops.html",
        count: count,
        feed: feed,
        platforms: platforms,
        platforms_geojson: platforms_geojson,
        show_unused: show_unused
      )
    end
  end

  def route(conn, %{"operator" => operator_slug, "ref" => ref}) do
    osm_state = Geofabrik.get_local_state()

    with {:ok, feed} <- fetch_active_feed(operator_slug),
         {:ok, route} <- Repo.fetch_by(Route, feed_id: feed.id, ref: ref) do
      # TODO: oh boy, much preload....
      route =
        Repo.preload(route,
          mapped_route_master_relations: :members,
          mapped_route_relations: :members,
          variants: [:mapped_relation, platforms: [platform: [mappings: [:way, :node]]]]
        )

      errors =
        Repo.all(
          from(
            error in Error,
            where: error.operator_id == ^feed.operator_id,
            where: error.route_ref == ^route.ref,
            where: is_nil(error.resolved_at)
          )
        )

      route_errors = Enum.filter(errors, &is_nil(&1.relation_id))

      relation_errors =
        errors
        |> Enum.filter(&(!is_nil(&1.relation_id)))
        |> Enum.group_by(& &1.relation_id)

      platform_errors =
        errors
        |> Enum.filter(&(!is_nil(&1.relation_id) and !is_nil(&1.sequence_id)))
        |> Enum.group_by(& &1.relation_id)
        |> Map.new(fn {relation_id, errors} ->
          {relation_id, Enum.group_by(errors, & &1.sequence_id)}
        end)

      variants_by_relation_id =
        route.variants
        |> Enum.filter(& &1.mapped_relation)
        |> Map.new(&{&1.mapped_relation.id, &1})

      route_relations = Osm.preload_members(route.mapped_route_relations)

      render(conn, "route.html",
        feed: feed,
        osm_relations: Enum.concat(route.mapped_route_master_relations, route.mapped_route_relations),
        osm_state: osm_state,
        route: route,
        route_relations: route_relations,
        variants: Enum.group_by(route.variants, & &1.direction),
        variants_by_relation_id: variants_by_relation_id,
        chart_data: chart_data(route, route_relations),
        route_errors: route_errors,
        relation_errors: relation_errors,
        platform_errors: platform_errors,
        has_errors: length(errors) > 0
      )
    end
  end

  def match(conn, %{"operator" => operator_slug, "ref" => ref, "relation_id" => relation_id}) do
    with {:ok, feed} <- fetch_active_feed(operator_slug),
         {:ok, route} <- Repo.fetch_by(Route, feed_id: feed.id, ref: ref) do
      relation = Repo.get!(Relation, relation_id)
      route = Repo.preload(route, variants: [platforms: :platform])
      platform_members = Osmosis.list_platform_members(relation_id)

      osm_platforms =
        platform_members
        |> Enum.map(& &1.member)
        |> Enum.filter(& &1.tags["gtfs:stop_id"])
        |> Map.new(&{&1.tags["gtfs:stop_id"], &1})

      gtfs_platforms =
        route.variants
        |> Enum.flat_map(fn variant -> variant.platforms end)
        |> Map.new(&{&1.platform.ref, &1})

      osm_stop_ids = Enum.map(platform_members, & &1.member.tags["gtfs:stop_id"])

      diffs =
        for variant <- route.variants do
          diffs = List.myers_difference(osm_stop_ids, variant.stop_ids)
          %{variant: variant, diffs: diffs, eq_count: count_eq(diffs)}
        end
        |> Enum.sort_by(& &1.eq_count, &>=/2)

      render(conn, "match.html",
        feed: feed,
        route: route,
        relation: relation,
        diffs: diffs,
        osm_platforms: osm_platforms,
        gtfs_platforms: gtfs_platforms
      )
    end
  end

  defp count_eq(diffs) do
    diffs
    |> Enum.filter(fn {k, _} -> k == :eq end)
    |> Enum.map(fn {_, v} -> length(v) end)
    |> Enum.sum()
  end

  defp chart_data(route, route_relations) do
    gtfs_stops = Charts.gtfs_stops_geojson(route) |> Jason.encode!()
    osm_relations = Charts.osm_relations_geojson(route_relations) |> Jason.encode!()

    %{
      gtfs_stops: gtfs_stops,
      osm_relations: osm_relations
    }
  end

  def errors_list(conn, %{"operator" => operator_slug}) do
    with {:ok, feed} <- fetch_active_feed(operator_slug) do
      histories =
        from(ErrorHistory,
          where: [operator_id: ^feed.operator_id],
          order_by: [desc: :timestamp],
          limit: 100
        )
        |> Repo.all()

      render(conn, "errors_list.html", feed: feed, histories: histories)
    end
  end

  def errors_detail(conn, %{"operator" => operator_slug, "id" => id}) do
    with {:ok, feed} <- fetch_active_feed(operator_slug),
         {:ok, history} <- Repo.fetch_by(ErrorHistory, operator_id: feed.operator_id, id: id) do
      render(conn, "errors_detail.html",
        feed: feed,
        history: history,
        created_errors: list_errors(history.created_error_ids),
        resolved_errors: list_errors(history.resolved_error_ids)
      )
    end
  end

  defp list_errors(ids),
    do: Repo.all(from(e in Error, where: e.id in ^ids, order_by: e.route_ref))

  def data(conn, %{"operator" => operator_slug}) do
    with {:ok, feed} <- fetch_active_feed(operator_slug) do
      counts = PublicTransport.feed_counts(feed.operator_id)
      render(conn, "data.html", feed: feed, counts: counts)
    end
  end

  defp fetch_active_feed(operator_slug) do
    from(feed in Feed,
      join: operator in assoc(feed, :operator),
      preload: [operator: operator],
      where: feed.active and operator.slug == ^operator_slug
    )
    |> Repo.fetch_one()
  end
end
