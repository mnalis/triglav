<main role="main" class="container-wide">
  <h1><%= @source.name %> POI</h1>

  <%= if length(@nodes) > 0 do %>
    <div class="flex space-x-4 mb-4">
      <button id="expand-all" class="button small">Expand all</button>
      <button id="collapse-all" class="button small">Collapse all</button>
    </div>

    <table>
      <tr>
        <th>Ref</th>
        <th>Name</th>
        <th>OSM element</th>
        <th>Distance</th>
        <th>Tags</th>
        <th>JOSM</th>
      </tr>
      <%= for node <- @nodes do %>
        <%
          mapping = node.mapping
          {lon, lat} = node.geometry.coordinates
          osm_element = if mapping, do: mapping.osm_node || mapping.osm_way
          distance_class = classes(["text-center", error: mapping && mapping.distance > 50])
        %>
        <tr>
          <td><%= node.ref %></td>
          <td><%= node.name %></td>
          <%= if mapping do %>
            <td><%= osm_link(osm_element) %></td>
            <td class={distance_class}>
              <%= round(mapping.distance) %>m
            </td>
          <% else %>
            <td colspan="2">
              <%= josm_add_node(lat, lon, node.tags) %>
              <%= josm_add_square_way(lat, lon, node.tags) %>
            </td>
          <% end %>
          <td>
            <.tag_diff node={node} />
          </td>
          <td class="whitespace-nowrap">
            <%= josm_zoom(lat, lon) %>
            <%= if osm_element do %>
              <%= josm_load_objects([osm_element]) %>
            <% end %>
          </td>
        </tr>
      <% end %>
    </table>
  <% else %>
    <p>No nodes loaded for this source.</p>
  <% end %>
</main>

<script>
document.getElementById("expand-all").addEventListener("click", () => {
  const elements = document.getElementsByTagName("details")
  for (const element of elements) {
    element.setAttribute("open", true)
  }
})

document.getElementById("collapse-all").addEventListener("click", () => {
  const elements = document.getElementsByTagName("details")
  for (const element of elements) {
    element.removeAttribute("open")
  }
})
</script>