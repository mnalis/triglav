<.navbar conn={@conn} feed={@feed} />

<main role="main" class="container-wide">
  <h1>Routes</h1>

  <div class="flex">
    <table>
      <tr>
        <th colspan="2">Route data</th>
      </tr>
      <tr>
        <th>Operator</th>
        <td><%= @feed.operator.name %></td>
      </tr>
      <tr>
        <th>Version</th>
        <td><%= @feed.ref %></td>
      </tr>
      <tr>
        <th>Imported at</th>
        <td><%= to_string(@feed.imported_at) %></td>
      </tr>
    </table>

    <table class="ml-4">
      <tr>
        <th colspan="2">OSM</th>
      </tr>
      <tr>
        <th>Source</th>
        <td>
          <a href="https://download.geofabrik.de/europe/croatia.html">Geofabrik</a>
        </td>
      </tr>
      <tr>
        <th>Sequence no.</th>
        <td><%= @osm_state.sequence_number %></td>
      </tr>
      <tr>
        <th>Imported at</th>
        <td><%= to_string(@osm_state.timestamp) %></td>
      </tr>
    </table>
  </div>

  <h3 id="routes">Routes</h3>

  <table>
    <thead>
      <tr>
        <th>Type</th>
        <th>Ref</th>
        <th>Name</th>
        <th>Counts</th>
        <th>Relations</th>
        <th>Errors</th>
      </tr>
    </thead>
    <tbody>
      <%= for route <- @routes do %>
        <% counts = Map.fetch!(@counts, route.id) %>
        <tr>
          <td><%= String.capitalize(to_string(route.route_type)) %></td>
          <td><%= route.ref %></td>
          <td>
            <a href={Routes.public_transport_path(@conn, :route, @feed.operator.slug, route.ref)}>
              <%= route.name %>
            </a>
          </td>
          <% good? = counts.matched_count == counts.total_count %>
          <% bad? = counts.unmatched_count > 0 %>
          <td class={classes(["whitespace-nowrap", %{error: bad?, success: good?}])}>
            <span class={classes(%{"text-green-900" => good?})}>
              Matched: <%= counts.matched_count %>/<%= counts.total_count %>
            </span>
            <%= if counts.unmatched_count > 0 do %>
              <br />
              <span class="text-red-900" >Unmatched: <%= counts.unmatched_count %></span>
            <% end %>
          </td>
          <td>
            <%= route_hierarchy(
              route.mapped_route_master_relations,
              route.mapped_route_relations
            ) %>
          </td>
          <td>
            <% errors = Map.get(@errors, route.ref, []) %>
            <%= if not Enum.empty?(errors) do %>
              <ul class="text-red-900">
              <%= for error <- errors do %>
                <li><.error error={error} /></li>
              <% end %>
              </ul>
            <% end %>
          </td>
        </tr>
      <% end %>
    </tbody>
  </table>

  <%= if @unmatched_relations != [] do %>
    <h3 id="unmatched_relations">Unmatched relations</h3>

    <p>Public transport relations found in OSM but not matched by any GTFS route.</p>

    <table>
      <%= for relation <- @unmatched_relations do %>
      <tr>
        <td><%= osm_link(relation, name: true, tags: ["type", "ref"]) %></td>
        <td><%= josm_load_objects([relation]) %></td>
      </tr>
      <% end %>
    </table>
  <% end %>

</main>
