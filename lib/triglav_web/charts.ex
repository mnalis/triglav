defmodule TriglavWeb.Charts do
  alias Triglav.GeoJSON
  alias Triglav.PublicTransport
  alias Triglav.PublicTransport.Schemas.{Platform, Route}
  alias Triglav.Schemas.Osmosis.Relation

  @spec gtfs_stops_geojson(Route.t()) :: GeoJSON.feature_collection()
  def gtfs_stops_geojson(route) do
    route.variants
    |> Enum.flat_map(fn variant -> Enum.map(variant.platforms, & &1.platform) end)
    |> Enum.map(&Platform.to_feature/1)
    |> GeoJSON.feature_collection()
  end

  @spec osm_relations_geojson([Relation.t()]) ::
          %{String.t() => GeoJSON.feature_collection()}
  def osm_relations_geojson(relations) do
    relations
    |> with_color()
    |> Map.new(fn {color, relation} ->
      name = relation.tags["name"]
      route = osm_route_geojson(relation, color)
      platforms = osm_platforms(relation, color)
      {name, GeoJSON.feature_collection([route | platforms])}
    end)
  end

  defp osm_route_geojson(relation, color) do
    # TODO: don't calculate the geometry each time, store it in RouteVariant
    geometry = PublicTransport.route_relation_geometry(relation)
    GeoJSON.feature(geometry, %{color: color})
  end

  defp osm_platforms(relation, color) do
    relation.members
    |> Enum.filter(&String.starts_with?(&1.member_role, "platform"))
    |> Enum.map(fn member ->
      stop_id = member.member.tags["gtfs:stop_id"]
      color = if stop_id, do: color, else: "#eb4d4b"

      GeoJSON.to_feature(member.member, %{
        color: color,
        gtfs_stop_id: stop_id,
        name: member.member.tags["name"],
        relation_id: relation.id,
        sequence_id: member.sequence_id
      })
    end)
  end

  defp with_color(items) do
    [
      light_blue: "#3498db",
      purple: "#8e44ad",
      green: "#27ae60",
      dark_blue: "#3742fa",
      orange: "#d35400",
      dark_gray: "#2c3e50"
    ]
    |> Keyword.values()
    |> Stream.cycle()
    |> Enum.zip(items)
  end
end
