defmodule TriglavWeb.Router do
  use TriglavWeb, :router

  import Phoenix.LiveDashboard.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/", TriglavWeb do
    pipe_through :browser

    get "/", HomeController, :index
    get "/data", HomeController, :data

    get "/public_transport/", PublicTransportController, :index
    get "/public_transport/:operator/data", PublicTransportController, :data
    get "/public_transport/:operator/errors", PublicTransportController, :errors_list
    get "/public_transport/:operator/errors/:id", PublicTransportController, :errors_detail
    get "/public_transport/:operator/routes", PublicTransportController, :routes
    get "/public_transport/:operator/routes/:ref", PublicTransportController, :route
    get "/public_transport/:operator/routes/:ref/match/:relation_id", PublicTransportController, :match
    get "/public_transport/:operator/stops", PublicTransportController, :stops

    get "/poi/", PoiController, :index
    get "/poi/:source", PoiController, :nodes

    scope "/hps", alias: Hps, as: :hps do
      get "/tracks", TracksController, :index
      get "/tracks/:id", TracksController, :detail
    end

    scope "/hp", alias: HP, as: :hps do
      get "/post_offices", PostOfficesController, :index
    end
  end

  scope "/" do
    pipe_through [:browser, :dashboard_auth]
    live_dashboard "/dashboard", metrics: TriglavWeb.Telemetry, ecto_repos: [Triglav.Repo]
  end

  defp dashboard_auth(conn, _opts) do
    username = System.fetch_env!("DASHBOARD_USERNAME")
    password = System.fetch_env!("DASHBOARD_PASSWORD")
    Plug.BasicAuth.basic_auth(conn, username: username, password: password)
  end
end
