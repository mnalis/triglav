defmodule TriglavWeb.Socket do
  use Phoenix.Socket

  channel "zet:*", TriglavWeb.Socket.ZetRealtime

  def id(_socket), do: nil
  def connect(_params, socket, _connect_info), do: {:ok, socket}
end

defmodule TriglavWeb.Socket.ZetRealtime do
  use Phoenix.Channel

  @impl Phoenix.Channel
  def join("zet:realtime", _payload, socket) do
    {:ok, socket}
  end

  def push_update(payload) do
    TriglavWeb.Endpoint.broadcast!("zet:realtime", "update", payload)
  end
end
