defmodule TriglavWeb.Components.PublicTransport do
  use Phoenix.Component

  alias Triglav.PublicTransport.Validation.Errors
  alias TriglavWeb.Router.Helpers, as: Routes

  import TriglavWeb.ViewHelpers

  def navbar(assigns) do
    pages = [
      %{
        name: "Routes",
        path: Routes.public_transport_path(assigns.conn, :routes, assigns.feed.operator.slug),
        active: "routes" in assigns.conn.path_info
      },
      %{
        name: "Stops",
        path: Routes.public_transport_path(assigns.conn, :stops, assigns.feed.operator.slug),
        active: List.last(assigns.conn.path_info) == "stops"
      },
      %{
        name: "Errors",
        path: Routes.public_transport_path(assigns.conn, :errors_list, assigns.feed.operator.slug),
        active: "errors" in assigns.conn.path_info
      },
      %{
        name: "Data",
        path: Routes.public_transport_path(assigns.conn, :data, assigns.feed.operator.slug),
        active: List.last(assigns.conn.path_info) == "data"
      }
    ]

    assigns = assign(assigns, :pages, pages)

    ~H"""
    <nav class="w-full text-white bg-purple">
      <div class="container flex space-x-10 items-baseline">
        <b class="text-lg"><%= @feed.operator.ref %></b>
        <%= for page <- @pages do %>
          <% class = classes(["text-white", %{"font-bold" => page.active}]) %>
          <a class={class} href={page.path}><%= page.name %></a>
        <% end %>
      </div>
    </nav>
    """
  end

  # TODO: use attr once updated to phoenix 1.7
  def error(assigns) do
    Errors.render(assigns.error)
  end
end
