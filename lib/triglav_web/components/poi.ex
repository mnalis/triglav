defmodule TriglavWeb.Components.Poi do
  use Phoenix.Component

  def tag_diff(assigns) do
    poi_node = assigns.node
    osm_element = if poi_node.mapping, do: poi_node.mapping.osm_node || poi_node.mapping.osm_way

    if osm_element do
      diffs = calc_diffs(assigns.node, osm_element)

      summary =
        diffs
        |> Enum.frequencies_by(fn {_, _, _, cmd} -> cmd end)
        |> Map.drop([:delete])
        |> Enum.map(fn {cmd, count} ->
          case cmd do
            :insert -> "#{count} new"
            :diff -> "#{count} different"
            :same -> "#{count} same"
            _ -> nil
          end
        end)
        |> Enum.join(", ")

      ~H"""
      <details>
        <summary><%= summary %></summary>

        <table class="border border-gray-300">
          <thead>
            <tr>
              <th>Key</th>
              <th>Source value</th>
              <th>OSM value</th>
            </tr>
          </thead>
          <tbody>
            <%= for {key, poi_value, osm_value, cmd} <- diffs do %>
              <tr>
                  <td><%= key %></td>
                <%= if cmd == :same do %>
                  <td class="text-gray-600"><%= poi_value %></td>
                  <td class="text-gray-600"><%= osm_value %></td>
                  <td></td>
                <% end %>
                <%= if cmd == :diff do %>
                  <td class="text-red-900"><%= poi_value %></td>
                  <td class="text-red-900"><%= osm_value %></td>
                  <td></td>
                <% end %>
                <%= if cmd == :insert do %>
                  <td class="text-green-900"><%= poi_value %></td>
                  <td><%= osm_value %></td>
                  <td></td>
                <% end %>
                <%= if cmd == :delete do %>
                  <td><%= poi_value %></td>
                  <td class="text-gray-600"><%= osm_value %></td>
                  <td></td>
                <% end %>
              </tr>
            <% end %>
          </tbody>
        </table>
      </details>

      """
    else
      ~H"""
      <details>
        <summary>Show tags</summary>
        <table>
          <thead>
            <tr>
              <th>Key</th>
              <th>Value</th>
            </tr>
          </thead>
          <tbody>
            <%= for {key, value} <- poi_node.tags do %>
              <tr>
                <td><%= key %></td>
                <td><%= value %></td>
              </tr>
            <% end %>
          </tbody>
        </table>
      </details>
      """
    end
  end

  def calc_diffs(poi_node, osm_element) do
    poi_tags = poi_node.tags
    osm_tags = osm_element.tags

    keys =
      Map.keys(poi_tags)
      |> Enum.concat(Map.keys(osm_tags))
      |> Enum.uniq()
      |> Enum.sort()

    for key <- keys do
      poi_value = Map.get(poi_tags, key)
      osm_value = Map.get(osm_tags, key)

      cmd =
        cond do
          is_nil(osm_value) -> :insert
          is_nil(poi_value) -> :delete
          poi_value == osm_value -> :same
          true -> :diff
        end

      {key, poi_value, osm_value, cmd}
    end
  end
end
