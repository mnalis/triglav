defmodule Triglav.Helpers do
  @doc """
  Returns mix environment in which this file was compiled.

  Unlike `Mix.env` this function can be safely used at runtime.

  Compared to the `@mix_env Mix.env` pattern, this fuction will not lead to dialyzer warnings.
  Consider the following code:

      @mix_env Mix.env()

      def foo do
        if @mix_env == :test, do: ...
      end

  Since `@mix_env` is resolved at compile time, the generated code in e.g. `:dev` mix env is going
  to be `if :dev == :test, do: ...` which is always false and leads to dialyzer warnings.

  In contrast, `if Tooling.mix_env() == :test, do: ...` won't cause such warnings.
  """
  @spec mix_env :: :dev | :test | :prod
  def mix_env do
    # We're tricking dialyzer by doing a persistent term lookup under an unknown key. As a result,
    # dialyzer can't deduce the exact value this function returns.
    :persistent_term.get({__MODULE__, :non_existent_key}, unquote(Mix.env()))
  end
end
