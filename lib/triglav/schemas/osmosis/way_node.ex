defmodule Triglav.Schemas.Osmosis.WayNode do
  use Ecto.Schema
  alias Triglav.Schemas.Osmosis.Node
  alias Triglav.Schemas.Osmosis.Way

  @primary_key false
  @schema_prefix :osmosis

  @type t :: %__MODULE__{
          way: Way.t() | Ecto.Association.NotLoaded.t(),
          node: Node.t() | Ecto.Association.NotLoaded.t(),
          sequence_id: non_neg_integer()
        }

  schema "way_nodes" do
    belongs_to :way, Way, primary_key: true
    belongs_to :node, Node
    field :sequence_id, :integer, primary_key: true
  end
end
