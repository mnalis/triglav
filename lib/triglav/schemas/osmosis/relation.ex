defmodule Triglav.Schemas.Osmosis.Relation do
  use Ecto.Schema

  alias Triglav.Osm.Tags
  alias Triglav.Schemas.Osmosis.RelationMember

  @schema_prefix :osmosis
  @type id() :: integer()

  @type t() :: %__MODULE__{
          __meta__: Ecto.Schema.Metadata.t(),
          id: integer() | nil,
          version: non_neg_integer(),
          tags: map(),
          members: [RelationMember.t()] | Ecto.Association.NotLoaded.t()
        }

  schema "relations" do
    field :version, :integer
    field :tags, :map
    has_many :members, RelationMember, foreign_key: :relation_id, references: :id, preload_order: [asc: :sequence_id]
  end

  def type(schema), do: Tags.get(schema, "type")
  def ref(schema), do: Tags.get(schema, "ref")
  def is_route(schema), do: type(schema) == "route"
  def is_route_master(schema), do: type(schema) == "route_master"
  def is_roundtrip(schema), do: Tags.has?(schema, "roundtrip", "yes")
  def is_ptv2(schema), do: Tags.has?(schema, "public_transport:version", "2")
end
