defmodule Triglav.Schemas.Osmosis.RelationMember do
  use Ecto.Schema
  alias Triglav.Schemas.Osmosis.Node
  alias Triglav.Schemas.Osmosis.Relation
  alias Triglav.Schemas.Osmosis.Way

  @primary_key false
  @schema_prefix :osmosis

  @type t() :: %__MODULE__{
          __meta__: Ecto.Schema.Metadata.t(),
          relation: Relation.t() | Ecto.Association.NotLoaded.t(),
          relation_id: integer(),
          sequence_id: integer(),
          member_id: integer(),
          member_type: String.t(),
          member_role: String.t(),
          member: Node.t() | Way.t() | Relation.t()
        }

  schema "relation_members" do
    belongs_to :relation, Relation, primary_key: true
    field :sequence_id, :integer, primary_key: true
    field :member_id, :integer
    field :member_type, :string
    field :member_role, :string

    # Used to populate the member record (node, relation, or way)
    field :member, :map, virtual: true
  end
end
