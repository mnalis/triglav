defmodule Triglav.Schemas.Osmosis.Node do
  use Ecto.Schema

  @schema_prefix :osmosis

  @type id() :: integer()

  @type t() :: %__MODULE__{
          __meta__: Ecto.Schema.Metadata.t(),
          id: integer() | nil,
          version: non_neg_integer(),
          geom: Geo.Point.t()
        }

  schema "nodes" do
    field :version, :integer
    field :tags, :map
    field :geom, Geo.PostGIS.Geometry
  end
end
