defmodule Triglav.Schemas.Osmosis.ReplicationChanges do
  use Ecto.Schema

  @schema_prefix :osmosis

  @type t() :: %__MODULE__{
          __meta__: Ecto.Schema.Metadata.t(),
          tstamp: DateTime.t(),
          nodes_modified: integer(),
          nodes_added: integer(),
          nodes_deleted: integer(),
          ways_modified: integer(),
          ways_added: integer(),
          ways_deleted: integer(),
          relations_modified: integer(),
          relations_added: integer(),
          relations_deleted: integer(),
          changesets_applied: [integer()],
          earliest_timestamp: DateTime.t(),
          latest_timestamp: DateTime.t()
        }

  schema "replication_changes" do
    field :tstamp, :utc_datetime
    field :nodes_modified, :integer
    field :nodes_added, :integer
    field :nodes_deleted, :integer
    field :ways_modified, :integer
    field :ways_added, :integer
    field :ways_deleted, :integer
    field :relations_modified, :integer
    field :relations_added, :integer
    field :relations_deleted, :integer
    field :changesets_applied, {:array, :integer}
    field :earliest_timestamp, :utc_datetime
    field :latest_timestamp, :utc_datetime
  end
end
