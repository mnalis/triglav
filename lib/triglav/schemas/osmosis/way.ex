defmodule Triglav.Schemas.Osmosis.Way do
  use Ecto.Schema

  alias Triglav.Osm.Tags
  alias Triglav.Schemas.Osmosis.Way
  alias Triglav.Schemas.Osmosis.WayNode

  @schema_prefix :osmosis

  @type id() :: integer()

  @type t :: %__MODULE__{
          version: non_neg_integer(),
          tags: map(),
          linestring: Geo.LineString.t(),
          node_ids: [integer()]
        }

  schema "ways" do
    field :version, :integer
    field :tags, :map
    field :linestring, Geo.PostGIS.Geometry
    field :node_ids, {:array, :integer}, source: :nodes

    # To contain the linestring converted to geojson
    field :geojson, :map, virtual: true

    has_many :way_nodes, WayNode
    has_many :nodes, through: [:way_nodes, :node], references: :id
  end

  @spec adjacent?(t(), t()) :: boolean()
  def adjacent?(%Way{} = one, %Way{} = other) do
    cond do
      is_whole_roundabout?(one) ->
        roundabout_adjacent?(one, other)

      is_whole_roundabout?(other) ->
        roundabout_adjacent?(other, one)

      true ->
        linear_adjacent?(one, other)
    end
  end

  defp roundabout_adjacent?(roundabout, way) do
    List.first(way.node_ids) in roundabout.node_ids or
      List.last(way.node_ids) in roundabout.node_ids
  end

  defp linear_adjacent?(one, other) do
    List.first(one.node_ids) == List.first(other.node_ids) or
      List.first(one.node_ids) == List.last(other.node_ids) or
      List.last(one.node_ids) == List.first(other.node_ids) or
      List.last(one.node_ids) == List.last(other.node_ids)
  end

  @doc """
  Returns true if the given way is tagged with `junction=roundabout`.

  Meaning it will return true when the given way is part of a roundabout, not a
  full circular way. See `is_whole_roundabout?/1`.
  """
  @spec is_roundabout?(t()) :: boolean()
  def is_roundabout?(%Way{} = way) do
    Tags.has?(way, "junction", "roundabout") and
      List.first(way.node_ids) == List.last(way.node_ids)
  end

  @doc """
  Returns true if the given way is circular and tagged with
  `junction=roundabout`.
  """
  @spec is_whole_roundabout?(t()) :: boolean()
  def is_whole_roundabout?(%Way{} = way) do
    Tags.has?(way, "junction", "roundabout") and
      List.first(way.node_ids) == List.last(way.node_ids)
  end
end
