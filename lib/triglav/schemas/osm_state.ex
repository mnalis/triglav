defmodule Triglav.Schemas.OsmState do
  use Ecto.Schema
  import Ecto.Changeset

  @type t :: %__MODULE__{}

  schema "osm_state" do
    field :sequence_number, :integer
    field :timestamp, :utc_datetime
    timestamps()
  end

  def changeset(row, params \\ %{}) do
    row
    |> cast(params, [:sequence_number, :timestamp])
  end
end
