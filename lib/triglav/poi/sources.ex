defmodule Triglav.Poi.Sources do
  alias Triglav.Poi.Source
  alias Triglav.Poi.Sources
  alias Triglav.Repo
  import Ecto.Query

  @spec all() :: [Source.t()]
  def all(), do: Source |> order_by(asc: :name) |> Repo.all()

  @spec get!(atom) :: Source.t()
  def get!(slug), do: Repo.get_by!(Source, slug: slug)

  @spec crodux() :: Source.t()
  def crodux(), do: get_or_create!(Sources.Crodux)

  @spec posta() :: Source.t()
  def posta(), do: get_or_create!(Sources.Posta)

  @spec tifon() :: Source.t()
  def tifon(), do: get_or_create!(Sources.Tifon)

  def get_or_create!(source) do
    %Source{
      name: source.name(),
      slug: source.slug()
    }
    |> Repo.insert!(on_conflict: :nothing, conflict_target: :slug)

    Repo.one!(where(Source, slug: ^source.slug()))
  end
end
