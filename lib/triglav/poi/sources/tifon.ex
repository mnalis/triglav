defmodule Triglav.Poi.Sources.Tifon do
  use Tesla, only: [:post]

  plug Tesla.Middleware.FormUrlencoded
  plug Tesla.Middleware.JSON

  plug Tesla.Middleware.Retry,
    delay: :timer.seconds(1),
    max_retries: 5,
    should_retry: fn
      {:ok, %{status: status}} when status != 200 -> true
      {:ok, _} -> false
      {:error, _} -> true
    end

  plug Tesla.Middleware.Timeout, timeout: 10_000
  plug Triglav.Tesla.Middleware.ErrrorOnStatus
  plug Triglav.Tesla.Middleware.Logger

  alias Triglav.Helpers.MapUtils
  alias Triglav.Poi
  alias Triglav.Poi.Source
  alias Triglav.Poi.Sources
  alias Triglav.Repo
  alias Triglav.Schemas.Osmosis

  import Ecto.Query
  import Geo.PostGIS
  import Triglav.Query

  require Logger

  @behaviour Source

  @impl Source
  def name(), do: "Tifon"

  @impl Source
  def slug(), do: "tifon"

  @impl Source
  def fetch() do
    source = Sources.tifon()

    with {:ok, records} <- fetch_list() do
      Enum.reduce_while(records, [], fn record, acc ->
        case fetch_details(record) do
          {:ok, details} ->
            {:cont, [parse_node(source, record, details) | acc]}

          {:error, error} ->
            Logger.error(error)
            {:halt, {:error, error}}
        end
      end)
    end
    |> case do
      {:error, error} -> {:error, error}
      list when is_list(list) -> {:ok, Enum.sort_by(list, & &1.name)}
    end
  end

  @impl Source
  def find_mappings() do
    source = Sources.tifon()

    node_mappings =
      Repo.all(
        from(poi_node in Poi.Node,
          where: poi_node.source_id == ^source.id,
          join: osm_node in Osmosis.Node,
          on:
            tag(osm_node, "amenity") == "fuel" and
              (tag(osm_node, "operator") == "Tifon" or tag(osm_node, "name") == "Tifon") and
              st_distance_in_meters(osm_node.geom, poi_node.geometry) < 100,
          select: {poi_node.id, osm_node.id, st_distance_in_meters(osm_node.geom, poi_node.geometry)},
          order_by: [asc: 1, asc: 3]
        )
      )
      # If multiple nodes are matched keep the closest one only
      |> Enum.dedup_by(fn {node_id, _, _} -> node_id end)
      |> Enum.map(fn {node_id, osm_node_id, distance} ->
        %{
          source_id: source.id,
          node_id: node_id,
          osm_node_id: osm_node_id,
          distance: distance
        }
      end)

    way_mappings =
      Repo.all(
        from(poi_node in Poi.Node,
          where: poi_node.source_id == ^source.id,
          join: osm_way in Osmosis.Way,
          on:
            tag(osm_way, "amenity") == "fuel" and
              (tag(osm_way, "operator") == "Tifon" or tag(osm_way, "name") == "Tifon") and
              st_distance_in_meters(osm_way.linestring, poi_node.geometry) < 100,
          select: {poi_node.id, osm_way.id, st_distance_in_meters(osm_way.linestring, poi_node.geometry)},
          order_by: [asc: 1, asc: 3]
        )
      )
      # If multiple ways are matched keep the closest one only
      |> Enum.dedup_by(fn {poi_id, _, _} -> poi_id end)
      |> Enum.map(fn {node_id, osm_way_id, distance} ->
        %{
          source_id: source.id,
          node_id: node_id,
          osm_way_id: osm_way_id,
          distance: distance
        }
      end)

    Enum.concat(node_mappings, way_mappings)
  end

  defp fetch_list() do
    url = "https://pretrazivacpostaja.tifon.hr/hr/portlet/routing/along_latlng.json"

    with {:ok, response} <- post(url, %{country: "Croatia", lat: 45.813, lng: 15.9779}) do
      {:ok, response.body}
    end
  end

  defp fetch_details(record) do
    url = "https://pretrazivacpostaja.tifon.hr/hr/portlet/routing/station_info.json"

    with {:ok, response} <- post(url, %{id: record["id"]}) do
      {:ok, response.body}
    end
  end

  defp parse_node(source, record, details) do
    {street, house_number} = parse_address(record["address"])

    {lat, ""} = Float.parse(record["lat"])
    {lng, ""} = Float.parse(record["lng"])

    name =
      record["name"]
      |> String.split()
      |> Enum.map(&String.capitalize/1)
      |> Enum.join(" ")

    %{
      source_id: source.id,
      ref: record["id"],
      name: name,
      geometry: %Geo.Point{coordinates: {lng, lat}, srid: 4326},
      tags:
        MapUtils.remove_blank(%{
          "addr:city": record["city"],
          "addr:housenumber": house_number,
          "addr:postcode": record["postcode"],
          "addr:street": street,
          amenity: "fuel",
          mobile: details["fs"]["fs_mobile_num"],
          name: "Tifon",
          official_name: record["name"],
          opening_hours: parse_opening_hours(details),
          operator: "Tifon",
          phone: details["fs"]["fs_phone_num"],
          website: "https://www.tifon.hr/"
        })
    }
  end

  defp parse_opening_hours(details) do
    # TODO: check how to handle this
    if details["fs"]["opn_hrs_wtr_wd"] != details["fs"]["opn_hrs_smr_wd"] or
         details["fs"]["opn_hrs_wtr_sat"] != details["fs"]["opn_hrs_smr_sat"] or
         details["fs"]["opn_hrs_wtr_sun"] != details["fs"]["opn_hrs_smr_sun"] do
      Logger.warning("Summer working hours different from winter.")
    end

    weekday = details["fs"]["opn_hrs_wtr_wd"]
    saturday = details["fs"]["opn_hrs_wtr_sat"]
    sunday = details["fs"]["opn_hrs_wtr_sun"]

    cond do
      weekday != saturday and saturday != sunday ->
        "Mo-Fr #{weekday}; Sa #{saturday}; Su #{sunday}"

      weekday == saturday and saturday != sunday ->
        "Mo-Sa #{weekday}; Su #{sunday}"

      weekday != saturday and saturday == sunday ->
        "Mo-Fr #{weekday}; Sa-Su #{sunday}"

      weekday == saturday and saturday == sunday ->
        "Mo-Su #{weekday}"
    end
  end

  defp parse_address(address) do
    parts =
      address
      |> String.replace("; A1 (Dir. Split)", "")
      |> String.replace("; A6 (Dir. Rijeka - Zagreb)", "")
      |> String.replace("; A1 (Dir. Zagreb)", "")
      |> String.replace(" A1 (Dir. Split)", "")
      |> String.replace(" A1 (Dir. Zagreb)", "")
      |> String.replace("471 c", "471c")
      |> String.replace("Ul. ", "Ulica ")
      |> String.replace(" ul.", " ulica")
      |> String.split()
      |> Enum.reverse()

    [number | street_parts] = parts
    street = street_parts |> Enum.reverse() |> Enum.join(" ")
    {street, number}
  end
end
