defmodule Triglav.Poi.Sources.Crodux do
  use Tesla, only: [:get]

  plug Tesla.Middleware.Retry,
    delay: :timer.seconds(1),
    max_retries: 5,
    should_retry: fn
      {:ok, %{status: status}} when status != 200 -> true
      {:ok, _} -> false
      {:error, _} -> true
    end

  plug Tesla.Middleware.JSON
  plug Tesla.Middleware.Timeout, timeout: :timer.seconds(10)
  plug Tesla.Middleware.Logger, debug: false
  plug Triglav.Tesla.Middleware.ErrrorOnStatus

  alias Triglav.Poi
  alias Triglav.Poi.Source
  alias Triglav.Poi.Sources
  alias Triglav.Repo
  alias Triglav.Schemas.Osmosis
  alias Triglav.Helpers.MapUtils

  import Ecto.Query
  import Geo.PostGIS
  import Triglav.Query

  require Logger

  @behaviour Source

  @impl Source
  def name(), do: "Crodux"

  @impl Source
  def slug(), do: "crodux"

  @impl Source
  def fetch() do
    url = "https://crodux-derivati.hr/wp-json/acf/v3/benzinski_servis/?per_page=500"

    with {:ok, response} <- get(url) do
      {:ok, parse(response.body)}
    end
  end

  def parse(body) do
    source = Sources.get_or_create!(__MODULE__)

    body
    |> Enum.map(&parse_node(source, &1))
    |> Enum.sort_by(& &1.name)
  end

  @impl Source
  def find_mappings() do
    source = Sources.get_or_create!(__MODULE__)

    node_mappings =
      Repo.all(
        from(poi_node in Poi.Node,
          where: poi_node.source_id == ^source.id,
          join: osm_node in Osmosis.Node,
          on:
            tag(osm_node, "amenity") == "fuel" and
              (tag(osm_node, "operator") == "Crodux" or tag(osm_node, "name") == "Crodux") and
              st_distance_in_meters(osm_node.geom, poi_node.geometry) < 100,
          select: {poi_node.id, osm_node.id, st_distance_in_meters(osm_node.geom, poi_node.geometry)},
          order_by: [asc: 1, asc: 3]
        )
      )
      # If multiple nodes are matched keep the closest one only
      |> Enum.dedup_by(fn {node_id, _, _} -> node_id end)
      |> Enum.map(fn {node_id, osm_node_id, distance} ->
        %{
          source_id: source.id,
          node_id: node_id,
          osm_node_id: osm_node_id,
          distance: distance
        }
      end)

    way_mappings =
      Repo.all(
        from(poi_node in Poi.Node,
          where: poi_node.source_id == ^source.id,
          join: osm_way in Osmosis.Way,
          on:
            tag(osm_way, "amenity") == "fuel" and
              (tag(osm_way, "operator") == "Crodux" or tag(osm_way, "name") == "Crodux") and
              st_distance_in_meters(osm_way.linestring, poi_node.geometry) < 100,
          select: {poi_node.id, osm_way.id, st_distance_in_meters(osm_way.linestring, poi_node.geometry)},
          order_by: [asc: 1, asc: 3]
        )
      )
      # If multiple nodes are matched keep the closest one only
      |> Enum.dedup_by(fn {node_id, _, _} -> node_id end)
      |> Enum.map(fn {node_id, osm_way_id, distance} ->
        %{
          source_id: source.id,
          node_id: node_id,
          osm_way_id: osm_way_id,
          distance: distance
        }
      end)

    Enum.concat(node_mappings, way_mappings)
  end

  def parse_node(source, record) do
    address = parse_address(record["acf"]["adresa"])
    %{"lat" => lat, "lng" => lng} = record["acf"]["koordinate"]
    {lat, ""} = Float.parse(lat)
    {lng, ""} = Float.parse(lng)

    %{
      source_id: source.id,
      ref: to_string(record["id"]),
      name: record["acf"]["title"],
      geometry: %Geo.Point{coordinates: {lng, lat}, srid: 4326},
      tags:
        %{
          # TODO: opening_hours
          amenity: "fuel",
          name: "Crodux",
          official_name: record["acf"]["title"],
          operator: "Crodux",
          phone: record["acf"]["telefon"],
          website: "https://crodux-derivati.hr/"
        }
        |> Map.merge(address)
        |> MapUtils.remove_blank()
    }
  end

  defp parse_address(address) do
    [street_housenumber, postcode_city] =
      address
      # Replace notes in brackets
      |> String.replace(~r"\(.+\)"U, "")
      # Replace nonbreaking space
      |> String.replace(<<160::utf8>>, " ")
      |> String.split("<br />")
      |> Enum.map(&String.trim/1)
      |> Enum.reject(&(&1 == ""))

    [postcode, city] =
      postcode_city
      |> String.replace("Požega Osječka 34 000", "34000 Požega")
      |> String.replace("Pula 52100", "52100 Pula")
      |> String.replace("Grad Novigrad - Cittanova", "Novigrad")
      |> String.replace("Sv.Križ", "Sveti Križ Začretje")
      |> String.replace("Sv. Križ", "Sveti Križ Začretje")
      # Remove space between post code digits
      |> String.replace(~r"(\d{2}) (\d{3})", "\\1\\2")
      |> String.split(~r"\s+", parts: 2, trim: true)

    [street_housenumber | _subplace] =
      street_housenumber
      |> String.trim(",")
      |> String.split(",")

    [housenumber | street] =
      street_housenumber
      |> String.replace(~r" AC .+", "")
      |> String.replace(~r"Autocesta.+", "")
      |> String.replace(~r" 18 i", " 18i")
      |> String.replace(~r" 346 a", " 346a")
      |> String.replace(~r"Rupa (Istok|Zapad)", "")
      |> String.replace(~r"br.", "")
      |> String.split(~r"\s+")
      |> Enum.reverse()

    street = Enum.reverse(street) |> Enum.join(" ")

    %{
      "addr:housenumber": housenumber,
      "addr:street": street,
      "addr:city": city,
      "addr:postcode": postcode
    }
  end
end
