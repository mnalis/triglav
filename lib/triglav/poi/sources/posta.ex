defmodule Triglav.Poi.Sources.Posta do
  @moduledoc """
  Loads and parses post office data from posta.hr.

  NB: There are some XML exports, but they don't include geo coordinates, so we
  read the website instead. See:
  https://www.posta.hr/preuzimanje-podataka-o-postanskim-uredima-6543/6543

  TODO: Add city (and place?) to tags.
  """
  use Tesla, only: [:get]

  alias Triglav.Helpers.{MapUtils, StringUtils}
  alias Triglav.Poi
  alias Triglav.Poi.Source
  alias Triglav.Schemas.Osmosis
  alias Triglav.Repo

  import Ecto.Query
  import Geo.PostGIS
  import Triglav.Query

  require Logger

  plug Tesla.Middleware.Retry,
    delay: :timer.seconds(1),
    max_retries: 5,
    should_retry: fn
      {:ok, %{status: status}} when status != 200 -> true
      {:ok, _} -> false
      {:error, _} -> true
    end

  plug Triglav.Tesla.Middleware.ErrrorOnStatus
  plug Triglav.Tesla.Middleware.Logger

  @behaviour Source

  @impl Source
  def name(), do: "Hrvatska Pošta"

  @impl Source
  def slug(), do: "posta"

  @impl Source
  def fetch() do
    response = get!("https://www.posta.hr/interaktivna-karta-postanskih-ureda")
    {:ok, parse_map(response.body)}
  end

  @impl Source
  def find_mappings() do
    source = Triglav.Poi.Sources.posta()
    Enum.concat(find_nodes(source), find_ways(source))
  end

  defp find_nodes(source) do
    from(poi_node in Poi.Node,
      where: poi_node.source_id == ^source.id,
      join: osm_node in Osmosis.Node,
      on:
        tag(osm_node, "amenity") == "post_office" and
          tag(osm_node, "addr:postcode") == poi_node.ref,
      select: %{
        source_id: poi_node.source_id,
        node_id: poi_node.id,
        osm_node_id: osm_node.id,
        distance: st_distance_in_meters(osm_node.geom, poi_node.geometry)
      },
      order_by: [asc: poi_node.ref]
    )
    |> Repo.all()
  end

  defp find_ways(source) do
    from(poi_node in Poi.Node,
      where: poi_node.source_id == ^source.id,
      join: osm_way in Osmosis.Way,
      on:
        tag(osm_way, "amenity") == "post_office" and
          tag(osm_way, "addr:postcode") == poi_node.ref,
      select: %{
        source_id: poi_node.source_id,
        node_id: poi_node.id,
        osm_way_id: osm_way.id,
        distance: st_distance_in_meters(osm_way.linestring, poi_node.geometry)
      },
      order_by: [asc: poi_node.ref]
    )
    |> Repo.all()
  end

  defp format_opening_hours(time_spans) do
    time_spans
    |> Enum.with_index()
    |> Enum.reject(fn {span, _} -> is_nil(span) end)
    |> Enum.reduce([], fn {span, day}, acc ->
      case acc do
        [] ->
          [{span, [day]}]

        [{last_span, days} | rest] = acc ->
          if last_span == span, do: [{span, [day | days]} | rest], else: [{span, [day]} | acc]
      end
    end)
    |> Enum.reverse()
    |> Enum.map(fn {{open, close}, days} ->
      day_span = format_day_span(List.last(days), List.first(days))
      "#{day_span} #{open}-#{close}"
    end)
    |> Enum.join("; ")
    |> String.replace("Mo-Su 7:00-24:00", "24/7")
  end

  defp format_day_span(same, same), do: format_day(same)
  defp format_day_span(first, last), do: "#{format_day(first)}-#{format_day(last)}"

  defp format_day(0), do: "Mo"
  defp format_day(1), do: "Tu"
  defp format_day(2), do: "We"
  defp format_day(3), do: "Th"
  defp format_day(4), do: "Fr"
  defp format_day(5), do: "Sa"
  defp format_day(6), do: "Su"

  def parse_map(html) do
    Logger.info("Parsing post offices")
    types = parse_types(html)
    coordinates = parse_coordinates(html)
    nodes = parse_content(html, types, coordinates)
    Logger.info("Found #{length(nodes)} post offices")
    nodes
  end

  defp parse_types(html) do
    Regex.scan(~r"vrsta\[(\d+)\] = '(\w+)';", html)
    |> Enum.map(fn [_, id, type] -> {to_integer(id), String.to_atom(type)} end)
    |> Map.new()
  end

  defp parse_coordinates(html) do
    [_, match] = Regex.run(~r/var neighborhoods = \[([^\n]+)\];/, html)

    for [_line, latlng] <- Regex.scan(~r"new google.maps.LatLng\(([^)]+)\)", match) do
      latlng
      |> String.trim()
      |> String.split(",")
      |> Enum.map(&to_float/1)
      |> List.to_tuple()
    end
    |> Enum.with_index()
    |> Map.new(fn {coordinates, index} -> {index, coordinates} end)
  end

  defp parse_content(html, types, coordinates) do
    source = Triglav.Poi.Sources.posta()

    for [_, index, content] <- Regex.scan(~r"content\[(\d+)\] = '(.+)';"U, html) do
      doc = Floki.parse_fragment!(content)
      index = to_integer(index)
      amenity = Map.get(types, index) |> amenity()
      {street, housenumber} = parse_address(doc)

      [post_code_div] = Floki.find(doc, ".cl-postanski")
      [post_code, name] = Floki.text(post_code_div) |> String.split(" ", parts: 2)
      name = StringUtils.title_case(name) |> String.trim()

      # Set ref for post office
      ref = if(amenity == "post_office", do: post_code)

      hours =
        with [hours_div] <- Floki.find(doc, ".rv"),
             children <- Floki.children(hours_div),
             {_, _, ["Radno vrijeme:"]} <- List.first(children) do
          children
          |> Enum.filter(&is_binary/1)
          |> Enum.map(&parse_hours/1)
          |> format_opening_hours()
        else
          _ -> nil
        end

      {lat, lng} = Map.get(coordinates, index)

      %{
        source_id: source.id,
        name: "#{post_code} #{name}",
        ref: ref,
        geometry: %Geo.Point{coordinates: {lng, lat}, srid: 4326},
        tags:
          MapUtils.remove_blank(%{
            "addr:postcode": post_code,
            "addr:street": street,
            "addr:housenumber": housenumber,
            official_name: name,
            name: "#{post_code} #{name}",
            amenity: amenity,
            opening_hours: hours
          })
      }
    end
  end

  defp parse_address(doc) do
    address =
      doc
      |> Floki.find(".cl-adresa")
      |> Floki.text()
      # Remove notes in parenthesis
      |> String.replace(~r"\(.+\)", "")
      |> String.trim()

    [street_no | _] =
      address
      |> String.split(",")
      |> Enum.map(&String.trim/1)
      # Remove TC references (Trgovački centar)
      |> Enum.reject(&String.starts_with?(&1, "TC "))

    # Join street number, e.g. "1 A" -> "1A" and "7/b" -> "7b"
    street_no = String.replace(street_no, ~r" (\d+)[\s/]+([a-z])$"i, " \\1\\2")

    case Regex.run(~r"^(.+) (\d+[a-z]*|bb)$"i, street_no) do
      [_, street, housenumber] -> {street, String.downcase(housenumber)}
      nil -> {street_no, nil}
    end
  end

  defp amenity(:pu), do: "post_office"
  defp amenity(:kov), do: "post_box"
  defp amenity(:pak), do: "parcel_locker"

  defp parse_hours(hours) do
    hours
    |> String.trim()
    |> String.trim("-")
    |> String.split("-")
    |> case do
      [open, close] -> {open, close}
      _ -> nil
    end
  end

  defp to_integer(str) do
    {int, ""} = Integer.parse(str)
    int
  end

  defp to_float(string) do
    {float, ""} = string |> String.trim() |> Float.parse()
    float
  end
end
