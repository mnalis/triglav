defmodule Triglav.Poi.Mapping do
  alias Triglav.Poi.{Node, Source}
  alias Triglav.Schemas.Osmosis
  use Ecto.Schema

  @type t() :: %__MODULE__{}

  schema "poi_mappings" do
    belongs_to :source, Source
    belongs_to :node, Node
    belongs_to :osm_node, Osmosis.Node
    belongs_to :osm_way, Osmosis.Way
    field :distance, :float
  end
end
