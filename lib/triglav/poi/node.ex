defmodule Triglav.Poi.Node do
  use Ecto.Schema

  @type t() :: %__MODULE__{}
  @type id() :: pos_integer()

  schema "poi_nodes" do
    belongs_to :source, Triglav.Poi.Source
    field :geometry, Geo.PostGIS.Geometry
    field :name, :string
    field :ref, :string
    field :tags, {:map, :string}
    has_one :mapping, Triglav.Poi.Mapping
  end
end
