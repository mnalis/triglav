defmodule Triglav.Poi.Source do
  use Ecto.Schema

  alias Triglav.Poi.{Node, Mapping}

  @type t() :: %__MODULE__{}
  @type id() :: pos_integer()

  schema "poi_sources" do
    field :name, :string
    field :slug, :string
    field :node_count, :integer
    field :mapping_count, :integer
    timestamps()

    has_many :nodes, Node
    has_many :mappings, Mapping
  end

  @type poi_node :: %{
          source_id: id(),
          geometry: Geo.geometry(),
          name: String.t(),
          ref: String.t(),
          tags: %{String.t() => String.t()}
        }

  @type mapping :: %{
          source_id: id(),
          node_id: Node.id(),
          osm_node_id: integer() | nil,
          osm_way_id: integer() | nil,
          distance: float()
        }

  @callback slug() :: String.t()
  @callback name() :: String.t()
  @callback fetch() :: {:ok, [poi_node()]} | {:error, any()}
  @callback find_mappings() :: [mapping()]
end
