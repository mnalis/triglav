defmodule Triglav.Helpers.MapUtils do
  @spec keys_to_atoms(map()) :: map()
  def keys_to_atoms(%{} = map) do
    for {k, v} <- map, into: %{} do
      k =
        cond do
          is_binary(k) -> String.to_existing_atom(k)
          is_atom(k) -> k
        end

      {k, keys_to_atoms(v)}
    end
  end

  def keys_to_atoms(other), do: other

  @doc "Drops keys whose values are nil or an empty string."
  @spec remove_blank(map()) :: map()
  def remove_blank(map) do
    for {k, v} <- map, not is_nil(v) and String.trim(v) != "", into: %{}, do: {k, v}
  end
end
