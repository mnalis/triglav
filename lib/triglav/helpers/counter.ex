defmodule Triglav.Helpers.Counter do
  @moduledoc """
  Utilize ETS to count things.

      iex()> counter = Triglav.Helpers.Counter.new()
      #Reference<0.694748076.457310213.214707>
      iex()> Triglav.Helpers.Counter.increment(counter, :foo)
      1
      iex()> Triglav.Helpers.Counter.increment(counter, :foo)
      2
      iex()> Triglav.Helpers.Counter.get(counter, :foo)
      3
      iex(10)> Triglav.Helpers.Counter.increment(counter, %{complex: {:keys, [:are, :ok]}})
      1
      iex()> Triglav.Helpers.Counter.all(counter)
      %{:foo => 2, %{complex: {:keys, [:are, :ok]}} => 1}
      iex()> Triglav.Helpers.Counter.delete(counter)
      true
  """
  @type counter :: :ets.tid()
  @type key :: term()

  @spec new() :: counter()
  def new(), do: :ets.new(:counter, [])

  @spec increment(counter(), key()) :: pos_integer()
  def increment(table, key) do
    :ets.update_counter(table, key, {2, 1}, {key, 0})
  end

  @spec get(counter(), key()) :: non_neg_integer()
  def get(table, key) do
    case :ets.lookup(table, key) do
      [{_, count}] -> count
      [] -> 0
    end
  end

  @spec all(counter()) :: %{key() => pos_integer()}
  def all(table), do: Map.new(:ets.tab2list(table))

  @spec delete(counter()) :: true
  def delete(table), do: :ets.delete(table)
end
