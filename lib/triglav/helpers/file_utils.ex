defmodule Triglav.Helpers.FileUtils do
  def md5sum!(path) do
    {res, 0} = System.cmd("md5sum", [path])
    String.slice(res, 0..31)
  end

  def wc_l!(path) do
    {res, 0} = System.cmd("wc", ["-l", path])
    {count, _} = Integer.parse(res)
    count
  end
end
