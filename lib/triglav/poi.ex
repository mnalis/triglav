defmodule Triglav.Poi do
  alias Triglav.Poi.{Node, Mapping, Source}
  alias Triglav.Poi.Sources
  alias Triglav.Repo

  import Ecto.Changeset
  import Ecto.Query

  require Logger

  @sources [
    Sources.Crodux,
    Sources.Posta,
    Sources.Tifon
  ]

  def update_all() do
    tasks = for source <- @sources, do: Task.async(fn -> update(source) end)
    Task.await_many(tasks, :timer.seconds(60))
  end

  def regenerate_all() do
    for source <- @sources do
      regenerate(source)
    end
  end

  def update(sources) when is_list(sources) do
    for source <- sources do
      update(source)
    end
  end

  def update(source_module) when is_atom(source_module) do
    Repo.transact(
      fn ->
        source = Sources.get_or_create!(source_module)

        with {:ok, nodes} <- source_module.fetch() do
          delete_mappings(source)
          delete_nodes(source)
          node_count = persist_nodes(source, nodes)
          mappings = source_module.find_mappings()
          mapping_count = persist_mappings(source, mappings)

          source
          |> change(%{node_count: node_count, mapping_count: mapping_count})
          |> Repo.update()
        end
      end,
      timeout: :timer.seconds(60)
    )
  end

  def regenerate(source_module) do
    Repo.transact(
      fn ->
        source = Sources.get_or_create!(source_module)
        mappings = source_module.find_mappings()
        delete_mappings(source)
        mapping_count = persist_mappings(source, mappings)

        source
        |> change(%{mapping_count: mapping_count})
        |> Repo.update()
      end,
      timeout: :timer.seconds(60)
    )
  end

  @spec nodes(Source.t()) :: [Node.t()]
  def nodes(source) do
    Repo.all(from(Node, where: [source_id: ^source.id], order_by: [asc: :name]))
  end

  defp delete_nodes(source) do
    Repo.delete_all(where(Node, source_id: ^source.id))
  end

  defp delete_mappings(source) do
    Repo.delete_all(where(Mapping, source_id: ^source.id))
  end

  defp persist_nodes(source, nodes) do
    {count, nil} = Repo.insert_all(Node, nodes)
    Logger.info("#{source.name}: Inserted #{count} nodes")
    count
  end

  defp persist_mappings(source, mappings) do
    {count, nil} = Repo.insert_all(Mapping, mappings)
    Logger.info("#{source.name}: Inserted #{count} mappings")
    count
  end
end
