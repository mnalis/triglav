defmodule Triglav.Zet.Realtime do
  @moduledoc """
  Fetch and decode ZET realtime data.

  Data is provided in GTFS Realtime format which is encoded using Protobuf.
  https://github.com/google/transit/tree/master/gtfs-realtime

  Latest data can be fetched here (currently for test only):
  https://www.zet.hr/odredbe/datoteke-u-gtfs-formatu/669
  """

  alias Gtfs.TransitRealtime.FeedMessage

  @type feed_message :: %FeedMessage{}

  @spec fetch() :: {:ok, feed_message()} | {:error, term()}
  def fetch() do
    with {:ok, binary} <- get() do
      decode(binary)
    end
  end

  @spec fetch!() :: feed_message()
  def fetch!() do
    {:ok, data} = fetch()
    data
  end

  defp get() do
    with {:ok, response} <- Tesla.get("https://zet.hr/gtfs-rt-protobuf") do
      if response.status == 200, do: {:ok, response.body}, else: {:error, response}
    end
  end

  defp decode(encoded) do
    {:ok, decode!(encoded)}
  rescue
    error -> {:error, error}
  end

  defp decode!(encoded), do: Gtfs.TransitRealtime.FeedMessage.decode(encoded)
end
