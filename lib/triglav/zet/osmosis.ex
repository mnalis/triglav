defmodule Triglav.Zet.Osmosis do
  @moduledoc """
  Loads public relation data from the osmosis schema.
  """
  alias Triglav.Repo
  alias Triglav.Schemas.Osmosis.Node
  alias Triglav.Schemas.Osmosis.Relation
  alias Triglav.Schemas.Osmosis.RelationMember
  alias Triglav.Schemas.Osmosis.ReplicationChanges
  alias Triglav.Schemas.Osmosis.Way

  import Ecto.Query

  def list_replication_changes(opts \\ []) do
    limit = Keyword.get(opts, :limit, 100)

    from(rc in ReplicationChanges, order_by: [desc: :tstamp], limit: ^limit)
    |> Repo.all()
  end

  @doc """
  For a given relation id or list of ids returns a list of relation members with
  a platform role.
  """
  @spec list_platform_members(Relation.id() | [Relation.id()]) :: [RelationMember.t()]

  def list_platform_members(relation_id) when is_integer(relation_id) or is_binary(relation_id),
    do: list_platform_members([relation_id])

  def list_platform_members(relation_ids) when is_list(relation_ids) do
    from(m in RelationMember,
      left_join: n in Node,
      on: m.member_type == "N" and m.member_id == n.id,
      left_join: w in Way,
      on: m.member_type == "W" and m.member_id == w.id,
      where: m.relation_id in ^relation_ids and ilike(m.member_role, "platform%"),
      select: {m, n, w},
      order_by: m.sequence_id
    )
    |> Repo.all()
    |> Enum.map(fn {member, node, way} -> %{member | member: node || way} end)
  end
end
