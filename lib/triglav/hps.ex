defmodule Triglav.Hps do
  alias Triglav.Hps.Api
  alias Triglav.Hps.Schemas.Trace
  alias Triglav.Hps.Schemas.Track
  alias Triglav.Hps.Schemas.Waypoint
  alias Triglav.Repo

  import Ecto.Query
  require Logger

  def get_track(id) do
    Repo.get(Track, id)
  end

  def list_tracks(opts \\ []) do
    region = Keyword.get(opts, :region)

    from(t in Track, order_by: :ref)
    |> maybe_filter_by_region(region)
    |> Repo.all()
  end

  # ----------------------------------------------------------------------------
  # Import
  # ----------------------------------------------------------------------------

  @spec import_tracks() :: {:ok, [Track.t()]} | {:error, any()}
  def import_tracks(opts \\ []) do
    delete? = Keyword.get(opts, :delete?, false)

    with {:ok, tracks} <- Api.fetch_tracks() do
      Repo.transact(fn ->
        if delete? do
          Repo.delete_all(Waypoint)
          Repo.delete_all(Trace)
          Repo.delete_all(Track)
        end

        Repo.insert_all(Track, tracks, on_conflict: :raise)
        :ok
      end)

      {:ok, tracks}
    end
  end

  def import_all_traces(opts \\ []) do
    Track
    |> maybe_filter_by_id(opts[:id])
    |> maybe_filter_by_region(opts[:region])
    |> Repo.all()
    |> Enum.each(&import_trace(&1, opts))
  end

  # TODO: more validation needed for traces
  # - what to do with gpx files with multiple segments?
  #     - use multilinestring?
  #     - create multiple traces?

  @spec import_trace(Track.t(), Keyword.t()) ::
          {:ok, Trace.t()} | {:error, :already_loaded | Ecto.Changeset.t()}
  def import_trace(track, opts \\ []) do
    force = Keyword.get(opts, :force, false)
    exists = from(tg in Trace, where: tg.track_id == ^track.id) |> Repo.exists?()

    if !exists or force do
      Logger.info("#{track.ref}: Loading from #{track.gpx2_url}")

      case do_import_trace(track) do
        {:ok, trace} ->
          {:ok, trace}

        {:error, error} ->
          Logger.error("Failed loading gpx: #{inspect(error)}")
          {:error, error}
      end
    else
      Logger.warning("#{track.ref}: Skipping, already loaded.")
      {:error, :already_loaded}
    end
  end

  defp do_import_trace(track) do
    with {:ok, trace, waypoints} <- Api.fetch_trace(track) do
      Repo.transact(fn ->
        with {:ok, trace} <-
               Repo.insert(struct(Trace, trace),
                 on_conflict: {:replace_all_except, [:id]},
                 conflict_target: :track_id
               ) do
          waypoints = Enum.map(waypoints, &Map.put(&1, :trace_id, trace.id))
          Repo.insert_all(Waypoint, waypoints)
          {:ok, trace}
        end
      end)
    end
  end

  # ----------------------------------------------------------------------------
  # Common
  # ----------------------------------------------------------------------------

  defp maybe_filter_by_id(query, nil), do: query
  defp maybe_filter_by_id(query, id), do: where(query, [t], t.id == ^id)

  defp maybe_filter_by_region(query, nil), do: query
  defp maybe_filter_by_region(query, region), do: where(query, [t], t.region == ^region)

  def track_count_by_region() do
    from(t in Track,
      group_by: :region,
      order_by: [asc: :region],
      select: {t.region, count()}
    )
    |> Repo.all()
  end
end
