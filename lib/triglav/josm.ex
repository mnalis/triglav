defmodule Triglav.Josm do
  @moduledoc """
  Generate JOSM remote control links.

  Remote docs:
  https://josm.openstreetmap.de/wiki/Help/RemoteControlCommands
  """
  alias Triglav.Schemas.Osmosis.Node
  alias Triglav.Schemas.Osmosis.Relation
  alias Triglav.Schemas.Osmosis.Way

  @type object :: Relation.t() | Way.t() | Node.t()
  @type options :: [new_layer: boolean()]

  @base "http://127.0.0.1:8111"

  @spec load_object(object, options) :: String.t()
  def load_object(object, opts \\ []) do
    load_objects([object], opts)
  end

  @spec load_objects([object], options) :: String.t()
  def load_objects(objects, opts \\ []) do
    new_layer = Keyword.get(opts, :new_layer, false)
    relation_members = Keyword.get(opts, :relation_members, true)

    url("/load_object", %{
      objects: object_ids(objects),
      new_layer: to_string(new_layer),
      relation_members: to_string(relation_members)
    })
  end

  def zoom(lat, lon) do
    url("/zoom", %{
      top: lat,
      bottom: lat,
      left: lon,
      right: lon
    })
  end

  def add_tags(object, tags) do
    str_tags = Enum.map_join(tags, "|", fn {k, v} -> "#{k}=#{v}" end)

    params =
      bounds(object)
      |> Map.put(:select, object_id(object))
      |> Map.put(:addtags, str_tags)

    url("/load_and_zoom", params)
  end

  def add_node(lat, lon, tags) do
    str_tags = Enum.map_join(tags, "|", fn {k, v} -> "#{k}=#{v}" end)
    url("/add_node", %{lat: lat, lon: lon, addtags: str_tags})
  end

  def add_way(points, tags) do
    way = Enum.map(points, fn {lat, lon} -> "#{lat},#{lon}" end) |> Enum.join(";")
    str_tags = Enum.map_join(tags, "|", fn {k, v} -> "#{k}=#{v}" end)
    url("/add_way", %{way: way, addtags: str_tags})
  end

  defp url(path, params) do
    "#{@base}#{path}?#{URI.encode_query(params)}"
  end

  defp object_ids(objects), do: Enum.map_join(objects, ",", &object_id/1)

  defp object_id(%Relation{} = relation), do: "r#{relation.id}"
  defp object_id(%Way{} = way), do: "w#{way.id}"
  defp object_id(%Node{} = node), do: "n#{node.id}"

  defp bounds(%Node{} = node) do
    {lon, lat} = node.geom.coordinates

    %{
      top: lat,
      bottom: lat,
      left: lon,
      right: lon
    }
  end

  defp bounds(%Way{} = way) do
    {lon_min, lon_max} =
      way.linestring.coordinates
      |> Enum.map(&elem(&1, 0))
      |> Enum.min_max()

    {lat_min, lat_max} =
      way.linestring.coordinates
      |> Enum.map(&elem(&1, 1))
      |> Enum.min_max()

    %{
      top: lat_max,
      bottom: lat_min,
      left: lon_min,
      right: lon_max
    }
  end
end
