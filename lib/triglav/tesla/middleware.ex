defmodule Triglav.Tesla.Middleware.Logger do
  @behaviour Tesla.Middleware

  require Logger

  @impl Tesla.Middleware
  def call(env, next, _opts) do
    log_request(env)
    {time, response} = :timer.tc(Tesla, :run, [env, next])
    log_response(env, response, time)
    response
  end

  defp log_request(env) do
    Logger.debug("--> #{sig(env)}")
  end

  defp log_response(env, response, time) do
    time = faint("#{round(time / 1000)}ms")
    sig = sig(env)

    case response do
      {:ok, %Tesla.Env{} = response} ->
        colour = if response.status >= 400, do: &red/1, else: &green/1
        status = colour.("HTTP #{response.status}")
        Logger.debug("<-- #{sig} #{status} #{time}")

      {:error, error} ->
        error = red("ERROR #{inspect(error)}")
        Logger.debug("<-- #{sig} #{error} #{time}")
    end
  end

  defp red(str), do: IO.ANSI.red() <> str <> IO.ANSI.reset()
  defp green(str), do: IO.ANSI.green() <> str <> IO.ANSI.reset()
  defp faint(str), do: IO.ANSI.faint() <> str <> IO.ANSI.reset()

  defp sig(env) do
    method = String.upcase(to_string(env.method))
    "#{method} #{env.url}"
  end
end

defmodule Triglav.Tesla.Middleware.ErrrorOnStatus do
  @behaviour Tesla.Middleware

  require Logger

  @impl Tesla.Middleware
  def call(env, next, _opts) do
    with {:ok, result} <- Tesla.run(env, next) do
      if result.status < 400 do
        {:ok, result}
      else
        {:error, result}
      end
    end
  end
end
