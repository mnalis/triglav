defmodule Triglav.PublicTransport do
  alias Triglav.GeoJSON
  alias Triglav.Import.Geofabrik
  alias Triglav.Osm.Router
  alias Triglav.PublicTransport.Schemas.{Error, ErrorHistory, Feed, Platform, Operator, Route, RouteVariant}
  alias Triglav.PublicTransport.{Derived, Mappings, Operators, Validation}
  alias Triglav.Repo
  alias Triglav.Schemas.Osmosis.Relation

  import Ecto.Query
  import Triglav.Tooling
  require Logger

  defdelegate generate_mappings(feed_id), to: Mappings, as: :generate
  defdelegate import_autotrolej(opts \\ []), to: Triglav.Autotrolej, as: :import
  defdelegate import_gp_sibenik(opts \\ []), to: Triglav.PublicTransport.ImportGtfs
  defdelegate import_gpp_osijek(opts \\ []), to: Triglav.PublicTransport.ImportGtfs
  defdelegate import_hzpp(opts \\ []), to: Triglav.PublicTransport.ImportGtfs
  defdelegate import_jadrolinija(opts \\ []), to: Triglav.PublicTransport.ImportGtfs
  defdelegate import_pulapromet(opts \\ []), to: Triglav.PublicTransport.ImportGtfs
  defdelegate import_zet(opts \\ []), to: Triglav.PublicTransport.ImportGtfs

  @doc """
  For each operator, check if a new feed is available. If so, update the feed,
  generate new mappings and update errors.

  Options:
  - `:operators` - A list of operator slugs to load
  - `:force` - If set to `true` will re-load the feeds even if a newer feed is
               not found. Default is `false`.
  """
  def update_all(opts \\ []) do
    slugs = Keyword.get(opts, :operators)

    operators =
      Operators.all()
      |> Enum.filter(&(is_nil(slugs) || &1.slug in slugs))

    # TODO: make this concurrent
    Enum.each(operators, &update_operator(&1, opts))
  end

  defp update_operator(operator, opts) do
    try do
      update_feed(operator, opts)
    rescue
      e -> Triglav.Tooling.log_error("Failed loading #{operator.name}", e, __STACKTRACE__)
    end
  end

  @doc """
  For each active feed, regenerate the mappings and errors. This is typically
  called after updating OSM data.
  """
  def regenerate_derived_data() do
    Repo.all(where(Feed, active: true) |> preload(:operator))
    |> Enum.each(fn feed ->
      try do
        Derived.regenerate(feed.operator)
        generate_mappings(feed.id)
        generate_errors(feed.id)
      rescue
        e -> log_error("Failed loading #{feed.operator.name}", e, __STACKTRACE__)
      end
    end)
  end

  defp update_feed(operator, opts) do
    with {:ok, fun} <- import_fn(operator),
         {:ok, {feed, changed?}} <- fun.(opts) do
      if changed? do
        Derived.regenerate(operator)
        generate_mappings(feed.id)
        generate_errors(feed.id)
      else
        Logger.info("#{operator.ref} already at latest version ##{feed.ref}")
      end
    else
      error -> Logger.error("Failed updating #{operator.name()}: #{inspect(error)}")
    end
  end

  defp import_fn(operator) do
    case operator.slug do
      "autotrolej" -> {:ok, &import_autotrolej/1}
      "hzpp" -> {:ok, &import_hzpp/1}
      "zet" -> {:ok, &import_zet/1}
      "gp_sibenik" -> {:ok, &import_gp_sibenik/1}
      "gpp_osijek" -> {:ok, &import_gpp_osijek/1}
      "jadrolinija" -> {:ok, &import_jadrolinija/1}
      "pulapromet" -> {:ok, &import_pulapromet/1}
      _ -> {:error, :unknown_operator}
    end
  end

  @spec delete_feed(Feed.id()) :: :ok
  def delete_feed(id) do
    Repo.transact(fn ->
      Mappings.delete(id)
      Repo.delete_all(where("pt_route_variant_platforms", feed_id: ^id))
      Repo.delete_all(where(Error, feed_id: ^id))
      Repo.delete_all(where(Platform, feed_id: ^id))
      Repo.delete_all(where(RouteVariant, feed_id: ^id))
      Repo.delete_all(where(Route, feed_id: ^id))
      Repo.delete_all(where(Feed, id: ^id))

      :ok
    end)
  end

  @doc """
  List all platforms linked to a given feed.

  Options:
  - hide_unused - if set to true, returns only platforms which are used in at
    least one route variant, otherwise returns all platforms given by operator
    data. Default is `false`.
  """
  def list_platforms(feed_id, opts \\ []) do
    from(Platform, as: :platform)
    |> where(feed_id: ^feed_id)
    |> order_by(:id)
    |> maybe_hide_unused(opts)
    |> Repo.all()
  end

  defp maybe_hide_unused(query, opts) do
    if Keyword.get(opts, :hide_unused, false),
      do: where(query, [platform: platform], platform.use_count > 0),
      else: query
  end

  def count_platforms(feed_id) do
    Platform
    |> where(feed_id: ^feed_id)
    |> Repo.aggregate(:count)
  end

  @spec matched_counts(Feed.id()) :: %{
          Route.id() => %{
            matched_count: non_neg_integer(),
            unmatched_count: non_neg_integer(),
            total_count: non_neg_integer()
          }
        }
  def matched_counts(feed_id) do
    {:ok, result} =
      Repo.query(
        """
        SELECT route.id,
             count(*) FILTER (WHERE variant.id IS NOT NULL) AS matched_count,
             count(*) FILTER (WHERE variant.id IS NULL) AS unmatched_count,
             (SELECT count(*) FROM pt_route_variants WHERE feed_id = $1 AND route_id = route.id) AS total_count
        FROM pt_routes AS route
        LEFT JOIN pt_route_mapped_route_relations ON route_id = route.id
        LEFT JOIN pt_route_variants AS variant
          ON variant.mapped_relation_id = pt_route_mapped_route_relations.relation_id
          AND variant.feed_id = $1
        WHERE route.feed_id = $1
        GROUP BY 1
        ORDER BY 1
        """,
        [feed_id]
      )

    Map.new(result.rows, fn [route_id, matched_count, unmatched_count, total_count] ->
      {route_id,
       %{
         matched_count: matched_count,
         unmatched_count: unmatched_count,
         total_count: total_count
       }}
    end)
  end

  # TODO: Store counts in Feed struct
  @spec feed_counts(Operator.id()) :: [
          %{
            id: Feed.id(),
            ref: String.t(),
            imported_at: NaiveDateTime.t(),
            platform_count: non_neg_integer(),
            route_count: non_neg_integer(),
            route_variant_count: non_neg_integer()
          }
        ]
  def feed_counts(operator_id) do
    Repo.select_atoms!(
      """
      SELECT
        feed.id,
        feed.ref,
        feed.imported_at,
        (SELECT count(*) FROM pt_platforms WHERE feed_id = feed.id) AS platform_count,
        (SELECT count(*) FROM pt_routes WHERE feed_id = feed.id) AS route_count,
        (SELECT count(*) FROM pt_route_variants  WHERE feed_id = feed.id) AS route_variant_count
       FROM pt_feeds AS feed
       WHERE feed.operator_id = $1
       ORDER BY feed.imported_at DESC
      """,
      [operator_id]
    )
  end

  @spec route_relation_geometry(Relation.t()) :: Geo.LineString.t() | Geo.MultiLineString.t()
  def route_relation_geometry(relation) do
    ordered_ways =
      relation.members
      |> Enum.filter(&(&1.member_type == "W" and &1.member_role == ""))
      |> Enum.sort_by(& &1.sequence_id)
      |> Enum.map(& &1.member)

    case Router.route_ways(ordered_ways) do
      {:ok, geometry} -> geometry
      {:error, _} -> GeoJSON.ways_to_multilinestring(ordered_ways)
    end
  end

  def generate_errors(feed_id) do
    osm_state = Geofabrik.get_local_state()
    feed = Repo.get!(Feed, feed_id) |> Repo.preload(:operator)
    Logger.info("#{feed.operator.ref} #{feed.ref}: Validating feed...")

    old_errors = unresolved_errors(feed.operator_id)
    new_errors = Validation.validate(feed_id)

    old_errors_map = Map.new(old_errors, &{Error.signature(&1), &1})
    new_errors_map = Map.new(new_errors, &{Error.signature(&1), &1})

    old_sigs = MapSet.new(Map.keys(old_errors_map))
    new_sigs = MapSet.new(Map.keys(new_errors_map))

    to_create = MapSet.difference(new_sigs, old_sigs) |> Enum.map(&Map.get(new_errors_map, &1))
    to_resolve = MapSet.difference(old_sigs, new_sigs) |> Enum.map(&Map.get(old_errors_map, &1))
    to_resolve_ids = Enum.map(to_resolve, & &1.id)

    old_count = map_size(old_errors_map)
    new_count = length(new_errors)
    created_count = length(to_create)
    resolved_count = length(to_resolve)

    # Sanity check
    if length(new_errors) != map_size(new_errors_map) do
      Logger.error("Length mismatch, Error.signature produces non-unique signatures?")
    end

    # Persist errors
    Repo.transact(fn ->
      now = DateTime.utc_now() |> DateTime.truncate(:second)

      {_, nil} =
        Repo.update_all(
          from(e in Error, where: e.id in ^to_resolve_ids),
          set: [resolved_at: now]
        )

      {_, created_errors} =
        Repo.insert_all(
          Error,
          Enum.map(to_create, &to_storeable_map/1),
          returning: [:id]
        )

      Repo.insert(%ErrorHistory{
        operator_id: feed.operator_id,
        feed_ref: feed.ref,
        count: new_count,
        previous_count: old_count,
        created_count: created_count,
        resolved_count: resolved_count,
        osm_sequence_number: osm_state.sequence_number,
        created_error_ids: Enum.map(created_errors, & &1.id),
        resolved_error_ids: to_resolve_ids
      })
    end)

    Logger.info(
      "#{feed.operator.ref} #{feed.ref}: Validation done. " <>
        "Errors #{old_count} -> #{new_count}. " <>
        "Created: #{created_count}. Resolved: #{resolved_count}."
    )
  end

  # TODO: this is ugly, perhaps `Triglav.PublicTransport.Validation.Errors.*`
  # factories should generate maps instead of structs so they can be given to
  # insert_all without these shenanigans.
  defp to_storeable_map(struct) do
    association_fields = struct.__struct__.__schema__(:associations)
    waste_fields = association_fields ++ [:__meta__, :id, :created_at]
    struct |> Map.from_struct() |> Map.drop(waste_fields)
  end

  @spec unresolved_errors(Operator.id()) :: [Error.t()]
  defp unresolved_errors(operator_id) do
    from(e in Error,
      where: e.operator_id == ^operator_id,
      where: is_nil(e.resolved_at)
    )
    |> Repo.all()
  end
end
