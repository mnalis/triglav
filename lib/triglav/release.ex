defmodule Triglav.Release do
  @app :triglav

  def migrate do
    load_app()

    for repo <- repos() do
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
    end
  end

  def rollback(repo, version) do
    load_app()
    {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :down, to: version))
  end

  def osmosis_init() do
    load_app()
    Mix.Tasks.Triglav.OsmosisInit.run([])
  end

  def osmosis_load(pbf_path) do
    load_app()
    Mix.Tasks.Triglav.OsmosisLoad.run([pbf_path])
  end

  def osmosis_update() do
    load_app()
    Mix.Tasks.Triglav.OsmosisUpdate.run([])
  end

  def pt_update do
    load_app()
    Mix.Tasks.Triglav.PtUpdate.run([])
  end

  def pt_regenerate do
    load_app()
    Mix.Tasks.Triglav.PtRegenerate.run([])
  end

  def poi_update do
    start_repo_only()
    Triglav.Poi.update_all()
  end

  def poi_regenerate do
    start_repo_only()
    Triglav.Poi.regenerate_all()
  end

  defp start_repo_only() do
    Application.put_env(:triglav, :repo_only, true)
    {:ok, _} = Application.ensure_all_started(:triglav)
  end

  defp repos do
    Application.fetch_env!(@app, :ecto_repos)
  end

  defp load_app do
    Application.load(@app)
  end
end
