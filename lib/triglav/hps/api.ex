defmodule Triglav.Hps.Api do
  alias Triglav.Haversine
  alias Triglav.Hps.Schemas.Track
  alias Triglav.Http

  import SweetXml, only: [xpath: 2, sigil_x: 2]

  require Logger

  @spec fetch_tracks() :: {:ok, [map()]} | {:error, any()}
  def fetch_tracks() do
    with {:ok, body} <- Http.get("https://www.hps.hr/karta/csv/tracks.csv") do
      File.write!("tracks_wtf.csv", body)

      body
      |> String.trim()
      # Strip BOM
      |> String.replace_leading("\uFEFF", "")
      |> String.split("\n")
      |> Enum.map(&String.trim/1)
      # Some rows are duplicated
      |> Enum.map(&String.split(&1, ";"))
      |> Enum.map(&parse_track/1)
      # Remove duplicates
      |> Enum.uniq_by(& &1.ref)
      |> then(fn tracks -> {:ok, tracks} end)
    end
  end

  @spec fetch_trace(Track.t()) :: {:ok, map(), [map()]} | {:error, any()}
  def fetch_trace(track) do
    with {:ok, body} <- Http.get(track.gpx2_url),
         {:ok, doc} <- parse_xml(body),
         :ok <- validate(doc) do
      trace = to_trace(doc, track)
      waypoints = to_waypoints(doc)
      {:ok, trace, waypoints}
    end
  end

  defp parse_xml(body) do
    try do
      {:ok, SweetXml.parse(body)}
    catch
      _, _ -> {:error, :parse_error}
    end
  end

  defp validate(doc) do
    if length(xpath(doc, ~x"/gpx/trk"l)) > 1 do
      Logger.warning("Multiple <trk>s found, considering only the first")
    end

    trk = xpath(doc, ~x"/gpx/trk")

    if length(xpath(trk, ~x"./trkseg"l)) > 1 do
      Logger.warning("Multiple <trkseg>s found, considering only the first")
    end

    :ok
  end

  defp to_trace(doc, track) do
    trk = xpath(doc, ~x"/gpx/trk[trkseg]")
    name = xpath(trk, ~x"./name/text()"s)
    trkseg = xpath(trk, ~x"./trkseg")
    trkpts = xpath(trkseg, ~x"./trkpt"l)

    coordinates = Enum.map(trkpts, &extract_coordinates/1)
    distances = extract_distances(coordinates)
    times = Enum.map(trkpts, &extract_time/1)
    elevations = Enum.map(trkpts, &extract_elevation/1)
    linestring = %Geo.LineString{coordinates: coordinates, srid: 4326}

    %{
      track_id: track.id,
      name: name,
      linestring: linestring,
      times: times,
      elevations: elevations,
      distances: distances
    }
  end

  defp extract_time(trkpt) do
    trkpt
    |> xpath(~x"./time/text()"s)
    |> parse_datetime()
  end

  defp extract_elevation(trkpt) do
    trkpt
    |> xpath(~x"./ele/text()"s)
    |> parse_float()
  end

  defp to_waypoints(doc) do
    doc
    |> xpath(~x"/gpx/wpt"l)
    |> Enum.map(&to_waypoint(&1))
  end

  defp to_waypoint(wpt) do
    name = xpath(wpt, ~x"./name/text()"s)
    description = xpath(wpt, ~x"./desc/text()"s)

    %{
      name: name,
      description: description,
      elevation: extract_elevation(wpt),
      time: extract_time(wpt),
      point: %Geo.Point{
        coordinates: extract_coordinates(wpt),
        srid: 4326
      }
    }
  end

  defp extract_coordinates(node) do
    lat = xpath(node, ~x"./@lat"s) |> parse_float()
    lon = xpath(node, ~x"./@lon"s) |> parse_float()

    {lon, lat}
  end

  defp extract_distances(coordinates) do
    coordinates
    |> Enum.chunk_every(2, 1, :discard)
    |> Enum.map(fn [p1, p2] -> Haversine.distance(p1, p2) end)
    |> Enum.reduce([0.0], fn d, [last | _] = acc -> [last + d | acc] end)
    |> Enum.reverse()
  end

  defp parse_track([
         gpx1,
         _,
         name,
         url,
         region,
         route_no,
         gpx2,
         _,
         _,
         _,
         _,
         length_km,
         height_diff,
         walk_time
       ]) do
    %{
      ref: String.replace_trailing(gpx1, ".gpx", ""),
      gpx1_url: "https://www.hps.hr/karta/gpx/#{gpx1}",
      gpx2_url: "https://info.hps.hr/putovi/www/static/gpx/#{gpx2}",
      length: parse_float(length_km),
      name: name,
      region: region,
      route_no: route_no,
      url: url,
      height_diff: parse_int(height_diff),
      walk_time: parse_duration(walk_time)
    }
  end

  defp parse_float(""), do: nil

  defp parse_float(value) do
    {float, ""} = value |> String.replace(",", ".") |> Float.parse()
    float
  end

  defp parse_int(""), do: nil

  defp parse_int(value) do
    {int, ""} = value |> String.replace(",", ".") |> Integer.parse()
    int
  end

  defp parse_duration(""), do: nil

  defp parse_duration(value) do
    [m, s] = String.split(value, ~r"[:.]")
    {m, ""} = Integer.parse(m)
    {s, ""} = Integer.parse(s)
    60 * m + s
  end

  defp parse_datetime(""), do: nil

  defp parse_datetime(value) do
    {:ok, datetime, _} = DateTime.from_iso8601(value)
    DateTime.truncate(datetime, :second)
  end
end
