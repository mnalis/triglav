defmodule Triglav.Hps.Schemas.Waypoint do
  alias Triglav.Hps.Schemas.Trace
  use Ecto.Schema

  @type t() :: %__MODULE__{}

  schema "hps_waypoints" do
    belongs_to :trace, Trace
    field :name, :string
    field :description, :string
    field :elevation, :float
    field :time, :utc_datetime
    field :point, Geo.PostGIS.Geometry
  end
end
