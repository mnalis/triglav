defmodule Triglav.Hps.Schemas.Track do
  alias Triglav.Hps.Schemas.Trace
  use Ecto.Schema

  @type t() :: %__MODULE__{
          __meta__: Ecto.Schema.Metadata.t(),
          trace: Trace.t() | nil,
          ref: String.t(),
          route_no: String.t(),
          name: String.t(),
          region: String.t(),
          url: String.t(),
          length: Decimal.t(),
          height_diff: integer(),
          walk_time: integer(),
          gpx1_url: String.t(),
          gpx2_url: String.t()
        }

  schema "hps_tracks" do
    has_one :trace, Trace
    field :ref, :string
    field :route_no, :string
    field :name, :string
    field :region, :string
    field :url, :string
    field :length, :decimal
    field :height_diff, :integer
    field :walk_time, :integer
    field :gpx1_url, :string
    field :gpx2_url, :string
  end
end
