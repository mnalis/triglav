defmodule Triglav.Hps.Schemas.Trace do
  alias Triglav.Hps.Schemas.Track
  alias Triglav.Hps.Schemas.Waypoint
  use Ecto.Schema

  @type t() :: %__MODULE__{}

  schema "hps_traces" do
    belongs_to :track, Track
    field :name, :string
    field :linestring, Geo.PostGIS.Geometry
    field :times, {:array, :utc_datetime}
    field :elevations, {:array, :float}
    field :distances, {:array, :float}
    has_many :waypoints, Waypoint
  end
end
