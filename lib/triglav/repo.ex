defmodule Triglav.Repo do
  use Ecto.Repo,
    otp_app: :triglav,
    adapter: Ecto.Adapters.Postgres

  def select!(sql, params \\ []) do
    {:ok, result} = query(sql, params)

    Enum.map(result.rows, fn row ->
      Enum.zip(result.columns, row) |> Enum.into(%{})
    end)
  end

  # TODO: see if this can be merged with `select!`, or perhaps `select!` can be
  # replaced with this implememntation.
  def select_atoms!(sql, params \\ []) do
    {:ok, result} = query(sql, params)
    columns = Enum.map(result.columns, &String.to_atom/1)

    Enum.map(result.rows, fn row ->
      Enum.zip(columns, row) |> Map.new()
    end)
  end

  @spec fetch(Ecto.Queryable.t(), term(), Keyword.t()) ::
          {:ok, Ecto.Schema.t()} | {:error, :not_found}
  def fetch(queryable, id, opts \\ []) do
    case get(queryable, id, opts) do
      nil -> {:error, :not_found}
      schema -> {:ok, schema}
    end
  end

  @spec fetch_by(Ecto.Queryable.t(), Keyword.t() | map(), Keyword.t()) ::
          {:ok, Ecto.Schema.t()} | {:error, :not_found}
  def fetch_by(queryable, clauses, opts \\ []) do
    case get_by(queryable, clauses, opts) do
      nil -> {:error, :not_found}
      schema -> {:ok, schema}
    end
  end

  @spec fetch_one(Ecto.Queryable.t(), Keyword.t()) ::
          {:ok, Ecto.Schema.t()} | {:error, :not_found}
  def fetch_one(queryable, opts \\ []) do
    case one(queryable, opts) do
      nil -> {:error, :not_found}
      record -> {:ok, record}
    end
  end

  @doc """
  Runs the given function inside a transaction.

  This function is a wrapper around `Ecto.Repo.transaction`, with the following differences:

  - It accepts only a lambda of arity 0 or 1 (i.e. it doesn't work with multi).
  - If the lambda returns `:ok | {:ok, result}` the transaction is committed.
  - If the lambda returns `:error | {:error, reason}` the transaction is rolled back.
  - If the lambda returns any other kind of result, an exception is raised, and the transaction is rolled back.
  - The result of `transact` is the value returned by the lambda.

  This function accepts the same options as `Ecto.Repo.transaction/2`.
  """
  @spec transact((() -> result) | (module -> result), Keyword.t()) :: result
        when result: :ok | {:ok, any} | :error | {:error, any}
  def transact(fun, opts \\ []) do
    transaction_result =
      transaction(
        fn repo ->
          lambda_result =
            case Function.info(fun, :arity) do
              {:arity, 0} -> fun.()
              {:arity, 1} -> fun.(repo)
            end

          case lambda_result do
            :ok -> {__MODULE__, :transact, :ok}
            :error -> rollback({__MODULE__, :transact, :error})
            {:ok, result} -> result
            {:error, reason} -> rollback(reason)
          end
        end,
        opts
      )

    with {outcome, {__MODULE__, :transact, outcome}} when outcome in [:ok, :error] <-
           transaction_result,
         do: outcome
  end

  def init(_context, config) do
    config =
      if Triglav.Helpers.mix_env() == :dev and System.get_env("LOG_ECTO") != "true",
        do: Keyword.put(config, :log, false),
        else: config

    {:ok, config}
  end
end
