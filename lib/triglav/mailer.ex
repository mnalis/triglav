defmodule Triglav.Mailer do
  use Swoosh.Mailer, otp_app: :triglav

  import Swoosh.Email

  @from {"Triglav", "triglav@bezdomni.net"}
  @to {"Ivan Habunek", "ivan@habunek.com"}

  def send_error(error) do
    new()
    |> to(@to)
    |> from(@from)
    |> subject("[Triglav] An error occured")
    |> text_body(inspect(error, pretty: true))
    |> Triglav.Mailer.deliver()
  end
end
