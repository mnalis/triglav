defmodule Triglav.Query do
  defmacro lower(field) do
    quote do
      fragment("lower(?)", unquote(field))
    end
  end

  defmacro tag(field, key) do
    quote do
      fragment("?.tags->?", unquote(field), unquote(key))
    end
  end
end
