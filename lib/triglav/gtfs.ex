defmodule Triglav.Gtfs do
  @typedoc """
  Works on GTFS data files and archives.
  https://gtfs.org/reference/static
  """
  @type file ::
          :agency
          | :attributions
          | :calendar
          | :calendar_dates
          | :fare_attributes
          | :fare_rules
          | :feed_info
          | :frequencies
          | :levels
          | :pathways
          | :routes
          | :shapes
          | :stop_times
          | :stops
          | :transfers
          | :translations
          | :trips

  @type file_paths ::
          %{
            required(:agency) => String.t(),
            required(:stops) => String.t(),
            required(:routes) => String.t(),
            required(:trips) => String.t(),
            required(:stop_times) => String.t(),
            optional(:calendar) => String.t(),
            optional(:calendar_dates) => String.t(),
            optional(:fare_attributes) => String.t(),
            optional(:fare_rules) => String.t(),
            optional(:shapes) => String.t(),
            optional(:frequencies) => String.t(),
            optional(:transfers) => String.t(),
            optional(:pathways) => String.t(),
            optional(:levels) => String.t(),
            optional(:translations) => String.t(),
            optional(:feed_info) => String.t(),
            optional(:attributions) => String.t()
          }

  @spec extract_archive(String.t()) :: file_paths()
  def extract_archive(path) do
    dirname = Path.basename(path, ".zip")
    target = Triglav.tmp_dir!() |> Path.join(dirname)
    File.mkdir_p!(target)

    {:ok, files} = :zip.extract(to_charlist(path), cwd: to_charlist(target))

    Map.new(files, fn path ->
      path = to_string(path)
      key = Path.basename(path, ".txt") |> String.to_atom()
      {key, path}
    end)
  end

  @type route_type() :: :tram | :subway | :railway | :bus

  @doc """
  Parse GTFS route type to OSM route.
  https://gtfs.org/reference/static#routestxt
  https://wiki.openstreetmap.org/wiki/Key:route
  """
  @spec parse_route_type(String.t()) :: route_type()
  def parse_route_type("0"), do: :tram
  def parse_route_type("1"), do: :subway
  def parse_route_type("2"), do: :train
  def parse_route_type("3"), do: :bus
  def parse_route_type("4"), do: :ferry
end
