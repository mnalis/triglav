defmodule Triglav.Autotrolej.Realtime do
  alias Triglav.Http

  # TODO: investigate etag header for ATvoznired.json
  # If they change overnight, that would simplify looking for changes.

  # NB: ATPoz.php linked on the rijeka data portal returns junk data
  @schedule_url "https://e-usluge2.rijeka.hr/OpenData/ATvoznired.json"
  @positions_url "https://e-usluge2.rijeka.hr/OpenData/ATPoz2.php?type=json"

  defmodule Trip do
    defstruct [:trip_id, :line_no, :direction, :variant, :name, :departure_time]
  end

  defmodule Position do
    defstruct [:vehicle_id, :trip_id, :stop_id, :position, :timestamp]
  end

  def fetch_positions() do
    @positions_url
    |> Http.get!()
    |> Jason.decode!()
    |> Enum.map(&parse_position/1)
  end

  def fetch_trips() do
    @schedule_url
    |> Http.get!()
    |> Jason.decode!()
    |> Stream.map(&parse_trip/1)
    |> Stream.uniq_by(& &1.trip_id)
    |> Enum.take(3)
  end

  defp parse_trip(row) do
    %Trip{
      trip_id: row["PolazakId"],
      line_no: row["BrojLinije"],
      direction: row["Smjer"],
      variant: row["Varijanta"],
      name: row["NazivVarijanteLinije"],
      departure_time: Time.from_iso8601!(row["Polazak"])
    }
  end

  defp parse_position(row) do
    %Position{
      vehicle_id: row["Autobus"],
      trip_id: row["PolazakId"],
      stop_id: row["StanicaId"],
      position: %Geo.Point{
        coordinates: {row["GpsX"], row["GpsY"]},
        srid: 4326
      },
      timestamp: parse_datetime(row["Vrijeme"])
    }
  end

  defp parse_datetime(value) do
    value
    |> NaiveDateTime.from_iso8601!()
    |> DateTime.from_naive!("Europe/Zagreb")
  end
end
