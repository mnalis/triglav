defmodule Triglav.Autotrolej.Import do
  @moduledoc """
  Import data from the Autotrolej API into the `PublicTransport` schemas.
  """
  alias Triglav.Autotrolej.Api
  alias Triglav.PublicTransport
  alias Triglav.PublicTransport.Operators
  alias Triglav.PublicTransport.Schemas.{Feed, Platform, RouteVariant, Route}
  alias Triglav.Repo

  import Ecto.Query

  require Logger

  @spec import(force: boolean()) :: {:ok, {Feed.t(), created? :: boolean()}} | {:error, term()}
  def import(opts \\ []) do
    Logger.info("Updating Autotrolej")

    force? = Keyword.get(opts, :force, false)
    source_data = Api.download_all()
    file_stats = Api.file_stats(source_data)
    data = Api.parse_data(source_data)

    Repo.transact(
      fn ->
        operator = Operators.autotrolej()
        existing_feed = Repo.get_by(Feed, ref: data.feed_ref)

        if existing_feed && not force? do
          Logger.info("Autotrolej already at latest version #{existing_feed.ref}")
          {:ok, {existing_feed, false}}
        else
          with {:ok, feed} <- import_feed(existing_feed, operator, data, file_stats) do
            routes = import_routes(feed, data)
            route_variants = import_route_variants(feed, routes, data)
            platforms = import_platforms(feed, data)
            import_route_variant_platforms(feed, route_variants, platforms, data)
            add_platform_use_count(feed.id)
            {:ok, {feed, true}}
          end
        end
      end,
      timeout: :timer.minutes(5)
    )
  end

  defp import_feed(existing_feed, operator, data, file_stats) do
    if existing_feed, do: PublicTransport.delete_feed(existing_feed.id)

    # Mark existing feeds as inactive, as this will be the new active feed
    where(Feed, operator_id: ^operator.id, active: true)
    |> Repo.update_all(set: [active: false])

    Repo.insert(%Feed{
      imported_at: data.feed_dt,
      operator_id: operator.id,
      ref: data.feed_ref,
      file_stats: file_stats,
      active: true
    })
  end

  defp import_routes(feed, data) do
    # Autotrolej has only buses
    route_type = :bus
    network = "Autotrolej"

    routes =
      Enum.map(data.routes, fn route ->
        %{
          ref: route.route_no,
          name: route.name,
          route_type: route_type,
          network: network,
          feed_id: feed.id
        }
      end)
      |> Enum.sort_by(&Integer.parse(&1.ref))
      |> Enum.with_index()
      |> Enum.map(fn {route, index} -> Map.put(route, :order_no, index) end)

    {count, records} = Repo.insert_all(Route, routes, returning: true)
    Logger.info("Inserted #{count} routes")
    records
  end

  defp import_route_variants(feed, routes, data) do
    route_ids = Map.new(routes, fn rm -> {rm.ref, rm.id} end)

    route_variants =
      data.route_variants
      |> Enum.map(fn route ->
        %{
          feed_id: feed.id,
          route_id: Map.fetch!(route_ids, route.route_no),
          ref: "#{route.route_no}_#{route.variant_no}",
          departure_count: route.departure_count,
          direction: route.direction,
          stop_ids: route.stops
        }
      end)

    {count, route_variants} = Repo.insert_all(RouteVariant, route_variants, returning: true)
    Logger.info("Inserted #{count} route variants")

    route_variants
  end

  defp import_platforms(feed, data) do
    platforms =
      data.stops
      # TODO: some stops don't have coordinates. are they used? hope not.
      |> Enum.reject(&(&1.longitude == nil or &1.latitude == nil))
      |> Enum.map(fn stop ->
        %{
          ref: to_string(stop.id),
          name: stop.name,
          feed_id: feed.id,
          geometry: %Geo.Point{coordinates: {stop.longitude, stop.latitude}, srid: 4326}
        }
      end)

    {count, platforms} = Repo.insert_all(Platform, platforms, returning: true)
    Logger.info("Inserted #{count} stops")
    platforms
  end

  defp import_route_variant_platforms(feed, route_variants, platforms, data) do
    route_variant_ids = Map.new(route_variants, &{&1.ref, &1.id})
    platform_ids = Map.new(platforms, &{&1.ref, &1.id})

    routes_platforms =
      data.route_variants
      |> Enum.map(fn route ->
        route.stops
        |> Enum.with_index()
        |> Enum.map(fn {ref, index} ->
          %{
            feed_id: feed.id,
            route_variant_id: Map.fetch!(route_variant_ids, "#{route.route_no}_#{route.variant_no}"),
            platform_id: Map.fetch!(platform_ids, ref),
            sequence_no: index + 1
          }
        end)
      end)
      |> List.flatten()

    {count, nil} = Repo.insert_all("pt_route_variant_platforms", routes_platforms)
    Logger.info("Inserted #{count} route platforms")
  end

  defp add_platform_use_count(feed_id) do
    Repo.query!(
      """
      UPDATE pt_platforms AS p
      SET use_count = tmp.count
      FROM (
        SELECT platform_id, count(*) AS count
        FROM pt_route_variant_platforms
        WHERE feed_id = $1
        GROUP BY 1
      ) AS tmp
      WHERE tmp.platform_id = p.id AND p.feed_id = $1;
      """,
      [feed_id]
    )
  end
end
