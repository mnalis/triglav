defmodule Triglav.Autotrolej.Api do
  @moduledoc """
  Load and parse Autotrolej CSV files.

  Source: http://data.rijeka.hr/organization/autotrolej
  License: http://opendefinition.org/licenses/cc-by/
  """
  use Tesla

  plug Tesla.Middleware.Logger, debug: false, log_level: :debug
  plug Tesla.Middleware.Timeout, timeout: 10_000

  alias Triglav.Helpers.{Counter, FileUtils}
  alias Triglav.PublicTransport.Schemas.FileStats

  require Logger

  NimbleCSV.define(Parser, separator: ";", escape: "\"")

  @type file :: :routes | :stops | :schedule_saturday | :schedule_sunday | :schedule_workday
  @type path :: String.t()
  @type source_data :: %{file() => path()}

  @type stop :: %{
          id: integer(),
          latitude: float(),
          longitude: float(),
          name: String.t()
        }

  @type route :: %{
          route_no: String.t(),
          name: String.t()
        }

  @type route_variant :: %{
          departure_count: pos_integer(),
          route_no: String.t(),
          stops: {ordinal_no :: pos_integer, stop_id :: String.t()},
          variant_no: pos_integer()
        }

  @type parsed_data :: %{
          feed_dt: DateTime.t(),
          feed_ref: String.t(),
          routes: [route()],
          route_variants: [route_variant()],
          stops: [stop()]
        }

  @data_file_urls %{
    routes: "http://e-usluge2.rijeka.hr/OpenData/ATlinije.csv",
    stops: "http://e-usluge2.rijeka.hr/OpenData/ATstanice.csv",
    schedule_saturday: "http://e-usluge2.rijeka.hr/OpenData/ATvoznired-subota.csv",
    schedule_sunday: "http://e-usluge2.rijeka.hr/OpenData/ATvoznired-nedjelja.csv",
    schedule_workday: "http://e-usluge2.rijeka.hr/OpenData/ATvoznired-tjedan.csv"
  }

  @doc """
  Downloads all files required CSV to a temporary directory.

  Skips files which are already downloaded.

  The `etag` is extracted from headers and hopefully changes only when the file
  contents change.
  """
  @spec download_all() :: source_data()
  def download_all() do
    tmp_dir = Triglav.tmp_dir!()
    target_dir = Path.join(tmp_dir, "autotrolej")
    File.mkdir_p!(target_dir)

    tasks =
      Enum.map(@data_file_urls, fn {_slug, url} ->
        Task.async(fn -> download_one(url, target_dir) end)
      end)

    results = Task.await_many(tasks, :timer.seconds(60))

    @data_file_urls
    |> Map.keys()
    |> Enum.zip(results)
    |> Map.new()
  end

  @spec file_stats(source_data()) :: [FileStats.t()]
  def file_stats(source_data) do
    for {_file, path} <- source_data do
      %FileStats{
        file: Path.basename(path),
        line_count: FileUtils.wc_l!(path),
        md5sum: FileUtils.md5sum!(path)
      }
    end
  end

  defp download_one(url, target_dir) do
    Triglav.Http.download!(url, target_dir, filename: url_filename(url), overwrite: true)
  end

  defp url_filename(url) do
    url
    |> URI.parse()
    |> Map.fetch!(:path)
    |> Path.basename()
  end

  @spec parse_data(source_data()) :: parsed_data()
  def parse_data(source_data) do
    feed_dt = DateTime.utc_now() |> DateTime.truncate(:second)
    schedule_stream = get_schedule_stream(source_data)
    stops = parse_stops(source_data.stops)
    routes = parse_routes(schedule_stream)
    route_variants = parse_route_variants(schedule_stream)

    data = %{routes: routes, route_variants: route_variants, stops: stops}
    # Calculate the feed ref as hash of the parsed data, this way the feed
    # will be imported every time the parsed data changes.
    feed_ref = :erlang.phash2(data) |> Integer.to_string(16)

    Map.merge(data, %{feed_ref: feed_ref, feed_dt: feed_dt})
  end

  defp get_schedule_stream(source_data) do
    schedule_paths = [
      source_data.schedule_workday,
      source_data.schedule_saturday,
      source_data.schedule_sunday
    ]

    for path <- schedule_paths do
      path
      |> File.stream!()
      |> Parser.parse_stream()
    end
    |> Stream.concat()
  end

  @spec parse_stops(String.t()) :: [stop()]
  defp parse_stops(path) do
    path
    |> File.stream!()
    |> Parser.parse_stream()
    |> Enum.map(fn [id, name, _, longitude, latitude] ->
      %{
        id: id,
        name: name,
        latitude: to_float(latitude),
        longitude: to_float(longitude)
      }
    end)
  end

  @spec parse_routes(Enumerable.t()) :: [route()]
  defp parse_routes(schedule_stream) do
    schedule_stream
    |> Stream.map(fn [_, _, _, _, _, _, route_no, direction, _, name, _, _, _, _] ->
      %{route_no: route_no, name: name, direction: direction}
    end)
    |> Stream.uniq()
    |> Enum.sort_by(&{&1.route_no, &1.direction})
    |> Enum.uniq_by(& &1.route_no)
    |> Enum.map(&Map.drop(&1, [:direction]))
  end

  @spec parse_route_variants(Enumerable.t()) :: [route_variant()]
  def parse_route_variants(schedule_stream) do
    trip_counter = Counter.new()

    schedule_stream
    |> Stream.chunk_by(fn [_, trip_id | _] -> trip_id end)
    |> Stream.map(fn chunk ->
      [_, trip_id, _, _, _, _, route_no, direction | _] = List.first(chunk)

      stops =
        chunk
        |> Enum.sort_by(&to_integer(Enum.at(&1, 5)))
        |> Enum.map(&Enum.at(&1, 2))

      {route_no, trip_id, direction, stops}
    end)
    |> Enum.uniq_by(fn {route_no, _trip_id, direction, stops} ->
      Counter.increment(trip_counter, {route_no, direction, stops})
      {route_no, direction, stops}
    end)
    |> Enum.map(fn {route_no, trip_id, direction, stops} ->
      count = Counter.get(trip_counter, {route_no, direction, stops})
      {trip_id, route_no, direction, stops, count}
    end)
    |> Enum.sort_by(fn {_trip_id, route_no, direction, _stops, count} ->
      {sort_value(route_no), direction, -count}
    end)
    |> Enum.map(fn {trip_id, route_no, direction, stops, count} ->
      %{
        departure_count: count,
        route_no: route_no,
        stops: stops,
        variant_no: trip_id,
        direction: direction
      }
    end)
    |> then(fn variants ->
      Counter.delete(trip_counter)
      variants
    end)
  end

  defp to_integer(str) do
    {int, ""} = Integer.parse(str)
    int
  end

  defp to_float(""), do: nil

  defp to_float(str) do
    {float, ""} = Float.parse(str)
    float
  end

  @doc """
  Given a route no creates a tuple which can be used to sort routes in a
  desirable order.
  """
  def sort_value(route_no) do
    route_no
    |> String.graphemes()
    |> Enum.chunk_by(&String.match?(&1, ~r"[0-9]"))
    |> Enum.map(fn chunks -> maybe_to_integer(Enum.join(chunks)) end)
    |> then(fn parts ->
      if length(parts) == 1, do: Enum.concat(parts, [nil]), else: parts
    end)
    |> List.to_tuple()
  end

  def maybe_to_integer(str) do
    case Integer.parse(str) do
      {int, ""} -> int
      _ -> str
    end
  end
end
