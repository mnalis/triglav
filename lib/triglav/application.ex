defmodule Triglav.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    minimal = Application.get_env(:triglav, :repo_only, false)

    children =
      if minimal do
        [Triglav.Repo]
      else
        [
          Triglav.Repo,
          TriglavWeb.Telemetry,
          {Phoenix.PubSub, name: Triglav.PubSub},
          TriglavWeb.Endpoint,
          Triglav.PublicTransport.Loader
        ]
      end

    Triglav.Telemetry.start!()

    opts = [strategy: :one_for_one, name: Triglav.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    TriglavWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
