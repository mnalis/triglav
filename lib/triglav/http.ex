defmodule Triglav.Http do
  require Logger

  @spec get(String.t()) :: {:ok, binary()} | {:error, term()}
  def get(url) do
    case :httpc.request(:get, {to_charlist(url), []}, [], body_format: :binary) do
      {:ok, {{'HTTP/1.1', 200, 'OK'}, _headers, body}} -> {:ok, body}
      {:ok, {{_, 404, _}, _headers, _body}} -> {:error, :not_found}
      _ -> {:error, :unknown}
    end
  end

  @spec get!(String.t()) :: {:ok, binary()}
  def get!(url) do
    {:ok, body} = get(url)
    body
  end

  @spec download(String.t(), String.t(), Keyword.t()) :: {:ok, String.t()} | {:error, term()}
  def download(url, target_dir, opts \\ []) do
    default_filename =
      url
      |> URI.parse()
      |> Map.fetch!(:path)
      |> Path.basename()

    overwrite? = Keyword.get(opts, :overwrite, false)
    filename = Keyword.get(opts, :filename, default_filename)
    path = Path.join(target_dir, filename)
    exists? = File.exists?(path)

    with :ok <- maybe_delete(path, exists?, overwrite?),
         :ok <- do_download(url, path) do
      {:ok, path}
    end
  end

  def download!(url, target_dir, opts \\ []) do
    {:ok, path} = download(url, target_dir, opts)
    path
  end

  defp maybe_delete(_, false, _), do: :ok
  defp maybe_delete(_, true, false), do: {:error, :file_exists}
  defp maybe_delete(path, true, true), do: File.rm(path)

  defp do_download(url, target) do
    Logger.debug("Downloading #{url} to #{target}")

    case :httpc.request(:get, {to_charlist(url), []}, [], stream: to_charlist(target)) do
      {:ok, :saved_to_file} -> :ok
    end
  end
end
