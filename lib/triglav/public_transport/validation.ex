defmodule Triglav.PublicTransport.Validation do
  @moduledoc """
  Validates public transport routes in OSM based on imported data.
  """
  alias Triglav.Haversine
  alias Triglav.Osm
  alias Triglav.Osm.Tags
  alias Triglav.PublicTransport.Schemas.{Error, Feed, RouteRelation}
  alias Triglav.PublicTransport.Validation.Errors
  alias Triglav.Repo
  alias Triglav.Schemas.Osmosis.Relation

  import Ecto.Query

  @spec validate(Feed.id()) :: [Error.t()]
  def validate(feed_id) do
    feed =
      Feed
      |> Repo.get!(feed_id)
      |> Repo.preload([
        :operator,
        routes: [
          :variants,
          [mapped_route_relations: :members],
          [mapped_route_master_relations: :members]
        ]
      ])

    route_relations =
      RouteRelation
      |> where(operator_id: ^feed.operator_id)
      |> Repo.all()
      |> Repo.preload([:relation, platforms: [:node, :way]])

    # TODO: think about how to avoid preloading memebers here
    relation_map =
      route_relations
      |> Enum.map(& &1.relation)
      |> Osm.preload_members(&(&1.member_role == "" and &1.member_type == "W"))
      |> Map.new(&{&1.id, &1})

    route_relations = Map.new(route_relations, &{&1.id, &1})

    feed
    |> validate_routes(relation_map, route_relations)
    |> List.flatten()
    |> Enum.reject(&is_nil/1)
  end

  defp validate_routes(feed, relation_map, route_relations) do
    for route <- feed.routes do
      validate_route(feed, route, relation_map, route_relations)
    end
  end

  defp validate_route(feed, route, relation_map, route_relations) do
    [
      validate_has_relations(feed, route),
      validate_has_route_master(feed, route),
      validate_routes_are_contained_in_route_master(feed, route),
      validate_routes_master_route_has_no_unknown_members(feed, route),
      validate_platform_tags(feed, route, route_relations),
      Enum.map(route.mapped_route_master_relations, &validate_route_master_relation(feed, route, &1)),
      Enum.map(route.mapped_route_relations, &validate_route_relation(feed, route, &1, relation_map, route_relations))
    ]
  end

  defp validate_has_relations(feed, route) do
    if Enum.empty?(route.mapped_route_relations) and Enum.empty?(route.mapped_route_master_relations) do
      Errors.no_relations(feed, route)
    end
  end

  defp validate_has_route_master(feed, route) do
    count = length(route.mapped_route_master_relations)
    all_roundtrips = Enum.all?(route.mapped_route_relations, &Relation.is_roundtrip/1)

    cond do
      count == 0 and !all_roundtrips -> Errors.missing_route_master(feed, route)
      count > 1 -> Errors.multiple_route_masters(feed, route)
      true -> nil
    end
  end

  defp validate_routes_are_contained_in_route_master(feed, route) do
    if length(route.mapped_route_master_relations) == 1 do
      [route_master] = route.mapped_route_master_relations
      route_relations = route.mapped_route_relations

      contained_ids = route_master.members |> Enum.map(& &1.member_id) |> MapSet.new()
      route_ids = route_relations |> Enum.map(& &1.id) |> MapSet.new()
      extra_ids = MapSet.difference(route_ids, contained_ids)
      extra_relations = Enum.filter(route_relations, &(&1.id in extra_ids))

      for relation <- extra_relations do
        Errors.relation_not_contained_in_route_master(feed, route, relation)
      end
    end
  end

  defp validate_routes_master_route_has_no_unknown_members(feed, route) do
    route_masters = route.mapped_route_master_relations

    if length(route_masters) == 1 do
      [route_master] = route_masters

      expected_members =
        route.mapped_route_relations
        |> Enum.map(&{"R", &1.id})
        |> MapSet.new()

      actual_members =
        route_master.members
        |> Enum.map(&{&1.member_type, &1.member_id})
        |> MapSet.new()

      unexpected_members = MapSet.difference(actual_members, expected_members)

      for {member_type, member_id} <- unexpected_members do
        Errors.unexpected_route_master_member(
          feed,
          route,
          route_master,
          member_type,
          member_id
        )
      end
    end
  end

  defp validate_route_master_relation(feed, route, relation) do
    route_type = to_string(route.route_type)
    required_tags = ["type", "ref", "name", "route_master"]
    allowed_tags = required_tags ++ ["network:wikidata", "network:wikipedia", "operator", "network", "wikidata"]
    expected_name = "#{String.capitalize(route_type)} #{route.ref}"

    expected_tags = %{
      "route_master" => route_type,
      "network" => feed.operator.ref,
      "operator" => feed.operator.ref,
      "ref" => route.ref,
      "name" => expected_name
    }

    [
      validate_required_tags(feed, route, relation, required_tags),
      validate_allowed_tags(feed, route, relation, allowed_tags),
      validate_tag_values(feed, route, relation, expected_tags)
    ]
  end

  defp validate_route_relation(feed, route, relation, relation_map, route_relations) do
    required_tags = ["type", "route", "ref", "from", "to", "name"]

    allowed_tags =
      Enum.concat(required_tags, [
        "network",
        "network:wikidata",
        "network:wikipedia",
        "operator",
        "public_transport:version",
        "roundtrip",
        "via",
        "wikidata"
      ])

    expected_tags = %{
      "route" => to_string(route.route_type),
      "network" => feed.operator.ref,
      "ref" => route.ref,
      "public_transport:version" => "2",
      "operator" => feed.operator.ref
    }

    [
      validate_is_ptv2(feed, route, relation),
      validate_route_naming(feed, route, relation),
      validate_required_tags(feed, route, relation, required_tags),
      validate_allowed_tags(feed, route, relation, allowed_tags),
      validate_tag_values(feed, route, relation, expected_tags),
      validate_continuous_ways(feed, route, relation, route_relations),
      validate_oneway_streets(feed, route, relation, relation_map),
      validate_has_gtfs_stop_ids(feed, route, relation, route_relations),
      validate_member_roles(feed, route, relation),
      validate_platform_distance_from_route(feed, route, relation, route_relations),
      validate_terminus_distance_from_route(feed, route, relation, route_relations)
    ]
  end

  defp validate_is_ptv2(feed, route, relation) do
    if !Relation.is_ptv2(relation) do
      Errors.relation_not_updated_to_ptv2(feed, route, relation)
    end
  end

  defp validate_route_naming(feed, route, relation) do
    if Tags.has_all?(relation, ["name", "ref", "from", "to"]) do
      actual = Tags.get(relation, "name")
      ref = Tags.get(relation, "ref")
      type = Tags.get(relation, "route")
      from = Tags.get(relation, "from")
      to = Tags.get(relation, "to")
      via = Tags.get(relation, "via")

      via_segments =
        if via,
          do: via |> String.split(";") |> Enum.map(&String.trim/1),
          else: []

      name_segments = [from | via_segments] ++ [to]
      expected = "#{String.capitalize(type)} #{ref}: " <> Enum.join(name_segments, " => ")

      if actual != expected do
        Errors.invalid_relation_name(feed, route, relation, actual, expected)
      end
    end
  end

  # If trip has an exact linestring, that means route is complete, so only
  # run this check if exact is false.
  defp validate_continuous_ways(feed, route, relation, route_relations) do
    relation = Map.fetch!(route_relations, relation.id)

    if not relation.contiguous do
      Errors.broken_route(feed, route, relation.relation)
    end
  end

  # Check that the route does not pass through any oneway streets in the
  # opposite direction
  # TODO: this re-does a lot of work from above fn, try to dedupe it
  defp validate_oneway_streets(feed, route, relation, relation_map) do
    relation = Map.fetch!(relation_map, relation.id)
    way_members = Enum.filter(relation.members, &(&1.member_type == "W" and &1.member_role == ""))

    sequence_map = Map.new(way_members, &{&1.member.id, &1.sequence_id})
    ordered_ways = Enum.map(way_members, & &1.member)

    case Osm.Router.annotate_ways(ordered_ways) do
      {:ok, annotated_ways} ->
        for annotated_way <- annotated_ways do
          oneway? = Map.get(annotated_way.way.tags, "oneway") == "yes"

          # Oneway restriction can be overriden for public service vehicles
          psv_override? = Map.get(annotated_way.way.tags, "oneway:psv") == "no"

          if annotated_way.direction == :backward and oneway? and not psv_override? do
            sequence_id = Map.fetch!(sequence_map, annotated_way.way.id)
            Errors.wrong_way(feed, route, relation, annotated_way.way, sequence_id)
          end
        end

      _ ->
        nil
    end
  end

  defp validate_has_gtfs_stop_ids(feed, route, relation, route_relations) do
    route_relation = Map.fetch!(route_relations, relation.id)
    empty_count = Enum.count(route_relation.platforms, &is_nil(&1.stop_id))

    if empty_count > 0 do
      Errors.relation_missing_gtfs_stop_ids(feed, route, relation, empty_count)
    end
  end

  defp validate_member_roles(feed, route, relation) do
    expected_node_roles = [
      "platform",
      "platform_entry_only",
      "platform_exit_only",
      "stop",
      "stop_entry_only",
      "stop_exit_only"
    ]

    expected_way_roles = [
      "",
      "platform",
      "platform_entry_only",
      "platform_exit_only"
    ]

    if Relation.is_ptv2(relation) do
      for member <- relation.members do
        if (member.member_type == "N" and member.member_role not in expected_node_roles) or
             (member.member_type == "W" and member.member_role not in expected_way_roles) do
          Errors.unexpected_member_role(feed, route, relation, member)
        end
      end
    end
  end

  defp validate_platform_distance_from_route(feed, route, relation, route_relations) do
    route_relation = Map.fetch!(route_relations, relation.id)

    for platform <- route_relation.platforms do
      if platform.distance_from_route > 25 do
        Errors.platform_too_far_from_route(feed, route, relation, platform)
      end
    end
  end

  defp validate_terminus_distance_from_route(feed, route, relation, route_relations) do
    route_relation = Map.fetch!(route_relations, relation.id)

    if route_relation.contiguous and length(route_relation.platforms) >= 2 do
      route_start = List.first(route_relation.geometry.coordinates) |> swap()
      route_end = List.last(route_relation.geometry.coordinates) |> swap()

      first_platform = List.first(route_relation.platforms)
      last_platform = List.last(route_relation.platforms)

      first_platform_coordinates = platform_coordinates(first_platform.geometry)
      last_platform_coordinates = platform_coordinates(last_platform.geometry)

      distance_from_start = Haversine.distance(route_start, first_platform_coordinates)
      distance_from_end = Haversine.distance(route_end, last_platform_coordinates)

      error_start =
        if distance_from_start > 25 do
          Errors.terminus_too_far_from_route_end(feed, route, relation, first_platform, distance_from_start, true)
        end

      error_end =
        if distance_from_end > 25 do
          Errors.terminus_too_far_from_route_end(feed, route, relation, last_platform, distance_from_end, false)
        end

      [error_start, error_end]
    end
  end

  defp swap({x, y}), do: {y, x}

  defp platform_coordinates(%Geo.Point{} = point), do: swap(point.coordinates)

  # If platform is a way, calculate the center point
  defp platform_coordinates(%Geo.LineString{} = line_string) do
    {lats, lons} = Enum.unzip(line_string.coordinates)
    lat = Enum.sum(lats) / Enum.count(lats)
    lon = Enum.sum(lons) / Enum.count(lons)
    {lon, lat}
  end

  defp validate_platform_tags(feed, route, route_relations) do
    for relation <- route.mapped_route_relations do
      route_relation = Map.fetch!(route_relations, relation.id)

      for platform <- route_relation.platforms do
        expected_tags = expected_platform_tags(route_relation.relation)
        member = platform.way || platform.node

        for {name, expected} <- expected_tags do
          actual = Map.get(member.tags, name)

          if actual != expected do
            Errors.platform_invalid_tag_value(
              feed,
              route,
              relation,
              platform.sequence_id,
              name,
              actual,
              expected
            )
          end
        end
      end
    end
  end

  defp expected_platform_tags(%Relation{tags: %{"type" => "route", "route" => "bus"}}) do
    %{
      "bus" => "yes",
      "highway" => "bus_stop",
      "public_transport" => "platform"
    }
  end

  defp expected_platform_tags(%Relation{tags: %{"type" => "route", "route" => "tram"}}) do
    %{
      "tram" => "yes",
      "railway" => "platform",
      "public_transport" => "platform"
    }
  end

  # ----------------------------------------------------------------------------
  # Generic relation validators
  # ----------------------------------------------------------------------------

  defp validate_required_tags(feed, route, relation, required_tags) do
    required = MapSet.new(required_tags)
    existing = MapSet.new(Map.keys(relation.tags))
    missing = MapSet.difference(required, existing) |> MapSet.to_list()

    if length(missing) > 0 do
      Errors.relation_missing_required_tags(feed, route, relation, missing)
    end
  end

  defp validate_allowed_tags(feed, route, relation, allowed_tags) do
    allowed = MapSet.new(allowed_tags)
    existing = MapSet.new(Map.keys(relation.tags))
    unexpected = MapSet.difference(existing, allowed) |> MapSet.to_list()

    if length(unexpected) > 0 do
      Errors.relation_contains_unexpected_tags(feed, route, relation, unexpected)
    end
  end

  defp validate_tag_values(feed, route, relation, expected_tags) do
    for {name, expected} <- expected_tags do
      actual = Map.get(relation.tags, name)

      if actual != expected do
        Errors.relation_invalid_tag_value(feed, route, relation, name, actual, expected)
      end
    end
  end
end
