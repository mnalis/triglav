defmodule Triglav.PublicTransport.Schemas.RouteVariant do
  use Ecto.Schema

  alias Triglav.PublicTransport.Schemas.Feed
  alias Triglav.PublicTransport.Schemas.Route
  alias Triglav.PublicTransport.Schemas.RouteVariantPlatform
  alias Triglav.Schemas.Osmosis.Relation

  @type id() :: integer()
  @type t() :: %__MODULE__{}

  schema "pt_route_variants" do
    belongs_to :feed, Feed
    belongs_to :route, Route
    belongs_to :mapped_relation, Relation

    field :departure_count, :integer
    field :ref, :string
    field :direction, :string
    field :stop_ids, {:array, :string}

    # Route geometry is derived from the mapped OSM relation, if it exists
    field :geometry, Geo.PostGIS.Geometry

    has_many :platforms, RouteVariantPlatform, preload_order: [asc: :sequence_no]
  end
end
