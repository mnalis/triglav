defmodule Triglav.PublicTransport.Schemas.ErrorHistory do
  use Ecto.Schema

  alias Triglav.PublicTransport.Schemas.Operator

  @type t :: %__MODULE__{}

  schema "pt_error_history" do
    belongs_to :operator, Operator
    field :count, :integer
    field :previous_count, :integer
    field :created_count, :integer
    field :resolved_count, :integer
    field :osm_sequence_number, :integer
    field :feed_ref, :string
    field :timestamp, :utc_datetime
    field :created_error_ids, {:array, :integer}
    field :resolved_error_ids, {:array, :integer}
  end
end
