defmodule Triglav.PublicTransport.Schemas.RouteVariantPlatform do
  use Ecto.Schema

  alias Triglav.PublicTransport.Schemas.Feed
  alias Triglav.PublicTransport.Schemas.Platform
  alias Triglav.PublicTransport.Schemas.RouteVariant

  @type t() :: %__MODULE__{}

  schema "pt_route_variant_platforms" do
    belongs_to :feed, Feed
    belongs_to :route_variant, RouteVariant
    belongs_to :platform, Platform

    field :sequence_no, :integer
  end
end
