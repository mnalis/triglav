defmodule Triglav.PublicTransport.Schemas.Route do
  use Ecto.Schema

  alias Triglav.PublicTransport.Schemas.{Feed, RouteVariant}
  alias Triglav.Schemas.Osmosis.Relation

  @type id() :: pos_integer()
  @type t() :: %__MODULE__{}

  schema "pt_routes" do
    belongs_to :feed, Feed

    field :name, :string
    field :network, :string
    field :order_no, :integer
    field :ref, :string
    field :route_type, Ecto.Enum, values: [:bus, :train, :tram, :subway, :ferry]

    has_many :variants, RouteVariant

    # TODO: consider mapping relations via an intermediate table which will
    # contain extra information about said relations, such as relation
    # geometry

    # Relates to OSM relations of type="route" matched by ref
    many_to_many :mapped_route_relations, Relation, join_through: "pt_route_mapped_route_relations"

    # Relates to OSM relations of type="route_master" matched by ref
    many_to_many :mapped_route_master_relations, Relation, join_through: "pt_route_mapped_route_master_relations"
  end
end
