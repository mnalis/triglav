defmodule Triglav.PublicTransport.Schemas.PlatformMapping do
  use Ecto.Schema

  @type t() :: %__MODULE__{}

  alias Triglav.PublicTransport.Schemas.{Feed, Platform}
  alias Triglav.Schemas.Osmosis.{Node, Way}

  schema "pt_platform_mappings" do
    belongs_to :feed, Feed
    belongs_to :platform, Platform
    belongs_to :way, Way
    belongs_to :node, Node

    field :distance, :float
  end
end
