defmodule Triglav.PublicTransport.Schemas.RouteRelationPlatform do
  use Ecto.Schema

  alias Triglav.PublicTransport.Schemas.RouteRelation
  alias Triglav.Schemas.Osmosis.{Node, Way}

  @type t() :: %__MODULE__{}

  schema "pt_route_relation_platforms" do
    belongs_to :route_relation, RouteRelation
    belongs_to :node, Node
    belongs_to :way, Way

    field :stop_id, :string
    field :sequence_id, :integer
    field :geometry, Geo.PostGIS.Geometry
    field :distance_from_route, :float
  end
end
