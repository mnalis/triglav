defmodule Triglav.PublicTransport.Schemas.Operator do
  use Ecto.Schema

  @type id() :: pos_integer()
  @type t() :: %__MODULE__{}

  schema "pt_operators" do
    field :name, :string
    field :ref, :string
    field :slug, :string
    field :wikidata_id, :string
  end
end
