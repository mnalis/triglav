defmodule Triglav.PublicTransport.Schemas.RouteRelation do
  @moduledoc """
  Additional data for OSM route relations.
  """
  use Ecto.Schema

  alias Triglav.PublicTransport.Schemas.Operator
  alias Triglav.PublicTransport.Schemas.RouteRelationPlatform
  alias Triglav.Schemas.Osmosis.Relation

  @type id() :: pos_integer()
  @type t() :: %__MODULE__{}

  schema "pt_route_relations" do
    belongs_to :operator, Operator
    belongs_to :relation, Relation

    field :stop_ids, {:array, :string}
    field :geometry, Geo.PostGIS.Geometry
    field :contiguous, :boolean

    has_many :platforms, RouteRelationPlatform, preload_order: [asc: :sequence_id]
  end
end
