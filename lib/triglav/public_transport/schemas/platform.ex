defmodule Triglav.PublicTransport.Schemas.Platform do
  use Ecto.Schema

  alias Triglav.GeoJSON
  alias Triglav.PublicTransport.Schemas.{Feed, PlatformMapping}

  @type t() :: %__MODULE__{}

  schema "pt_platforms" do
    belongs_to :feed, Feed

    field :geometry, Geo.PostGIS.Geometry
    field :name, :string
    field :ref, :string
    field :use_count, :integer

    # A GTFS platform can match multiple OSM platforms if the OSM platforms
    # have the same gtfs:stop_id tag value as the GTFS platform.
    has_many :mappings, PlatformMapping
  end

  def to_feature(%__MODULE__{} = platform) do
    properties =
      platform
      |> Map.take([:ref, :name, :use_count])
      |> Map.put(:matched, platform.mappings != [])

    GeoJSON.feature(platform.geometry, properties)
  end
end
