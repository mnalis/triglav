defmodule Triglav.PublicTransport.Schemas.Error do
  use Ecto.Schema

  alias Triglav.Helpers.MapUtils
  alias Triglav.PublicTransport.Schemas.{Feed, Operator}
  alias Triglav.Schemas.Osmosis.Relation

  @type t :: %__MODULE__{}

  @type tag_name :: String.t()
  @type tag_value :: String.t()

  schema "pt_errors" do
    belongs_to :operator, Operator
    belongs_to :relation, Relation

    # TODO: this may not be needed?
    belongs_to :feed, Feed

    field :created_at, :utc_datetime
    field :key, :string
    field :params, :map, default: %{}
    field :resolved_at, :utc_datetime
    field :route_ref, :string
    field :sequence_id, :integer
  end

  @doc """
  For a given error returns a map of fields which describe the error, without
  the metadata (primary key, timestamps). This map can be used to compare two
  errors to see if they are the same.

  Map keys in `params` are strings when fetched from the db, so they are
  converted to atoms so they can be compared with errors created by factory
  methods below which use atom keys.
  """
  @spec signature(t()) :: map()
  def signature(%__MODULE__{} = error) do
    error
    |> Map.take([:route_ref, :relation_id, :sequence_id, :key, :params])
    # Map keys are strings when fetched from the db, convert them to atoms
    |> Map.update!(:params, &MapUtils.keys_to_atoms/1)
  end
end
