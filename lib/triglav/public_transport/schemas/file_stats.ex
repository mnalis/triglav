defmodule Triglav.PublicTransport.Schemas.FileStats do
  use Ecto.Schema

  @type t() :: %__MODULE__{}
  @primary_key false

  embedded_schema do
    field :file, :string
    field :line_count, :integer
    field :md5sum, :string
  end
end
