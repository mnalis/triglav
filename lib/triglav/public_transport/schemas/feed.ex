defmodule Triglav.PublicTransport.Schemas.Feed do
  use Ecto.Schema

  alias Triglav.PublicTransport.Schemas.{
    FileStats,
    Operator,
    Platform,
    Route,
    RouteVariant,
    RouteVariantPlatform,
    PlatformMapping
  }

  @type id() :: integer()
  @type t() :: %__MODULE__{}

  schema "pt_feeds" do
    belongs_to :operator, Operator
    embeds_many :file_stats, FileStats
    field :ref, :string
    field :imported_at, :utc_datetime
    field :start_date, :date
    field :end_date, :date
    field :active, :boolean

    has_many :platform_mappings, PlatformMapping
    has_many :platforms, Platform
    has_many :route_variant_platforms, RouteVariantPlatform
    has_many :route_variants, RouteVariant
    has_many :routes, Route
  end
end
