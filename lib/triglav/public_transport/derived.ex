defmodule Triglav.PublicTransport.Derived do
  @moduledoc """
  Generates derived OSM data from osmosis.* tables.
  This includes RouteRelation and RouteRelationPlatform tables.
  """

  alias Triglav.Osm
  alias Triglav.PublicTransport
  alias Triglav.PublicTransport.Schemas.Operator
  alias Triglav.PublicTransport.Schemas.{RouteRelation, RouteRelationPlatform}
  alias Triglav.Repo
  alias Triglav.Schemas.Osmosis.{Node, Relation, Way}

  import Ecto.Query
  import Triglav.Query

  require Logger

  @doc """
  Regenerate derived data for an operator.
  """
  @spec regenerate(Operator.t()) :: :ok
  def regenerate(operator) do
    Repo.transact(fn ->
      delete(operator)
      generate(operator)
      :ok
    end)
  end

  @spec delete(Operator.t()) :: :ok
  defp delete(operator) do
    Logger.info("Deleting derived data for #{operator.name}")

    relations_query = from(relation in RouteRelation, where: [operator_id: ^operator.id])
    relation_ids = relations_query |> select([relation], relation.id) |> Repo.all()
    platforms_query = from(platform in RouteRelationPlatform, where: platform.route_relation_id in ^relation_ids)

    {platform_count, nil} = Repo.delete_all(platforms_query)
    Logger.debug("Deleted #{platform_count} platforms")

    {relation_count, nil} = Repo.delete_all(relations_query)
    Logger.debug("Deleted #{relation_count} relations")
  end

  defp generate(operator) do
    Logger.info("Generating derived data for #{operator.name}")
    relations = list_relations(operator)
    route_relations = Enum.map(relations, &to_route_relation(operator, &1))

    route_relation_platforms =
      for relation <- relations,
          member <- relation.members,
          String.starts_with?(member.member_role, "platform") do
        to_route_relation_platform(relation, member.member, member.sequence_id)
      end

    {count, nil} = Repo.insert_all(RouteRelation, route_relations)
    Logger.debug("Inserted #{count} relations")

    {count, nil} = Repo.insert_all(RouteRelationPlatform, route_relation_platforms)
    Logger.debug("Inserted #{count} platforms")

    add_distance_from_route()
  end

  defp list_relations(operator) do
    variants = [String.downcase(operator.name), String.downcase(operator.ref)]

    from(relation in Relation,
      where: lower(tag(relation, "operator")) in ^variants,
      where: tag(relation, "type") in ["route"]
    )
    |> Repo.all()
    |> Osm.preload_members(&(&1.member_type in ["N", "W"]))
  end

  defp to_route_relation(operator, relation) do
    geometry = PublicTransport.route_relation_geometry(relation)
    contiguous = geometry.__struct__ == Geo.LineString
    stop_ids = stop_ids(relation)
    # TODO: if not all platforms have a stop id, perhaps set stop_ids to null?

    %{
      id: relation.id,
      # TODO: hm?
      relation_id: relation.id,
      operator_id: operator.id,
      stop_ids: stop_ids,
      geometry: geometry,
      contiguous: contiguous
    }
  end

  defp stop_ids(relation) do
    relation.members
    |> Enum.filter(&String.starts_with?(&1.member_role, "platform"))
    |> Enum.map(& &1.member.tags["gtfs:stop_id"])
    |> Enum.reject(&is_nil/1)
  end

  defp to_route_relation_platform(relation, %Node{} = node, sequence_id) do
    %{
      route_relation_id: relation.id,
      geometry: node.geom,
      node_id: node.id,
      sequence_id: sequence_id,
      stop_id: node.tags["gtfs:stop_id"]
    }
  end

  defp to_route_relation_platform(relation, %Way{} = way, sequence_id) do
    %{
      route_relation_id: relation.id,
      geometry: way.linestring,
      sequence_id: sequence_id,
      stop_id: way.tags["gtfs:stop_id"],
      way_id: way.id
    }
  end

  defp add_distance_from_route() do
    Repo.query!("""
    UPDATE pt_route_relation_platforms p
    SET distance_from_route = st_distance(r.geometry::geography, p.geometry::geography)
    FROM pt_route_relations r
    WHERE p.route_relation_id = r.id;
    """)
  end
end
