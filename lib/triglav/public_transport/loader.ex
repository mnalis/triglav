defmodule Triglav.PublicTransport.Loader do
  @mode if Mix.env() == :prod, do: :auto, else: :manual

  def child_spec(_arg) do
    Periodic.child_spec(
      id: __MODULE__,
      mode: @mode,
      run: &Triglav.PublicTransport.update_all/0,
      every: :timer.minutes(1),
      when: &run?/0
    )
  end

  defp run?() do
    dttm = DateTime.now!("Europe/Zagreb")
    dttm.hour == 5 and dttm.minute == 0
  end
end
