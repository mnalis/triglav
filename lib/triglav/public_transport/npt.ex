defmodule Triglav.PublicTransport.Npt do
  @moduledoc """
  Download data from NPT (https://www.promet-info.hr/).
  """
  use Tesla

  plug Tesla.Middleware.BasicAuth,
    username: Application.get_env(:triglav, __MODULE__) |> Keyword.fetch!(:username),
    password: Application.get_env(:triglav, __MODULE__) |> Keyword.fetch!(:password)

  plug Tesla.Middleware.Timeout, timeout: 10_000
  plug Triglav.Tesla.Middleware.ErrrorOnStatus
  plug Triglav.Tesla.Middleware.Logger

  def download!(url, target) do
    response = get!(url)
    File.write!(target, response.body)
  end
end
