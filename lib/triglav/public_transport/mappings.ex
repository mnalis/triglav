defmodule Triglav.PublicTransport.Mappings do
  alias Triglav.PublicTransport

  alias Triglav.PublicTransport.Schemas.{
    Feed,
    Operator,
    PlatformMapping,
    Route,
    RouteVariant,
    RouteVariantPlatform
  }

  alias Triglav.Repo
  alias Triglav.Osm
  alias Triglav.Schemas.Osmosis.{Node, Way, Relation}

  import Ecto.Query
  import Triglav.Query

  require Logger

  def regenerate(feed_id) do
    delete(feed_id)
    generate(feed_id)
  end

  def generate(feed_id) do
    feed = Repo.get!(Feed, feed_id) |> Repo.preload(:operator)

    Logger.info("#{feed.operator.ref} #{feed.ref}: Generating mappings...")

    generate_platform_mappings(feed)
    generate_route_mappings(feed)
    generate_route_variant_mappings(feed)

    Logger.info("#{feed.operator.ref} #{feed.ref}: Mappings done")

    :ok
  end

  def delete(feed_id) do
    {count, nil} = Repo.delete_all(where(PlatformMapping, feed_id: ^feed_id))
    Logger.debug("Deleted #{count} pt_platform_mappings")

    {count, nil} = Repo.delete_all(where("pt_route_mapped_route_relations", feed_id: ^feed_id))
    Logger.debug("Deleted #{count} pt_route_mapped_route_relations")

    {count, nil} = Repo.delete_all(where("pt_route_mapped_route_master_relations", feed_id: ^feed_id))
    Logger.debug("Deleted #{count} pt_route_mapped_route_master_relations")

    {count, nil} = Repo.update_all(where(RouteVariant, feed_id: ^feed_id), set: [mapped_relation_id: nil])
    Logger.debug("Deleted #{count} pt_route_variants")
  end

  # Match imported PublicTransport platforms to platforms in OSM.
  defp generate_platform_mappings(feed) do
    pt_platforms =
      from(rvp in RouteVariantPlatform,
        where: [feed_id: ^feed.id],
        join: platform in assoc(rvp, :platform),
        select: platform,
        distinct: platform
      )
      |> Repo.all()

    # TODO: Add 'operator' tag to stops, then match by it rather than source?
    tags = %{
      "public_transport" => "platform",
      "source" => feed.operator.name
    }

    osm_platforms_map =
      Stream.concat(
        Osm.list_nodes(tags),
        Osm.list_ways(tags)
      )
      |> Enum.map(fn platform ->
        # Platforms can have multiple GTFS stop ids, separated by semicolons
        stop_id = platform.tags["gtfs:stop_id"]

        stop_ids =
          if stop_id != "" and stop_id != nil,
            do: String.split(stop_id, ";"),
            else: []

        for stop_id <- stop_ids do
          {stop_id, platform}
        end
      end)
      |> List.flatten()
      |> Enum.group_by(&elem(&1, 0), &elem(&1, 1))

    platform_mappings =
      for pt_platform <- pt_platforms do
        for osm_platform <- Map.get(osm_platforms_map, pt_platform.ref, []) do
          node_id = if match?(%Node{}, osm_platform), do: osm_platform.id
          way_id = if match?(%Way{}, osm_platform), do: osm_platform.id

          %{
            feed_id: feed.id,
            platform_id: pt_platform.id,
            node_id: node_id,
            way_id: way_id
          }
        end
      end
      |> List.flatten()

    Repo.delete_all(where(PlatformMapping, feed_id: ^feed.id))
    {count, nil} = Repo.insert_all(PlatformMapping, platform_mappings)
    add_computed_data(feed.id)
    Logger.debug("#{feed.operator.ref} #{feed.ref}: Created #{count} platform mappings")
  end

  defp add_computed_data(feed_id) do
    # Distance from osm platform to pt platform for nodes
    Repo.query!(
      """
        UPDATE pt_platform_mappings AS m
        SET distance = st_distance(p.geometry::geography, n.geom::geography)
        FROM pt_platforms AS p, osmosis.nodes AS n
        WHERE p.id = m.platform_id AND n.id = m.node_id AND m.feed_id = $1
      """,
      [feed_id]
    )

    # Distance from osm platform to pt platform for ways
    Repo.query!(
      """
        UPDATE pt_platform_mappings AS m
        SET distance = st_distance(p.geometry::geography, w.linestring::geography)
        FROM pt_platforms AS p, osmosis.ways AS w
        WHERE p.id = m.platform_id AND w.id = m.way_id AND m.feed_id = $1
      """,
      [feed_id]
    )
  end

  defp generate_route_mappings(feed) do
    mappings =
      from(route in Route,
        where: route.feed_id == ^feed.id,
        join: feed in Feed,
        on: feed.id == route.feed_id,
        join: operator in Operator,
        on: operator.id == feed.operator_id,
        join: relation in Relation,
        # TODO: match also by operator name? lowercase things?
        on:
          tag(relation, "operator") == operator.ref and
            lower(tag(relation, "ref")) == lower(route.ref) and
            tag(relation, "type") in ["route", "route_master"],
        select: %{
          feed_id: feed.id,
          route_id: route.id,
          relation_id: relation.id,
          type: tag(relation, "type")
        }
      )
      |> Repo.all()

    routes =
      mappings
      |> Enum.filter(&(&1.type == "route"))
      |> Enum.map(&Map.drop(&1, [:type]))

    route_masters =
      mappings
      |> Enum.filter(&(&1.type == "route_master"))
      |> Enum.map(&Map.drop(&1, [:type]))

    Repo.delete_all(where("pt_route_mapped_route_relations", feed_id: ^feed.id))
    Repo.delete_all(where("pt_route_mapped_route_master_relations", feed_id: ^feed.id))

    {c1, nil} = Repo.insert_all("pt_route_mapped_route_relations", routes)
    {c2, nil} = Repo.insert_all("pt_route_mapped_route_master_relations", route_masters)

    Logger.debug("#{feed.operator.ref} #{feed.ref}: Created #{c1} route mappings")
    Logger.debug("#{feed.operator.ref} #{feed.ref}: Created #{c2} route master mappings")
  end

  defp generate_route_variant_mappings(feed) do
    Repo.update_all(where(RouteVariant, feed_id: ^feed.id), set: [mapped_relation_id: nil])

    mappings = match_route_variants_to_osm_relations(feed.id)
    relation_ids = Map.values(mappings) |> Enum.concat()

    relations_map =
      from(r in Relation, where: r.id in ^relation_ids)
      |> Repo.all()
      |> Triglav.Osm.preload_members(&(&1.member_type == "W"))
      |> Map.new(&{&1.id, &1})

    feed.id
    |> match_route_variants_to_osm_relations()
    |> Enum.each(fn {route_variant_id, relation_ids} ->
      if length(relation_ids) > 1,
        do:
          Logger.warning(
            "#{feed.operator.ref} #{feed.ref}: Route variant #{route_variant_id} matches multiple OSM relations: #{inspect(relation_ids)}"
          )

      relation_id = List.first(relation_ids)
      relation = Map.fetch!(relations_map, relation_id)
      geometry = PublicTransport.route_relation_geometry(relation)

      Repo.update_all(
        where(RouteVariant, id: ^route_variant_id),
        set: [mapped_relation_id: relation_id, geometry: geometry]
      )
    end)

    Logger.debug("#{feed.operator.ref} #{feed.ref}: Created #{map_size(mappings)} route variant mappings")
  end

  @doc """
  For each RouteVariant finds Relations which have the same route ref and stop
  at the same stops.

  In theory it's possible that mutliple relations match a route variant, but
  it's unlikely and would probably indicate a mistake in mapping. This should be
  noted as an error.
  """
  @spec match_route_variants_to_osm_relations(Feed.id()) :: %{RouteVariant.id() => [Relation.id()]}
  def match_route_variants_to_osm_relations(feed_id) do
    route_variants =
      Repo.select!(
        """
          SELECT rv.id AS route_variant_id,
                 r.id AS route_id,
                 ARRAY_AGG(p.ref ORDER BY rvp.sequence_no) AS stop_ids
          FROM pt_route_variant_platforms rvp
          JOIN pt_platforms p ON p.id = rvp.platform_id
          JOIN pt_route_variants rv ON rv.id = rvp.route_variant_id
          JOIN pt_routes r ON r.id = rv.route_id
          WHERE rvp.feed_id = $1
          GROUP BY 1, 2;
        """,
        [feed_id]
      )

    osm_relations =
      Repo.select!(
        """
          SELECT relation.id AS relation_id,
                 route_id,
                 ARRAY_AGG(coalesce(node.tags->'gtfs:stop_id', way.tags->'gtfs:stop_id') ORDER BY member.sequence_id) AS stop_ids
          FROM pt_route_mapped_route_relations rmrr
          JOIN osmosis.relations relation ON relation.id = rmrr.relation_id
          JOIN osmosis.relation_members member ON member.relation_id = relation.id
          LEFT join osmosis.nodes node ON node.id = member.member_id and member.member_type = 'N'
          LEFT join osmosis.ways way ON way.id = member.member_id and member.member_type = 'W'
          WHERE rmrr.feed_id = $1 and member.member_role like 'platform%'
          GROUP BY 1, 2;
        """,
        [feed_id]
      )

    # Each OSM platform maps onto one or more GTFS stops by GTFS ID. When a
    # platform matches multiple GTFS IDs, they are stored in the `gtfs:stop_id`
    # tag delimited by semicolons.
    osm_relations_by_route_id =
      osm_relations
      |> Enum.map(fn row ->
        Map.update!(row, "stop_ids", fn ids ->
          Enum.map(ids, &if(&1, do: String.split(&1, ";"), else: []))
        end)
      end)
      |> Enum.group_by(& &1["route_id"])

    route_variants
    |> Enum.map(fn variant ->
      matched_relation_ids =
        osm_relations_by_route_id
        |> Map.get(variant["route_id"], [])
        |> Enum.filter(fn relation -> matches?(variant["stop_ids"], relation["stop_ids"]) end)
        |> Enum.map(& &1["relation_id"])

      {variant["route_variant_id"], matched_relation_ids}
    end)
    |> Enum.filter(fn {_, ids} -> length(ids) > 0 end)
    |> Map.new()
  end

  @spec matches?([String.t()], [[String.t()]]) :: boolean()
  defp matches?(gtfs_stop_ids, osm_stop_ids) do
    length(gtfs_stop_ids) == length(osm_stop_ids) and
      gtfs_stop_ids
      |> Enum.zip(osm_stop_ids)
      |> Enum.all?(fn {id, ids} -> Enum.member?(ids, id) end)
  end
end
