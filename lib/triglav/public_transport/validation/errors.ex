defmodule Triglav.PublicTransport.Validation.Errors do
  alias Triglav.PublicTransport.Schemas.Error
  alias Triglav.PublicTransport.Schemas.{Feed, Route, RouteRelationPlatform}
  alias Triglav.Schemas.Osmosis.{Relation, RelationMember, Way}

  import Phoenix.LiveView.Helpers

  @spec no_relations(Feed.t(), Route.t()) :: Error.t()
  def no_relations(feed, route) do
    %Error{
      key: "no_relations",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref
    }
  end

  @spec missing_route_master(Feed.t(), Route.t()) :: Error.t()
  def missing_route_master(feed, route) do
    %Error{
      key: "missing_route_master",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref
    }
  end

  @spec multiple_route_masters(Feed.t(), Route.t()) :: Error.t()
  def multiple_route_masters(feed, route) do
    %Error{
      key: "multiple_route_masters",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref
    }
  end

  @spec relation_not_contained_in_route_master(Feed.t(), Route.t(), Relation.t()) :: Error.t()
  def relation_not_contained_in_route_master(feed, route, relation) do
    %Error{
      key: "relation_not_contained_in_route_master",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref,
      relation_id: relation.id
    }
  end

  @spec unexpected_route_master_member(Feed.t(), Route.t(), Relation.t(), String.t(), integer()) :: Error.t()
  def unexpected_route_master_member(feed, route, relation, member_type, member_id) do
    %Error{
      key: "unexpected_route_master_member",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref,
      relation_id: relation.id,
      params: %{
        member_type: member_type,
        member_id: member_id
      }
    }
  end

  @spec relation_missing_required_tags(Feed.t(), Route.t(), Relation.t(), [String.t()]) :: Error.t()
  def relation_missing_required_tags(feed, route, relation, tags) do
    %Error{
      key: "relation_missing_required_tags",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref,
      relation_id: relation.id,
      params: %{tags: tags}
    }
  end

  @spec relation_contains_unexpected_tags(Feed.t(), Route.t(), Relation.t(), [String.t()]) :: Error.t()
  def relation_contains_unexpected_tags(feed, route, relation, tags) do
    %Error{
      key: "relation_contains_unexpected_tags",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref,
      relation_id: relation.id,
      params: %{tags: tags}
    }
  end

  @spec relation_missing_gtfs_stop_ids(Feed.t(), Route.t(), Relation.t(), integer()) :: Error.t()
  def relation_missing_gtfs_stop_ids(feed, route, relation, count) do
    %Error{
      key: "relation_missing_gtfs_stop_ids",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref,
      relation_id: relation.id,
      params: %{count: count}
    }
  end

  @spec relation_invalid_tag_value(Feed.t(), Route.t(), Relation.t(), String.t(), String.t(), String.t()) :: Error.t()
  def relation_invalid_tag_value(feed, route, relation, name, actual, expected) do
    %Error{
      key: "relation_invalid_tag_value",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref,
      relation_id: relation.id,
      params: %{name: name, expected: expected, actual: actual}
    }
  end

  @spec relation_not_updated_to_ptv2(Feed.t(), Route.t(), Relation.t()) :: Error.t()
  def relation_not_updated_to_ptv2(feed, route, relation) do
    %Error{
      key: "relation_not_updated_to_ptv2",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref,
      relation_id: relation.id
    }
  end

  @spec invalid_relation_name(Feed.t(), Route.t(), Relation.t(), String.t(), String.t()) :: Error.t()
  def invalid_relation_name(feed, route, relation, actual, expected) do
    %Error{
      key: "invalid_relation_name",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref,
      relation_id: relation.id,
      params: %{expected: expected, actual: actual}
    }
  end

  @spec broken_route(Feed.t(), Route.t(), Relation.t()) :: Error.t()
  def broken_route(feed, route, relation) do
    %Error{
      key: "broken_route",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref,
      relation_id: relation.id
    }
  end

  @spec wrong_way(Feed.t(), Route.t(), Relation.t(), Way.t(), pos_integer()) :: Error.t()
  def wrong_way(feed, route, relation, way, sequence_id) do
    count = length(way.linestring.coordinates)
    way_name = Map.get(way.tags, "name", "highway")
    name = "#{way_name} (#{count} nodes)"

    %Error{
      key: "wrong_way",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref,
      relation_id: relation.id,
      params: %{name: name, sequence_id: sequence_id, way_id: way.id}
    }
  end

  @spec unexpected_member_role(Feed.t(), Route.t(), Relation.t(), RelationMember.t()) :: Error.t()
  def unexpected_member_role(feed, route, relation, member) do
    %Error{
      key: "unexpected_member_role",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref,
      relation_id: relation.id,
      params: Map.take(member, [:member_id, :member_role, :member_type])
    }
  end

  @spec platform_invalid_tag_value(Feed.t(), Route.t(), Relation.t(), integer(), String.t(), String.t(), String.t()) ::
          Error.t()
  def platform_invalid_tag_value(feed, route, relation, sequence_id, name, actual, expected) do
    %Error{
      key: "platform_invalid_tag_value",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref,
      relation_id: relation.id,
      sequence_id: sequence_id,
      params: %{name: name, expected: expected, actual: actual}
    }
  end

  @spec platform_too_far_from_route(Feed.t(), Route.t(), Relation.t(), RouteRelationPlatform.t()) :: Error.t()
  def platform_too_far_from_route(feed, route, relation, platform) do
    %Error{
      key: "platform_too_far_from_route",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref,
      relation_id: relation.id,
      sequence_id: platform.sequence_id,
      params: %{
        distance: platform.distance_from_route,
        node_id: platform.node_id,
        way_id: platform.way_id
      }
    }
  end

  @spec terminus_too_far_from_route_end(
          Feed.t(),
          Route.t(),
          Relation.t(),
          RouteRelationPlatform.t(),
          float(),
          boolean()
        ) ::
          Error.t()
  def terminus_too_far_from_route_end(feed, route, relation, platform, distance, is_start) do
    %Error{
      key: "terminus_too_far_from_route_end",
      operator_id: feed.operator_id,
      feed_id: feed.id,
      route_ref: route.ref,
      relation_id: relation.id,
      sequence_id: platform.sequence_id,
      params: %{
        distance: distance,
        is_start: is_start
      }
    }
  end

  @spec render(Error.t()) :: Phoenix.LiveView.Rendered.t()
  def render(%{key: "missing_route_master"}) do
    assigns = %{}
    ~H"Missing route master relation"
  end

  def render(%{key: "multiple_route_masters"}) do
    assigns = %{}
    ~H"Multiple route master relations"
  end

  def render(%{key: "no_relations"}) do
    assigns = %{}
    ~H"No OSM relations found"
  end

  def render(%{
        key: "unexpected_route_master_member",
        params: %{"member_type" => member_type, "member_id" => member_id}
      }) do
    assigns = %{member_type: member_type, member_id: member_id}
    ~H"Route master relation contains unexpected member <%= @member_type %> <%= @member_id %>"
  end

  def render(%{key: "relation_missing_required_tags", params: %{"tags" => tags}}) do
    assigns = %{tags: Enum.join(tags, ", ")}
    ~H"Missing required tags: <%= @tags %>"
  end

  def render(%{key: "relation_contains_unexpected_tags", params: %{"tags" => tags}}) do
    assigns = %{tags: Enum.join(tags, ", ")}
    ~H"Contains unexpected tags: <%= @tags %>"
  end

  def render(%{
        key: "relation_invalid_tag_value",
        params: %{"name" => name, "expected" => expected, "actual" => actual}
      }) do
    assigns = %{name: name, expected: expected, actual: actual}

    if actual do
      ~H"Invalid tag [<%= @name %>=<%= @actual %>], expected [<%= @name %>=<%= @expected %>]"
    else
      ~H"Missing tag [<%= @name %>=<%= @expected %>]"
    end
  end

  def render(%{key: "relation_not_updated_to_ptv2"}) do
    assigns = %{}
    ~H"Relation not updated to [public_transport:version=2]"
  end

  def render(%{key: "invalid_relation_name", params: %{"expected" => expected, "actual" => actual}}) do
    assigns = %{expected: expected, actual: actual}
    ~H"Expected name based on tags: '<%= @expected %>', actual name: '<%= @actual %>"
  end

  def render(%{key: "broken_route"}) do
    assigns = %{}
    ~H"Route broken, segments are not contiguous"
  end

  def render(%{key: "relation_missing_gtfs_stop_ids", params: %{"count" => count}}) do
    assigns = %{count: count}
    ~H"Missing gtfs:stop_id tags on #{count} platforms"
  end

  def render(%{
        key: "unexpected_member_role",
        params: %{"member_role" => member_role, "member_type" => member_type, "member_id" => member_id}
      }) do
    assigns = %{member_role: member_role, member_type: member_type, member_id: member_id}
    ~H'Unexpected role "<%= @member_role %>" on relation member <%= @member_type %><%= @member_id %>'
  end

  def render(%{key: "relation_not_contained_in_route_master"}) do
    assigns = %{}
    ~H"Route relation not contained in route master"
  end

  def render(%{key: "wrong_way", params: %{"sequence_id" => sequence_id, "way_id" => way_id, "name" => name}}) do
    assigns = %{
      name: name,
      href: "http://127.0.0.1:8111/load_object?objects=w#{way_id}",
      way_id: way_id,
      sequence_id: sequence_id
    }

    ~H"""
    Route member #<%= sequence_id %>: one-way way #<%= way_id %> "<%= name %>" traveresed in wrong direction.
    <a class="josm-remote" href={@href} target="josm">Load</a>
    """
  end

  def render(%{
        key: "platform_too_far_from_route",
        sequence_id: sequence_id,
        params: %{"way_id" => way_id, "node_id" => node_id, "distance" => distance}
      }) do
    object = if way_id, do: "w#{way_id}", else: "n#{node_id}"

    assigns = %{
      platform: sequence_id + 1,
      distance: round(distance),
      href: "http://127.0.0.1:8111/load_object?objects=#{object}"
    }

    ~H"""
    Platform <%= @platform %> is <%= @distance %>m from route.
    <a class="josm-remote" href={@href} target="josm">Load</a>
    """
  end

  def render(%{
        key: "terminus_too_far_from_route_end",
        params: %{"distance" => distance, "is_start" => is_start}
      }) do
    assigns = %{distance: round(distance)}

    if is_start do
      ~H"First platform is <%= @distance %>m from route start."
    else
      ~H"Last platform is <%= @distance %>m from route end."
    end
  end

  def render(%{key: key, params: params}) do
    assigns = %{key: key, params: inspect(params)}
    ~H"MISSING ERROR CAPTION: <%= @key %> <%= @params %>)}"
  end
end
