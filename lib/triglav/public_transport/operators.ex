defmodule Triglav.PublicTransport.Operators do
  alias Triglav.PublicTransport.Schemas.Operator
  alias Triglav.Repo

  import Ecto.Query

  def all() do
    [
      autotrolej(),
      gpp_osijek(),
      gp_sibenik(),
      hzpp(),
      jadrolinija(),
      pulapromet(),
      zet()
    ]
  end

  def autotrolej() do
    get_or_insert!(%Operator{
      name: "Autotrolej",
      ref: "Autotrolej",
      slug: "autotrolej",
      wikidata_id: "Q12627396"
    })
  end

  def gpp_osijek() do
    get_or_insert!(%Operator{
      name: "Gradski Prijevoz Putnika Osijek",
      ref: "GPP Osijek",
      slug: "gpp_osijek",
      wikidata_id: "Q13056097"
    })
  end

  def gp_sibenik() do
    get_or_insert!(%Operator{
      name: "Gradski parking, Šibenik",
      ref: "Gradski parking",
      slug: "gp_sibenik",
      wikidata_id: "Q12627396"
    })
  end

  def hzpp() do
    get_or_insert!(%Operator{
      name: "HŽ Putnički prijevoz",
      ref: "HŽPP",
      slug: "hzpp",
      wikidata_id: "Q931988"
    })
  end

  def jadrolinija() do
    get_or_insert!(%Operator{
      name: "Jadrolinija",
      ref: "Jadrolinija",
      slug: "jadrolinija",
      wikidata_id: "Q652564"
    })
  end

  def pulapromet() do
    get_or_insert!(%Operator{
      name: "Pulapromet",
      ref: "Pulapromet",
      slug: "pulapromet",
      wikidata_id: "Q12640747"
    })
  end

  def zet() do
    get_or_insert!(%Operator{
      name: "Zagrebački električni tramvaj",
      ref: "ZET",
      slug: "zet",
      wikidata_id: "Q136060"
    })
  end

  defp get_or_insert!(operator) do
    Repo.insert!(operator, on_conflict: :nothing, conflict_target: :slug)
    Repo.one!(where(Operator, slug: ^operator.slug))
  end
end
