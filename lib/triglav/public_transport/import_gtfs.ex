defmodule Triglav.PublicTransport.ImportGtfs do
  @moduledoc """
  Importdata from GTFS files into `Triglav.PublicTransport` schemas.

  Sources:
    - ZET - https://zet.hr/gtfs2
    - HŽ - http://www.hzpp.hr/Media/Default/GTFS/GTFS_files.zip
  """
  alias NimbleCSV.RFC4180, as: CsvParser
  alias Triglav.Gtfs
  alias Triglav.Helpers.{Counter, FileUtils}
  alias Triglav.Http
  alias Triglav.PublicTransport
  alias Triglav.PublicTransport.Npt
  alias Triglav.PublicTransport.Operators
  alias Triglav.PublicTransport.Schemas.{Feed, FileStats, Operator, Platform, Route, RouteVariant}
  alias Triglav.Repo

  import Ecto.Query

  require Logger

  def import_zet(opts \\ []) do
    operator = Operators.zet()
    url = "https://zet.hr/gtfs-scheduled/latest"
    import_from_url(operator, url, opts)
  end

  def import_hzpp(opts \\ []) do
    operator = Operators.hzpp()
    url = "https://www.hzpp.hr/Media/Default/GTFS/GTFS_files.zip"
    import_from_url(operator, url, opts)
  end

  def import_gp_sibenik(opts \\ []) do
    operator = Operators.gp_sibenik()
    url = "https://www.gradski-parking.hr/upload/stranice/2022/08/2022-08-30/89/gtfs.zip"
    import_from_url(operator, url, opts)
  end

  def import_gpp_osijek(opts \\ []) do
    operator = Operators.gpp_osijek()
    url = "https://b2b.promet-info.hr/dc/b2b.gtfs.osijekgpp"
    target = Triglav.tmp_dir!() <> "/#{operator.slug}.zip"
    Npt.download!(url, target)
    import_from_archive(operator, target, opts)
  end

  def import_jadrolinija(opts \\ []) do
    operator = Operators.jadrolinija()
    url = "https://b2b.promet-info.hr/dc/b2b.gtfs.jl"
    target = Triglav.tmp_dir!() <> "/#{operator.slug}.zip"
    Npt.download!(url, target)
    import_from_archive(operator, target, opts)
  end

  def import_pulapromet(opts \\ []) do
    operator = Operators.pulapromet()
    url = "https://b2b.promet-info.hr/dc/b2b.gtfs.pulapromet"
    target = Triglav.tmp_dir!() <> "/#{operator.slug}.zip"
    Npt.download!(url, target)
    import_from_archive(operator, target, opts)
  end

  defp import_from_url(operator, url, opts) do
    target = Triglav.tmp_dir!()
    path = Http.download!(url, target, overwrite: true, filename: "#{operator.slug}.zip")
    import_from_archive(operator, path, opts)
  end

  defp import_from_archive(operator, path, opts) do
    files = Gtfs.extract_archive(path)

    # Unique feed ref is calculated as a hash of loaded data.
    feed_ref = FileUtils.md5sum!(path)

    import_gtfs(files, operator, feed_ref, opts)
  end

  @spec import_gtfs(Gtfs.file_paths(), Operator.t(), String.t(), force: boolean()) ::
          {:ok, {Feed.t(), created :: boolean()}}
  defp import_gtfs(files, operator, feed_ref, opts) do
    existing_feed = Repo.get_by(Feed, ref: feed_ref)
    exists? = not is_nil(existing_feed)
    force? = Keyword.get(opts, :force, false)

    # TODO: think about doing the parsing outside the transaction to avoid
    # locking the database for longer than required.
    Repo.transact(
      fn ->
        if force? or not exists? do
          if exists? do
            Logger.info("Deleting feed ##{existing_feed.ref}")
            PublicTransport.delete_feed(existing_feed.id)
          end

          forced_label = if force?, do: "(forced)"
          Logger.info("Updating #{operator.ref} #{forced_label}")

          # Mark existing feeds as inactive, as this will be the new active feed
          where(Feed, operator_id: ^operator.id, active: true)
          |> Repo.update_all(set: [active: false])

          feed = import_feed(operator, files, feed_ref)
          import_platforms(feed, files)
          import_routes(feed, operator, files)
          import_route_variants(feed, files)
          add_platform_use_count(feed.id)

          Logger.info("#{operator.ref} updated to ##{feed.ref}")
          {:ok, {feed, true}}
        else
          {:ok, {existing_feed, false}}
        end
      end,
      # This can be slow so increase default timeout
      timeout: :timer.minutes(5)
    )
  end

  defp import_feed(operator, files, feed_ref) do
    now = DateTime.utc_now() |> DateTime.truncate(:second)
    {start_date, end_date} = get_feed_dates(files)

    Repo.insert!(%Feed{
      start_date: start_date,
      end_date: end_date,
      imported_at: now,
      operator_id: operator.id,
      ref: feed_ref,
      file_stats: file_stats(files),
      active: true
    })
  end

  defp file_stats(files) do
    Enum.map(files, fn {_slug, path} ->
      %FileStats{
        file: Path.basename(path),
        md5sum: FileUtils.md5sum!(path),
        line_count: FileUtils.wc_l!(path)
      }
    end)
  end

  defp import_routes(feed, operator, files) do
    routes =
      files.routes
      |> stream_csv()
      |> Enum.map(fn row ->
        %{
          feed_id: feed.id,
          name: row.route_long_name,
          ref: row.route_id,
          route_type: Gtfs.parse_route_type(row.route_type),
          network: operator.name
        }
      end)
      |> sort_routes(operator.slug)
      |> Enum.with_index()
      |> Enum.map(fn {route, index} -> Map.put(route, :order_no, index) end)

    {count, _} = Repo.insert_all(Route, routes)
    Logger.info("Inserted #{count} routes")
  end

  defp sort_routes(routes, "zet") do
    # Trams before buses, then by ref as number
    Enum.sort_by(routes, fn route ->
      type_order = if route.route_type == :tram, do: 1, else: 2
      ref_order = Integer.parse(route.ref)
      {type_order, ref_order}
    end)
  end

  defp sort_routes(routes, _) do
    Enum.sort_by(routes, & &1.ref)
  end

  defp import_route_variants(feed, files) do
    trips = :ets.new(:trips, [])
    departure_counter = Counter.new()

    # Map route ref to database ID
    routes =
      from(route in Route,
        where: [feed_id: ^feed.id],
        select: {route.ref, route.id}
      )
      |> Repo.all()
      |> Map.new()

    # Map plarform ref to database ID
    platforms =
      from(platform in Platform,
        where: [feed_id: ^feed.id],
        select: {platform.ref, platform.id}
      )
      |> Repo.all()
      |> Map.new()

    Logger.info("Crunching trips...")

    # Map trip ID to route ID using ETS
    {stream, columns} = stream_csv_fast(files.trips)

    Enum.each(stream, fn row ->
      :ets.insert(trips, {
        Enum.at(row, columns.trip_id),
        Enum.at(row, columns.route_id),
        Enum.at(row, columns.direction_id)
      })
    end)

    Logger.info("Crunching routes...")

    {stream, columns} = stream_csv_fast(files.stop_times)

    # This bit takes a long time. e.g. for ZET, 200k lines ~35s
    # Assumes that file is sorted by trip_id, will break if it is not.
    variants =
      stream
      |> Stream.chunk_by(fn row -> Enum.at(row, columns.trip_id) end)
      |> Stream.map(fn chunk ->
        trip_id = chunk |> List.first() |> Enum.at(columns.trip_id)
        [{_, route_id, direction}] = :ets.lookup(trips, trip_id)

        stops =
          chunk
          |> Enum.sort_by(&to_integer(Enum.at(&1, columns.stop_sequence)))
          |> Enum.map(&Enum.at(&1, columns.stop_id))

        # Delete trip from trips table to mark it as processed
        # This will make the import break if the file isn't sorted by trip_id
        true = :ets.delete(trips, trip_id)

        # Count departures along this route
        Counter.increment(departure_counter, {route_id, stops})

        {trip_id, route_id, stops, direction}
      end)
      |> Enum.uniq_by(fn {_trip_id, route_id, stops, _direction} -> {route_id, stops} end)

    route_variants =
      Enum.map(variants, fn {trip_id, route_id, stops, direction} ->
        departure_count = Counter.get(departure_counter, {route_id, stops})

        direction =
          case direction do
            "0" -> "A"
            "1" -> "B"
            # TODO: make nullable?
            "" -> ""
          end

        %{
          feed_id: feed.id,
          ref: trip_id,
          route_id: Map.fetch!(routes, route_id),
          departure_count: departure_count,
          direction: direction,
          stop_ids: stops
        }
      end)

    {count, _} = Repo.insert_all(RouteVariant, route_variants)
    Logger.info("Inserted #{count} variants")

    # Map route variant ref to database ID
    variants_map =
      from(variant in RouteVariant,
        where: [feed_id: ^feed.id],
        select: {variant.ref, variant.id}
      )
      |> Repo.all()
      |> Map.new()

    route_platforms =
      Enum.map(variants, fn {trip_id, _route_id, stops, _direction} ->
        stops
        |> Enum.with_index()
        |> Enum.map(fn {stop_id, index} ->
          %{
            feed_id: feed.id,
            route_variant_id: variants_map[trip_id],
            platform_id: platforms[stop_id],
            sequence_no: index + 1
          }
        end)
      end)
      |> List.flatten()

    {count, _} = Repo.insert_all("pt_route_variant_platforms", route_platforms)
    Logger.info("Inserted #{count} route platforms")

    :ets.delete(trips)
    Counter.delete(departure_counter)
  end

  defp import_platforms(feed, files) do
    platforms =
      files.stops
      |> stream_csv()
      |> Enum.map(fn row ->
        coordinates = {to_float(row.stop_lon), to_float(row.stop_lat)}

        %{
          feed_id: feed.id,
          name: row.stop_name,
          ref: row.stop_id,
          geometry: %Geo.Point{coordinates: coordinates, srid: 4326}
        }
      end)

    {count, _} = Repo.insert_all(Platform, platforms)
    Logger.info("Inserted #{count} platforms")
  end

  defp add_platform_use_count(feed_id) do
    Repo.query!(
      """
      UPDATE pt_platforms AS p
      SET use_count = tmp.count
      FROM (
        SELECT platform_id, count(*) AS count
        FROM pt_route_variant_platforms
        WHERE feed_id = $1
        GROUP BY 1
      ) AS tmp
      WHERE tmp.platform_id = p.id AND p.feed_id = $1;
      """,
      [feed_id]
    )
  end

  # Parse yyyymmdd formatted date
  defp parse_gtfs_date(date) do
    if date != "" do
      [String.slice(date, 0..3), String.slice(date, 4..5), String.slice(date, 6..7)]
      |> Enum.join("-")
      |> Date.from_iso8601!()
    end
  end

  defp get_feed_dates(files) do
    if Map.has_key?(files, :feed_info) do
      feed_info = parse_feed_info(files.feed_info)
      start_date = parse_gtfs_date(Map.get(feed_info, :feed_start_date, ""))
      end_date = parse_gtfs_date(Map.get(feed_info, :feed_end_date, ""))
      {start_date, end_date}
    else
      {nil, nil}
    end
  end

  defp parse_feed_info(path) do
    stream_csv(path) |> Enum.at(0)
  end

  defp stream_csv(path) do
    stream =
      path
      |> File.stream!()
      |> CsvParser.parse_stream(skip_headers: false)

    header = Enum.at(stream, 0)

    stream
    |> Stream.drop(1)
    |> Stream.map(&to_map(header, &1))
  end

  defp stream_csv_fast(path) do
    stream =
      path
      |> File.stream!()
      |> CsvParser.parse_stream(skip_headers: false)

    columns =
      stream
      |> Enum.at(0)
      |> Enum.with_index()
      |> Map.new(fn {name, index} -> {String.to_atom(name), index} end)

    {Stream.drop(stream, 1), columns}
  end

  # TODO: create structs for GTFS maps instead?
  defp to_map(header, values) do
    Enum.zip(header, values)
    |> Enum.map(fn {k, v} -> {String.to_atom(k), v} end)
    |> Map.new()
  end

  defp to_float(""), do: nil

  defp to_float(str) do
    {float, ""} = Float.parse(str)
    float
  end

  defp to_integer(str) do
    {int, ""} = Integer.parse(str)
    int
  end
end
