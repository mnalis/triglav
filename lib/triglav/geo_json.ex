defmodule Triglav.GeoJSON do
  alias Triglav.Schemas.Osmosis.{Way, Node}

  @type geometry :: %{
          coordinates: {number, number} | [{number, number}] | [[{number, number}]] | [[[{number, number}]]],
          properties: map()
        }

  @type feature :: %{
          type: String.t(),
          properties: map(),
          geometry: geometry()
        }

  @type feature_collection :: %{
          type: String.t(),
          features: [feature()]
        }

  def to_feature(object, properties \\ %{})

  def to_feature(%Way{} = way, properties) do
    properties = Map.merge(%{id: way.id, type: "W"}, properties)
    feature(geometry(way), properties)
  end

  def to_feature(%Node{} = node, properties) do
    properties = Map.merge(%{id: node.id, type: "N"}, properties)

    feature(geometry(node), properties)
  end

  defp geometry(%Node{} = node) do
    %{
      type: "Point",
      coordinates: Tuple.to_list(node.geom.coordinates)
    }
  end

  defp geometry(%Way{} = way) do
    %{
      type: "LineString",
      coordinates: Enum.map(way.linestring.coordinates, &Tuple.to_list/1)
    }
  end

  def feature(geometry, properties \\ %{}) do
    %{
      type: "Feature",
      properties: properties,
      geometry: geometry
    }
  end

  def feature_collection(features) do
    %{
      type: "FeatureCollection",
      features: features
    }
  end

  @spec nodes_to_linestring([Node.t()]) :: map()
  def nodes_to_linestring(nodes) do
    coordinates = Enum.map(nodes, &Tuple.to_list(&1.geom.coordinates))

    %{
      type: "LineString",
      coordinates: coordinates
    }
  end

  @spec ways_to_multilinestring([Way.t()]) :: map()
  def ways_to_multilinestring(ways) do
    %Geo.MultiLineString{
      coordinates: Enum.map(ways, & &1.linestring.coordinates),
      srid: List.first(ways).linestring.srid
    }
  end
end
