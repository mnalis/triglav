defmodule Triglav.Osm do
  alias Triglav.Repo
  alias Triglav.Schemas.Osmosis.Node
  alias Triglav.Schemas.Osmosis.Relation
  alias Triglav.Schemas.Osmosis.RelationMember
  alias Triglav.Schemas.Osmosis.Way

  import Ecto.Query

  @type tags :: Keyword.t() | map()

  @spec list_nodes(tags()) :: [Node.t()]
  def list_nodes(tags \\ []), do: filter_by_tags(Node, tags) |> Repo.all()

  @spec list_ways(tags()) :: [Way.t()]
  def list_ways(tags \\ []), do: filter_by_tags(Way, tags) |> Repo.all()

  @spec list_relations(tags()) :: [Relation.t()]
  def list_relations(tags \\ []), do: filter_by_tags(Relation, tags) |> Repo.all()

  @spec filter_by_tags(Ecto.Queryable.t(), tags()) :: Ecto.Query.t()
  def filter_by_tags(query, tags) do
    Enum.reduce(tags, query, fn {key, value}, query ->
      cond do
        is_binary(value) ->
          where(query, [], fragment("tags->?", ^to_string(key)) == ^value)

        is_list(value) ->
          where(query, [], fragment("tags->?", ^to_string(key)) in ^value)
      end
    end)
  end

  @spec preload_platform_members([Relation.t()]) :: [Relation.t()]
  def preload_platform_members(relations) do
    preload_members(relations, &String.starts_with?(&1.member_role, "platform"))
  end

  @spec preload_members([Relation.t()], (RelationMember.t() -> boolean())) :: [Relation.t()]
  def preload_members(relations, include? \\ fn _ -> true end) do
    relations = Repo.preload(relations, :members)

    ids_by_type =
      Enum.flat_map(relations, fn relation ->
        for member <- relation.members, include?.(member) do
          {member.member_type, member.member_id}
        end
      end)
      |> Enum.group_by(&elem(&1, 0), &elem(&1, 1))

    node_ids = ids_by_type |> Map.get("N", [])
    way_ids = ids_by_type |> Map.get("W", [])

    nodes = Repo.all(from(node in Node, where: node.id in ^node_ids)) |> Map.new(&{&1.id, &1})
    ways = Repo.all(from(way in Way, where: way.id in ^way_ids)) |> Map.new(&{&1.id, &1})

    Enum.map(relations, fn relation ->
      Map.update!(relation, :members, fn members ->
        Enum.map(members, fn member ->
          if include?.(member) do
            case member.member_type do
              "N" -> Map.put(member, :member, Map.fetch!(nodes, member.member_id))
              "W" -> Map.put(member, :member, Map.fetch!(ways, member.member_id))
            end
          else
            member
          end
        end)
      end)
    end)
  end
end
