# Add postgis extesion to postgrex
# https://hexdocs.pm/ecto_sql/Ecto.Adapters.Postgres.html#module-extensions
Postgrex.Types.define(
  Triglav.PostgrexTypes,
  [Geo.PostGIS.Extension] ++ Ecto.Adapters.Postgres.extensions(),
  json: Jason
)
