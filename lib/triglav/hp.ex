defmodule Triglav.HP do
  alias Triglav.HP.API
  alias Triglav.HP.Mappings
  alias Triglav.HP.Schemas.PostOffice
  alias Triglav.HP.Schemas.PostOfficeMapping
  alias Triglav.Repo

  import Ecto.Query

  @spec import() :: :ok
  def import() do
    post_offices = API.fetch_post_offices!()

    Repo.transact(fn ->
      Repo.delete_all(PostOfficeMapping)
      Repo.delete_all(PostOffice)
      Repo.insert_all(PostOffice, post_offices)
      Repo.insert_all(PostOfficeMapping, Mappings.generate())
      :ok
    end)
  end

  @spec list_post_office_mappings() :: [PostOfficeMapping.t()]
  def list_post_office_mappings() do
    from(p in PostOfficeMapping)
    |> preload([:post_office, :way, :node])
    |> Repo.all()
  end
end
