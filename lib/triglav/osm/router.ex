defmodule Triglav.Osm.Router do
  alias Geo.LineString
  alias Triglav.Schemas.Osmosis.{Node, Way}

  defmodule AnnotatedWay do
    @type t :: %__MODULE__{
            way: Way.t(),
            direction: :forward | :backward,
            node_ids: [Node.id()],
            linestring: Geo.LineString.t()
          }

    defstruct [:way, :direction, :node_ids, :linestring]

    def forward(way) do
      %AnnotatedWay{
        way: way,
        direction: :forward,
        node_ids: way.node_ids,
        linestring: way.linestring
      }
    end

    def backward(way) do
      %AnnotatedWay{
        way: way,
        direction: :backward,
        node_ids: Enum.reverse(way.node_ids),
        linestring: Map.update!(way.linestring, :coordinates, &Enum.reverse/1)
      }
    end

    def custom(way, node_ids, linestring) do
      %AnnotatedWay{
        way: way,
        direction: :custom,
        node_ids: node_ids,
        linestring: linestring
      }
    end
  end

  @doc """
  Given a list of ordered, consecutive ways returns a LineString containing all
  points through which the ways pass. Fails if ways are not consecutive.
  """
  @spec route_ways([Way.t()]) :: {:ok, LineString.t()} | {:error, any()}
  def route_ways(ordered_ways) do
    with {:ok, annotated_ways} <- annotate_ways(ordered_ways) do
      coordinates =
        annotated_ways
        |> Enum.map(& &1.linestring.coordinates)
        |> Enum.concat()
        |> Enum.dedup()

      {:ok, %Geo.LineString{coordinates: coordinates, srid: 4326}}
    end
  end

  @spec annotate_ways([Way.t()]) :: {:ok, [AnnotatedWay.t()]} | {:error, term()}

  # If the relation contains only one way, this is not routable
  def annotate_ways([_]), do: {:error, :cannot_route_single_way}

  def annotate_ways(ways), do: annotate_ways(ways, [])

  def annotate_ways([], annotated_ways), do: {:ok, Enum.reverse(annotated_ways)}

  # For the first way in relation, check out the second way to determine direction
  def annotate_ways(ways, []) do
    [w1, w2 | _] = ways

    cond do
      List.last(w1.node_ids) in w2.node_ids ->
        annotate_ways(tl(ways), [AnnotatedWay.forward(w1)])

      List.first(w1.node_ids) in w2.node_ids ->
        annotate_ways(tl(ways), [AnnotatedWay.backward(w1)])

      true ->
        {:error, {:ways_not_connected, w1, w2}}
    end
  end

  def annotate_ways([w2 | ways], [annotated_w1 | _] = annotated_ways) do
    if Way.is_whole_roundabout?(w2) do
      with {:ok, node_ids, linestring} <- get_roundabout_segment(w2, annotated_w1, hd(ways)) do
        annotate_ways(ways, [AnnotatedWay.custom(w2, node_ids, linestring) | annotated_ways])
      end
    else
      common_node_id = List.last(annotated_w1.node_ids)

      cond do
        common_node_id == List.first(w2.node_ids) ->
          annotate_ways(ways, [AnnotatedWay.forward(w2) | annotated_ways])

        common_node_id == List.last(w2.node_ids) ->
          annotate_ways(ways, [AnnotatedWay.backward(w2) | annotated_ways])

        true ->
          {:error, {:ways_not_connected, annotated_w1.way, w2}}
      end
    end
  end

  # Given a way which is a full roundabout (circular, same first and last node)
  # returns a segment which connects the previous and next way.
  @spec get_roundabout_segment(Way.t(), AnnotatedWay.t(), Way.t()) ::
          {:ok, [integer()], LineString.t()} | {:error, any()}
  defp get_roundabout_segment(way, annotated_prev_way, next_way) do
    entry_node_id = List.last(annotated_prev_way.node_ids)

    first_node_id = List.first(next_way.node_ids)
    last_node_id = List.last(next_way.node_ids)

    exit_node_id =
      cond do
        first_node_id in way.node_ids -> first_node_id
        last_node_id in way.node_ids -> last_node_id
        true -> nil
      end

    cond do
      entry_node_id not in way.node_ids ->
        {:error, {:ways_not_connected, annotated_prev_way.way, way}}

      is_nil(exit_node_id) ->
        {:error, {:ways_not_connected, way, next_way}}

      true ->
        entry_index = Enum.find_index(way.node_ids, &(&1 == entry_node_id))
        exit_index = Enum.find_index(way.node_ids, &(&1 == exit_node_id))

        node_ids = slice_roundabout(way.node_ids, entry_index, exit_index)
        coordinates = slice_roundabout(way.linestring.coordinates, entry_index, exit_index)
        linestring = %Geo.LineString{coordinates: coordinates}

        {:ok, node_ids, linestring}
    end
  end

  defp slice_roundabout(items, start_index, end_index) do
    if start_index <= end_index do
      Enum.slice(items, start_index..end_index)
    else
      count = end_index + length(items) - start_index

      items
      |> Stream.cycle()
      |> Stream.dedup()
      |> Stream.drop(start_index)
      |> Enum.take(count)
    end
  end
end
