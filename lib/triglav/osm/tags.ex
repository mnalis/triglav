defmodule Triglav.Osm.Tags do
  @moduledoc """
  Utility functions for working with schemas containing tags.
  """
  alias Triglav.Schemas.Osmosis.{Node, Relation, Way}

  @type tagged_schema :: Node.t() | Relation.t() | Way.t()

  @doc """
  Returns the value of a tag with the given name.
  """
  @spec get(tagged_schema(), String.t()) :: String.t() | nil
  def get(schema, name), do: Map.get(schema.tags, name)

  @doc """
  Returns true if a tag with the given name exists (even if it is empty).
  """
  @spec has?(tagged_schema(), String.t()) :: boolean()
  def has?(schema, name), do: Map.has_key?(schema.tags, name)

  @doc """
  Returns true if a tag with the given name and value exists.
  """
  @spec has?(tagged_schema(), String.t(), String.t()) :: boolean()
  def has?(schema, name, value), do: get(schema, name) == value

  @doc """
  Returns true if the entity has all the given tags.
  """
  @spec has_all?(tagged_schema(), [String.t()]) :: boolean()
  def has_all?(schema, names), do: Enum.all?(names, &has?(schema, &1))
end
