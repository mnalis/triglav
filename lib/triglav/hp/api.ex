defmodule Triglav.HP.API do
  @moduledoc """
  Loads and parses post office data from posta.hr.
  """

  @doc """
  Loads data from posta.hr, returns maps ready to be inserted as PostOffice schemas.

  TODO: Check out:
  https://www.posta.hr/preuzimanje-podataka-o-postanskim-uredima-6543/6543
  There's an XML file which could be used to fetch a list of post offices,
  although geo locations are not included and would still need to be parsed
  from the HTML.
  """
  @spec fetch_post_offices!() :: [map()]
  def fetch_post_offices!() do
    result = Tesla.get!("https://www.posta.hr/mapahp.aspx?lng=_hr")
    coordinates_map = parse_coordinates(result.body)

    for {index, record} <- parse_contents(result.body) do
      {latitude, longitude} = Map.fetch!(coordinates_map, index)
      point = %Geo.Point{coordinates: {longitude, latitude}, srid: 4326}
      Map.put(record, :geometry, point)
    end
  end

  defp parse_coordinates(body) do
    [_line, match] = Regex.run(~r/var neighborhoods = \[([^\n]+)\];/, body)

    for [_line, latlng] <- Regex.scan(~r"new google.maps.LatLng\(([^)]+)\)", match) do
      latlng
      |> String.trim()
      |> String.split(",")
      |> Enum.map(&to_float/1)
      |> List.to_tuple()
    end
    |> Enum.with_index()
    |> Map.new(fn {el, idx} -> {idx, el} end)
  end

  defp parse_contents(body) do
    Regex.scan(~r"content\[(\d+)\] = '(.+)';"U, body)
    |> Enum.filter(fn [_, _, content] -> content =~ "POŠTANSKI URED" end)
    |> Enum.map(fn [_line, index, content] ->
      pattern = ~r'<div class="cloud"><h1>POŠTANSKI URED<br /><br />(\d+) ([^<]+)</h1>([^<]+)'
      [_, post_code, place, street] = Regex.run(pattern, content)

      # Street address ends with place, remove it
      street =
        street
        |> String.split(",")
        |> List.delete_at(-1)
        |> Enum.map_join(", ", &String.trim/1)

      {String.to_integer(index),
       %{
         post_code: post_code,
         place: normalize_place(place),
         street: street
       }}
    end)
  end

  defp to_float(string) do
    {float, ""} = string |> String.trim() |> Float.parse()
    float
  end

  def normalize_place(string) do
    string
    |> String.split("-")
    |> Enum.map_join(" - ", &title_case/1)
  end

  defp title_case(string) do
    string
    |> String.split(~r"\s+")
    |> Enum.map(&String.trim/1)
    |> Enum.map_join(" ", &String.capitalize/1)
  end
end
