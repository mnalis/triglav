defmodule Triglav.HP.Mappings do
  @moduledoc """
  Generates PostOfficeMapping records which link imported PostOffice records and
  OSM data.
  """
  alias Triglav.Repo
  alias Triglav.HP.Schemas.PostOffice
  alias Triglav.HP.Schemas.PostOfficeMapping
  alias Triglav.Schemas.Osmosis.Node
  alias Triglav.Schemas.Osmosis.Way
  import Ecto.Query
  import Geo.PostGIS

  @spec generate() :: [PostOfficeMapping.t()]
  def generate() do
    matched_nodes = fetch_matched_nodes()
    matched_ways = fetch_matched_ways()
    unmatched = fetch_unmatched(matched_nodes, matched_ways)

    [matched_nodes, matched_ways, unmatched]
    |> Enum.concat()
    |> Enum.sort_by(& &1.post_office_id)
  end

  defp fetch_matched_nodes() do
    from(p in PostOffice,
      join: n in Node,
      on:
        fragment("? -> ? = ?", n.tags, "amenity", "post_office") and
          fragment("? -> ? = ?", n.tags, "addr:postcode", p.post_code),
      select: %{
        post_office_id: p.id,
        node_id: n.id,
        way_id: nil,
        distance: st_distance_in_meters(p.geometry, n.geom)
      }
    )
    |> Repo.all()
  end

  defp fetch_matched_ways() do
    from(p in PostOffice,
      join: w in Way,
      on:
        fragment("? -> ? = ?", w.tags, "amenity", "post_office") and
          fragment("? -> ? = ?", w.tags, "addr:postcode", p.post_code),
      select: %{
        post_office_id: p.id,
        node_id: nil,
        way_id: w.id,
        distance: st_distance_in_meters(p.geometry, w.linestring)
      }
    )
    |> Repo.all()
  end

  defp fetch_unmatched(matched_nodes, matched_ways) do
    matched_ids =
      Enum.concat(
        Enum.map(matched_nodes, & &1.post_office_id),
        Enum.map(matched_ways, & &1.post_office_id)
      )

    from(p in PostOffice,
      where: p.id not in ^matched_ids,
      select: %{
        post_office_id: p.id,
        node_id: nil,
        way_id: nil,
        distance: nil
      }
    )
    |> Repo.all()
  end
end
