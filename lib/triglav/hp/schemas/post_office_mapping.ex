defmodule Triglav.HP.Schemas.PostOfficeMapping do
  @doc """
  Maps post offices imported from HP with existing OSM nodes/ways representing
  post offices (amenity=post_office). Includes the distance between them for
  validation.
  """
  alias Triglav.HP.Schemas.PostOffice
  alias Triglav.Schemas.Osmosis.Node
  alias Triglav.Schemas.Osmosis.Way
  use Ecto.Schema

  @type t() :: %__MODULE__{}

  schema "hp_post_office_mappings" do
    belongs_to :post_office, PostOffice
    belongs_to :node, Node
    belongs_to :way, Way
    field :distance, :float
  end
end
