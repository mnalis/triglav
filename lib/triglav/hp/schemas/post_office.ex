defmodule Triglav.HP.Schemas.PostOffice do
  use Ecto.Schema

  @type t() :: %__MODULE__{}

  schema "hp_post_offices" do
    field :post_code, :string
    field :street, :string
    field :place, :string
    field :geometry, Geo.PostGIS.Geometry
  end
end
