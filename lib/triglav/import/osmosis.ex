defmodule Triglav.Import.Osmosis do
  @moduledoc """
  Imports the latest OSM data for Croatia from Geofabrik

  See:
  https://download.geofabrik.de/europe/croatia.html
  """

  alias Triglav.Import.Geofabrik
  alias Triglav.Repo
  alias Triglav.Schemas.OsmState

  require Logger

  @schema "osmosis"

  # Osmosis schema creation scripts
  # https://wiki.openstreetmap.org/wiki/Osmosis/PostGIS_Setup
  @scripts [
    "priv/osmosis/pgsnapshot_schema_0.6.sql",
    "priv/osmosis/pgsnapshot_schema_0.6_action.sql",
    "priv/osmosis/pgsnapshot_schema_0.6_changes.sql",
    "priv/osmosis/pgsnapshot_schema_0.6_linestring.sql"
  ]

  def create_schema() do
    file_args =
      @scripts
      |> Enum.map(&Application.app_dir(:triglav, &1))
      |> Enum.map(&["--file", &1])
      |> List.flatten()

    psql_args = [
      "--quiet",
      "--single-transaction",
      "--command",
      "DROP SCHEMA IF EXISTS \"#{@schema}\" CASCADE;",
      "--command",
      "CREATE SCHEMA \"#{@schema}\";",
      "--command",
      "SET search_path TO \"#{@schema}\",public;"
      | file_args
    ]

    cmd("psql", psql_args)
  end

  def schema_exists?() do
    result =
      Repo.query!(
        "SELECT * FROM information_schema.schemata WHERE schema_name = $1;",
        [@schema]
      )

    result.num_rows > 0
  end

  def load_initial() do
    with {:ok, state} <- Geofabrik.web_state(),
         {:ok, tmp_dir} <- Triglav.tmp_dir(),
         {:ok, pbf_path} <- Geofabrik.download_latest(tmp_dir),
         :ok <- cmd("osmosis", ["--read-pbf", pbf_path, "--write-pgsql" | osmosis_db_params()]),
         {:ok, state} <- Geofabrik.save_state(state) do
      Logger.info("Loaded state: #{state.sequence_number} #{state.timestamp}")
      Logger.info("Done")
      :ok
    end
  end

  def load_file(pbf_path) do
    Logger.info("Loading: #{pbf_path}")

    with {:ok, state} <- Geofabrik.file_state(pbf_path),
         :ok <- cmd("osmosis", ["--read-pbf", pbf_path, "--write-pgsql" | osmosis_db_params()]),
         {:ok, state} <- Geofabrik.save_state(state) do
      Logger.info("Loaded state: #{state.sequence_number} #{state.timestamp}")
      Logger.info("Done")
      :ok
    end
  end

  @spec update(pos_integer() | nil) ::
          {:ok, :no_update_available} | {:ok, {:updated_to, OsmState.t()}} | {:error, term}
  def update(max_count \\ nil) do
    Logger.info("Checking Geofabrik for updates...")

    with {:ok, web_state} <- Geofabrik.web_state(),
         {:ok, local_state} <- Geofabrik.local_state() do
      Logger.info("Local state: #{local_state.sequence_number} #{local_state.timestamp}")
      Logger.info("  Web state: #{web_state.sequence_number} #{web_state.timestamp}")

      if web_state.sequence_number > local_state.sequence_number do
        Logger.info("New data available. Updating.")
        apply_updates(local_state, web_state, max_count)
        {:ok, {:updated_to, web_state}}
      else
        Logger.info("You already have the latest data")
        {:ok, :no_update_available}
      end
    end
  end

  defp apply_updates(local_state, web_state, max_count) do
    range = sequence_range(local_state, web_state, max_count)

    Enum.each(range, fn seq ->
      {:ok, state} = apply_update(seq)
      Logger.info("Updated to ##{state.sequence_number}")
    end)
  end

  defp sequence_range(local_state, web_state, max_count)
       when is_integer(max_count) and max_count > 0 do
    min_seq = local_state.sequence_number + 1
    max_seq = min(web_state.sequence_number, min_seq + max_count)
    min_seq..max_seq
  end

  defp sequence_range(local_state, web_state, _) do
    (local_state.sequence_number + 1)..web_state.sequence_number
  end

  defp apply_update(sequence_number) do
    Logger.info("Applying update ##{sequence_number}")

    with {:ok, tmp_dir} <- Triglav.tmp_dir(),
         {:ok, change_path} <- Geofabrik.download_change(sequence_number, tmp_dir),
         {:ok, change_state} <- Geofabrik.get_change_state(sequence_number),
         :ok <- osmosis_apply_update(change_path) do
      Geofabrik.save_state(change_state)
    end
  end

  defp osmosis_apply_update(path) do
    cmd("osmosis", [
      "--read-xml-change",
      "file=#{path}",
      "--write-pgsql-change"
      | osmosis_db_params()
    ])
  end

  def osmosis_db_params do
    params =
      Application.fetch_env!(:triglav, Triglav.Repo)
      |> Keyword.get(:url)
      |> Ecto.Repo.Supervisor.parse_url()

    hostname = Keyword.get(params, :hostname)
    port = Keyword.get(params, :port, 5432)

    [
      host: "#{hostname}:#{port}",
      database: Keyword.get(params, :database),
      user: Keyword.get(params, :username),
      password: Keyword.get(params, :password),
      postgresSchema: @schema
    ]
    |> Enum.map(fn {k, v} -> "#{k}=#{v}" end)
  end

  def cmd(command, args, opts \\ []) do
    Logger.info("Running: " <> Enum.join([command | args], " "))

    case System.cmd(command, args, opts) do
      {_, 0} -> :ok
      _ -> :error
    end
  end
end
