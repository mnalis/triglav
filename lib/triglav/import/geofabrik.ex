defmodule Triglav.Import.Geofabrik do
  @moduledoc """
  Download data snapshots from
  https://download.geofabrik.de/europe/croatia.html
  """

  alias Triglav.Http
  alias Triglav.Repo
  alias Triglav.Schemas.OsmState

  require Logger

  @pbf_url "https://download.geofabrik.de/europe/croatia-latest.osm.pbf"
  @hash_url "https://download.geofabrik.de/europe/croatia-latest.osm.pbf.md5"
  @state_url "https://download.geofabrik.de/europe/croatia-updates/state.txt"
  @updates_url "https://download.geofabrik.de/europe/croatia-updates/"

  @spec web_state() :: {:ok, map()} | {:error, term}
  def web_state() do
    with {:ok, body} <- Http.get(@state_url) do
      parse_web_state(body)
    end
  end

  @spec file_state(String.t()) :: {:ok, map()} | {:error, term}
  def file_state(path) do
    case System.cmd("osmium", ["fileinfo", path]) do
      {out, 0} ->
        terms =
          out
          |> String.split("\n")
          |> Enum.map(&String.trim/1)
          |> Enum.map(&String.split(&1, "="))

        with [_, seq] <-
               Enum.find(terms, &(List.first(&1) == "osmosis_replication_sequence_number")),
             [_, ts] <- Enum.find(terms, &(List.first(&1) == "osmosis_replication_timestamp")),
             {sequence_number, ""} <- Integer.parse(seq),
             {:ok, timestamp, 0} <- DateTime.from_iso8601(ts) do
          {:ok, %{sequence_number: sequence_number, timestamp: timestamp}}
        end

      _ ->
        {:error, :osmium_failed}
    end
  end

  @spec local_state() :: {:ok, OsmState.t()} | {:error, :not_found}
  def local_state() do
    case Repo.get(OsmState, 1) do
      nil -> {:error, :not_found}
      state -> {:ok, state}
    end
  end

  @spec get_local_state() :: OsmState.t() | nil
  def get_local_state() do
    Repo.get(OsmState, 1)
  end

  @spec save_state(map()) :: {:ok, OsmState.t()} | {:error, Ecto.Changeset.t()}
  def save_state(attrs) do
    %OsmState{id: 1}
    |> OsmState.changeset(attrs)
    |> Repo.insert(on_conflict: :replace_all, conflict_target: [:id])
  end

  @spec download_latest(String.t()) :: {:ok, String.t()} | {:error, term()}
  def download_latest(target_dir) do
    with {:ok, pbf_path} <- Http.download(@pbf_url, target_dir, overwrite: true),
         {:ok, hash_path} <- Http.download(@hash_url, target_dir, overwrite: true),
         :ok <- validate_hash(target_dir, hash_path) do
      {:ok, pbf_path}
    end
  end

  @spec download_change(integer(), String.t()) :: {:ok, String.t()} | {:error, term()}
  def download_change(sequence_number, target_dir) do
    sequence_number
    |> change_url()
    |> Http.download(target_dir, overwrite: true)
  end

  # @spec get_change_state(integer()) :: {:ok, OsmState.t()} | {:error, term()}
  def get_change_state(sequence_number) do
    state_url = change_state_url(sequence_number)

    with {:ok, body} <- Http.get(state_url) do
      parse_web_state(body)
    end
  end

  defp parse_web_state(text_state) do
    state =
      text_state
      |> String.split(~r"\n")
      |> Enum.reject(&(String.starts_with?(&1, "#") or &1 == ""))
      |> Enum.map(&String.split(&1, "="))
      |> Enum.map(&List.to_tuple/1)
      |> Enum.into(%{}, fn {k, v} -> {k, v} end)

    with {sequence_number, ""} <- Integer.parse(state["sequenceNumber"]),
         str_timestamp = String.replace(state["timestamp"], ~r"\\", ""),
         {:ok, timestamp, 0} <- DateTime.from_iso8601(str_timestamp) do
      {:ok, %{sequence_number: sequence_number, timestamp: timestamp}}
    end
  end

  defp change_url(sequence_number) do
    change_base_url(sequence_number) <> ".osc.gz"
  end

  defp change_state_url(sequence_number) do
    change_base_url(sequence_number) <> ".state.txt"
  end

  defp change_base_url(sequence_number) do
    path =
      sequence_number
      |> to_string()
      |> String.pad_leading(9, "0")
      |> String.to_charlist()
      |> Enum.chunk_every(3)
      |> Enum.map_join("/", &to_string/1)

    @updates_url <> path
  end

  defp validate_hash(target_dir, hash_path) do
    case System.cmd("md5sum", ["--check", hash_path], cd: target_dir) do
      {_, 0} ->
        Logger.info("Checksum OK")
        :ok

      _ ->
        {:error, :invalid_hash}
    end
  end
end
