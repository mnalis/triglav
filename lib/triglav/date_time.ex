defmodule Triglav.DateTime do
  @doc """
  Parse a date-time from HTTP headers in RFC1123 format.

  For example:

      iex> Triglav.DateTime.parse_http_date("Wed, 01 Jun 2022 22:00:02 GMT")
      {:ok, ~U[2022-06-01 22:00:02Z]}
  """
  @spec parse_http_date(String.t()) ::
          {:ok, DateTime.t()} | {:error, {:invalid_http_date, String.t(), term()}}
  def parse_http_date(date) do
    case :hackney_date.parse_http_date(date) do
      {:error, error} ->
        {:error, {:invalid_http_date, date, error}}

      {date, time} ->
        with {:ok, date} <- Date.from_erl(date),
             {:ok, time} <- Time.from_erl(time) do
          DateTime.new(date, time)
        end
    end
  end

  @spec parse_http_date!(String.t()) :: DateTime.t()
  def parse_http_date!(date) do
    {:ok, date_time} = parse_http_date(date)
    date_time
  end
end
