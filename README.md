# Triglav

![Triglav logo](priv/static/images/logo_title_dark.png)

A web for helping out with editing OSM.

Logo designed by [Nela Dunato](https://neladunato.com/).

Check it out here:
https://triglav.bezdomni.net/

## Prerequisites

* Elixir 1.13 with Erlang/OTP 24
* PostgreSQL 14 with PostGIS 3.2
* Osmosis 0.48.3

May work with other versions, but these are tested.

## Quick start

```ex
# Define where the database is located
export DATABASE_URL=ecto://user:pass@localhost:5432/triglav_dev

# Install elixir dependencies
mix deps.get

# Install node dependancies
npm install --prefix assets

# Initialize the database
mix ecto.create
mix ecto.migrate

# Import OSM data for Croatia from Geofabrik
mix triglav.osmosis_init

# Create operators
# TODO: make this automatic
mix run -e "Triglav.PublicTransport.Operators.all()"

# Import public transport data
mix triglav.pt_update

# Start server
mix phx.server
```

The web is hopefully available at http://localhost:4000/

Deployment
----------

Reading:

* https://hexdocs.pm/phoenix/releases.html
* https://phoenixframework.readme.io/docs/serving-your-application-behind-a-proxy
* https://mfeckie.github.io/Phoenix-In-Production-With-Systemd/

### Nginx config

```
upstream triglav {
  server 127.0.0.1:4000 max_fails=5 fail_timeout=60s;
}

server {
    listen 80;
    server_name triglav.bezdomni.net;
    return 301 https://$host$request_uri;
}

server {
    listen 443;

    server_name triglav.bezdomni.net;

    access_log /var/log/nginx/triglav.bezdomni.net.access.log;
    error_log /var/log/nginx/triglav.bezdomni.net.error.log;

    ssl_certificate     /etc/letsencrypt/live/bezdomni.net/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/bezdomni.net/privkey.pem;
    include             /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam         /etc/letsencrypt/ssl-dhparams.pem;

    location / {
      allow all;

      # Proxy Headers
      proxy_http_version 1.1;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $http_host;
      proxy_set_header X-Cluster-Client-Ip $remote_addr;

      # The Important Websocket Bits!
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";

      proxy_pass http://triglav;
    }
}
```

### Systemd config

**/etc/systemd/system/triglav.service**

```
[Unit]
Description=Triglav daemon
After=network.target

[Service]
PIDFile=/run/triglav/pid
User=ihabunek
Group=ihabunek
WorkingDirectory=/home/ihabunek/projects/triglav
EnvironmentFile=/home/ihabunek/projects/triglav/systemd.env
ExecStart=/home/ihabunek/projects/triglav/prod/rel/triglav/bin/triglav start
ExecReload=/home/ihabunek/projects/triglav/prod/rel/triglav/bin/triglav restart
ExecStop=/home/ihabunek/projects/triglav/prod/rel/triglav/bin/triglav stop

[Install]
WantedBy=multi-user.target
```

Enable service (only required once):

```
sudo systemctl enable triglav.service
```

Read logs:

```
journalctl -u service-name.service
```
